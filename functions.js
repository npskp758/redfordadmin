module.exports = {
    /**
     * This function returns the word after capitalized the first letter. 
     * @param string 
     */
    capWord: function (string){
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    /**
     * This function returns the OTP of 4 characters.
     */
    randomOtp: function() {
        return Math.floor(1000 + Math.random() * 9000);
    }
};