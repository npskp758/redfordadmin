var express = require('express');
var router = express.Router();
var expressValidator = require('express-validator');
router.use(expressValidator());

var candidateController = require('../controllers/api/candidate/candidateController');
var adminController = require('../controllers/api/admin/adminController');
var clientsController = require('../controllers/api/clients/clientsController');
var companyController = require('../controllers/api/company/companyController');
var categoryController = require('../controllers/api/category/categoryController');
var candidateDashboardController = require('../controllers/api/candidate/candidateDashboardController');
var sub_categoryController = require('../controllers/api/sub_category/sub_categoryController');
var jobsController = require('../controllers/api/jobs/jobsController');
// var candidateJobController = require('../controllers/api/candidate/candidateJobController');
var homeController = require('../controllers/api/home/homeController');
var subscribe_listController = require('../controllers/api/subscribe_list/subscribe_listController');





///////////// candidate Controller start////////////////

router.post('/candidate-signin',candidateController.candidate_signin);
router.post('/candidate-registration',candidateController.candidate_registration);
router.post('/candidate-list',candidateController.candidate_list);

router.post('/candidate-provider-registration',candidateController.candidate_provider_registration);
router.post('/candidate-provider-signin',candidateController.candidate_provider_signin);

router.get('/candidate-category-list',candidateController.candidate_category_list);
router.post('/candidate-details',candidateController.candidate_details);
router.post('/check-candidate-category', candidateController.check_candidate_category);
router.post('/provider-candidate-update', candidateController.provider_candidate_update);
router.post('/candidate-password-update', candidateController.candidate_password_update);

router.post('/candidate-preferred-job-listing',candidateController.candidate_preferred_job_listing);
router.post('/candidate-preferred-job-delete',candidateController.candidate_preferred_job_delete);
router.post('/candidate-applied-job-listing',candidateController.candidate_applied_job_listing);

router.post('/candidate-addedit',candidateController.candidate_addedit);
router.post('/candidate-delete',candidateController.candidate_delete);

///////////// candidate Controller ends////////////////

///////////// candidate Dashboard Controller start////////////////

router.post('/candidate-dashboard',candidateDashboardController.candidate_dashboard);
router.post('/candidate-cv-upload',candidateDashboardController.candidate_cv_upload);
router.post('/candidate-work-experience-upload',candidateDashboardController.candidate_work_experience_upload);
router.post('/candidate-work-experience-delete',candidateDashboardController.candidate_work_experience_delete);
router.post('/candidate-qualification-upload',candidateDashboardController.candidate_qualification_upload);
router.post('/candidate-qualification-delete',candidateDashboardController.candidate_qualification_delete);
router.post('/candidate-language-upload',candidateDashboardController.candidate_language_upload);
router.post('/candidate-language-delete',candidateDashboardController.candidate_language_delete);
router.post('/candidate-skill-upload',candidateDashboardController.candidate_skill_upload);
router.post('/candidate-skill-delete',candidateDashboardController.candidate_skill_delete);
router.post('/candidate-looking-for-upload',candidateDashboardController.candidate_looking_for_upload);
router.post('/candidate-address-info-upload',candidateDashboardController.candidate_address_info_upload);
router.post('/candidate-profile-pic-upload',candidateDashboardController.candidate_profile_pic_upload);
router.post('/candidate-medical-info-upload',candidateDashboardController.candidate_medical_info_upload);
router.post('/candidate-related-job',candidateDashboardController.candidate_related_job);


///////////// candidate Dashboard Controller ends////////////////

///////////// Admin Controller start////////////////

router.post('/admin-signin',adminController.admin_signin);
router.post('/client-list',adminController.client_list);
router.post('/client-details',adminController.client_details);

router.get('/company-listing',adminController.company_listing);
router.post('/company-details',adminController.company_details);
router.post('/company-listing-for-client',adminController.company_listing_for_client);

router.get('/candidate-listing-for-admin',adminController.candidate_listing_for_admin);
router.post('/candidate-details-for-admin',adminController.candidate_details_for_admin);

// router.post('/client-addedit',adminController.client_addedit);
// router.post('/client-delete',adminController.client_delete);

router.post('/job-opening-status-upload',adminController.job_opening_status_upload);
router.post('/job-opening-status-list',adminController.job_opening_status_list);
router.post('/job-opening-status-delete',adminController.job_opening_status_delete);

router.post('/website-logo-upload',adminController.website_logo_upload);
router.post('/website-logo-showing',adminController.website_logo_showing);

router.post('/candidate-job-opening-status-upload',adminController.candidate_job_opening_status_upload);
router.post('/candidate-job-opening-status-list',adminController.candidate_job_opening_status_list);
router.post('/candidate-job-opening-status-delete',adminController.candidate_job_opening_status_delete);

router.post('/candidate-job-opening-status-dropdown',adminController.candidate_job_opening_status_dropdown);
router.post('/candidate-job-opening-sub-status-upload',adminController.candidate_job_opening_sub_status_upload);
router.post('/candidate-job-opening-sub-status-list',adminController.candidate_job_opening_sub_status_list);
router.post('/candidate-job-opening-sub-status-delete',adminController.candidate_job_opening_sub_status_delete);

router.post('/email-template-type-list',adminController.email_template_type_list);
router.post('/email-template-upload',adminController.email_template_upload);
router.post('/email-template-list',adminController.email_template_list);
router.post('/email-template-details',adminController.email_template_details);
router.post('/email-template-delete',adminController.email_template_delete);

router.post('/admin-profile-information',adminController.admin_profile_information);
router.post('/admin-profile-update',adminController.admin_profile_update);
router.post('/admin-password-update',adminController.admin_password_update);

router.post('/admin-dashboard',adminController.admin_dashboard);

///////////// Admin Controller ends////////////////

///////////// clients Controller start////////////////

router.get('/client-registration-form',clientsController.client_registration_form);
router.post('/client-signin',clientsController.client_signin);
router.post('/client-registration',clientsController.client_registration);
router.post('/client-provider-registration',clientsController.client_provider_registration);
router.post('/client-provider-signin',clientsController.client_provider_signin);
router.post('/provider-client-update', clientsController.provider_client_update);

router.post('/client-company-submit',clientsController.client_company_submit);
router.post('/client-company-listing',clientsController.client_company_listing);
router.post('/client-company-details',clientsController.client_company_details);
router.post('/client-company-delete',clientsController.client_company_delete);
router.post('/client-profile-information',clientsController.client_profile_information);
router.post('/client-profile-update',clientsController.client_profile_update);
router.post('/client-password-update',clientsController.client_password_update);
router.post('/client-delete',clientsController.client_delete);

router.post('/candidate-job-opening-status',clientsController.candidate_job_opening_status);
router.post('/candidate-job-opening-sub-status',clientsController.candidate_job_opening_sub_status);

router.post('/client-dashboard',clientsController.client_dashboard);

router.post('/mail-check',clientsController.mail_check);

///////////// clients Controller ends////////////////

///////////// company Controller start////////////////

router.post('/company-size',companyController.company_sizeList);
router.post('/company-type',companyController.company_typeList);

///////////// company Controller ends////////////////

///////////// category Controller start////////////////

router.get('/category-list',categoryController.categoryList);
router.post('/category-addedit',categoryController.categoryAddEdit);
router.post('/category-delete',categoryController.categoryDelete);

///////////// category Controller ends////////////////

///////////// jobs Controller start////////////////

router.post('/client-company-list-on-job-posting-form',jobsController.client_company_list_on_job_posting_form);
router.post('/company-details-on-job-posting-form',jobsController.company_details_on_job_posting_form);
router.get('/job-posting-form',jobsController.job_posting_form);
router.post('/job-posting',jobsController.job_posting);
router.post('/candidate-job-listing',jobsController.candidate_job_listing);
router.post('/job-details',jobsController.job_details);
router.post('/candidate-job-apply',jobsController.candidate_job_apply);
router.post('/candidate-shortlisted-job',jobsController.candidate_shortlisted_job);

router.post('/client-job-listing',jobsController.client_job_listing);
router.post('/client-job-details',jobsController.client_job_details);
router.post('/candidate-favourite-job-upload',jobsController.candidate_favourite_job_upload);
router.post('/client-favourite-job-list',jobsController.client_favourite_job_list);
// router.post('/job-details-with-out-login',jobsController.job_details_with_out_login);
router.post('/job-delete',jobsController.job_delete);

router.post('/assigned-candidate-list',jobsController.assigned_candidate_list);
router.post('/assigned-candidate-delete',jobsController.assigned_candidate_delete);
router.post('/shortlisted-candidate-list',jobsController.shortlisted_candidate_list);
router.post('/assigned-candidate-upload',jobsController.assigned_candidate_upload);
router.post('/job-opening-status-in-dropdown',jobsController.job_opening_status_in_dropdown);
router.post('/assigned-candidate-update',jobsController.assigned_candidate_update);
router.post('/assigned-candidate-details',jobsController.assigned_candidate_details);

router.post('/job-status-changed-history',jobsController.job_status_changed_history);
router.post('/candidate-status-changed-history',jobsController.candidate_status_changed_history);
router.post('/candidate-job-application-status-history',jobsController.candidate_job_application_status_history);



///////////// jobs Controller ends////////////////


///////////// home Controller start////////////////

router.post('/job-list',homeController.jobsList);
router.get('/home-all-type-count',homeController.home_all_type_count);
router.post('/home-page-job-search',homeController.home_page_job_search);
router.post('/header-credential-details',homeController.header_credential_details);

router.post('/testimonial-upload',homeController.testimonial_upload);
router.post('/testimonial-list',homeController.testimonial_list);
router.post('/testimonial-details',homeController.testimonial_details);
router.post('/testimonial-delete',homeController.testimonial_delete);
router.post('/home-testimonial',homeController.home_testimonial);

router.post('/cms-upload',homeController.cms_upload);
router.post('/cms-list',homeController.cms_list);
router.post('/cms-details',homeController.cms_details);
router.post('/cms-delete',homeController.cms_delete);
router.post('/home-cms-list',homeController.home_cms_list);
router.post('/home-cms-details',homeController.home_cms_details);

router.post('/forgot-password',homeController.forgot_password);
router.post('/forgot-password-otp-check',homeController.forgot_password_otp_check);
router.post('/forgot-password-update',homeController.forgot_password_update);

router.post('/who-we-are-upload',homeController.who_we_are_upload);
router.post('/who-we-are-details',homeController.who_we_are_details);

router.post('/contact-us-upload',homeController.contact_us_upload);
router.post('/contact-us-list',homeController.contact_us_list);
router.post('/contact-us-details',homeController.contact_us_details);

///////////// home Controller ends////////////////

///////////// sub category Controller start////////////////

router.get('/sub-category-list',sub_categoryController.sub_categoryList);
router.post('/sub-category-addedit',sub_categoryController.sub_categoryAddEdit);
router.post('/sub-category-delete',sub_categoryController.sub_categoryDelete);
router.post('/category-sub-categoryList',sub_categoryController.categorytosub_categoryList);

///////////// sub category Controller ends////////////////

////////// subscribe_list Controller  start///////////////
router.post('/subscribed',subscribe_listController.subscribe);
/////////// subscribe_list Controller ends///////////////



module.exports = router;




