var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);


/************************* candidate dashboard details start *******************************/

exports.candidate_dashboard = async function(req, res, next) {
    console.log(req.body.data);
    var candidate_id = req.body.data.candidate_id;
    var resultArray = [];
    // console.log(candidate_cv);
    
    if(candidate_id && candidate_id !='' ){ 

        var candidate_experience_details =await sequelize.query("SELECT candidate_experience_details.* from candidate_experience_details where candidate_experience_details.candidate_id ="+candidate_id+" ORDER BY `cand_experience_details_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_qualification_details =await sequelize.query("SELECT candidate_qualification.* from candidate_qualification where candidate_id ="+candidate_id+" ORDER BY `candidate_qualification_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_language_details =await sequelize.query("SELECT candidate_language.candidate_language_id, candidate_language.candidate_id, candidate_language.language, candidate_language.fluency from candidate_language where candidate_id ="+candidate_id+" ORDER BY `candidate_language_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_skill_details =await sequelize.query("SELECT candidate_skill.candidate_skill_id, candidate_skill.candidate_id, candidate_skill.skill from candidate_skill where candidate_id ="+candidate_id+" ORDER BY `candidate_skill_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_looking_for =await sequelize.query("SELECT candidate_looking_for.*, categories.title as category_title from candidate_looking_for LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_address_info =await sequelize.query("SELECT candidates.*, candidate_address_info.cand_address_info_id, candidate_address_info.address, candidate_address_info.city, candidate_address_info.state, candidate_address_info.country, candidate_address_info.zip_code, candidate_address_info.website from candidates LEFT JOIN candidate_address_info ON candidates.candidate_id = candidate_address_info.candidate_id  where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_cv =await sequelize.query("SELECT profile_picture, provider_image, candidate_id, cv_name  from candidates where candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_medical_info =await sequelize.query("SELECT candidate_medical_info.*  from candidate_medical_info where candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });

        if(candidate_address_info.length > 0){

            var i = 0;
            var candidate_first_name = '';
            var candidate_last_name = '';

            candidate_address_info.forEach(function(element,index){ 

                if(element.first_name !='' && element.first_name!= null){
                    candidate_first_name = element.first_name;
                    candidate_last_name = element.last_name;
                }else if(element.name !='' && element.name!= null){
                    candidate_name = element.name.split(' ');
                    candidate_first_name = candidate_name[0];
                    for( var j =1; j<=candidate_name.length-1; j++){
                        candidate_last_name += candidate_name[j]+ " ";
                    }
                    candidate_last_name = candidate_last_name.substring(0, candidate_last_name.length-1);
                }

                resultArray.push({
                    "candidate_id": element.candidate_id,
                    "username": element.username,
                    "first_name": candidate_first_name,
                    "last_name": candidate_last_name,
                    "email": element.email,
                    "mob_country_code": element.mob_country_code,
                    "mobile": element.mobile,
                    "gender": element.gender,
                    "nationality": element.nationality,
                    "cand_address_info_id": element.cand_address_info_id,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "zip_code": element.zip_code,
                    "website": element.website
                });    
                i++;             
            });
        }

        if(candidate_cv[0].profile_picture !='' && candidate_cv[0].profile_picture != null){
            //var profile_picture = 'contents/candidates/'+ candidate_id +'/'+ candidate_cv[0].profile_picture;
            var profile_picture = req.app.locals.baseurl+candidate_cv[0].profile_picture;
        }else if(candidate_cv[0].provider_image !='' && candidate_cv[0].provider_image != null){
            var profile_picture = candidate_cv[0].provider_image;
        }else{
            var profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
        }

        if(candidate_cv[0].cv_name !='' && candidate_cv[0].cv_name != null){
            // var path='contents/candidates/'+ candidate_id +'/'+candidate_cv[0].cv_name;
            var path= req.app.locals.baseurl+candidate_cv[0].cv_name;
        }else{
            var path='';
        }

        res.status(200).send({ status:200, success: true, value:candidate_experience_details, qualification_details:candidate_qualification_details, language_details:candidate_language_details, skill_details:candidate_skill_details, candidate_looking_for:candidate_looking_for, candidate_address_info:resultArray, candidate_cv: path, profile_picture:profile_picture, candidate_medical_info:candidate_medical_info }); 

    }else{
        res.status(200).json({ success: false, message: "Candidate id is required!"});
    }  
}

/************************* candidate dashboard details ends *******************************/


/************************* candidate dashboard details start *******************************/

exports.candidate_dashboard_111 = function(req, res, next) {
    
    var candidate_id = req.body.data.candidate_id;
    var resultArray = [];
    // console.log(candidate_cv);
    
    if(candidate_id !='' ){

        // var candidate_experience_details = sequelize.query("SELECT candidate_experience_details.*, CONCAT(job_title,' in ',company,' from ',from_month,' ',from_year,' to ',to_month,' ',to_year) as job_experience_title from candidate_experience_details where candidate_experience_details.candidate_id ="+candidate_id+" ORDER BY `cand_experience_details_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
        var candidate_experience_details = sequelize.query("SELECT candidate_experience_details.* from candidate_experience_details where candidate_experience_details.candidate_id ="+candidate_id+" ORDER BY `cand_experience_details_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
        candidate_experience_details.then(function (candidate_experience_details) {
            var candidate_qualification_details = sequelize.query("SELECT candidate_qualification.* from candidate_qualification where candidate_id ="+candidate_id+" ORDER BY `candidate_qualification_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
            candidate_qualification_details.then(function (candidate_qualification_details) {
                var candidate_language_details = sequelize.query("SELECT candidate_language.candidate_language_id, candidate_language.candidate_id, candidate_language.language, candidate_language.fluency from candidate_language where candidate_id ="+candidate_id+" ORDER BY `candidate_language_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
                candidate_language_details.then(function (candidate_language_details) {
                    var candidate_skill_details = sequelize.query("SELECT candidate_skill.candidate_skill_id, candidate_skill.candidate_id, candidate_skill.skill from candidate_skill where candidate_id ="+candidate_id+" ORDER BY `candidate_skill_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
                    candidate_skill_details.then(function (candidate_skill_details) {
                        var candidate_looking_for = sequelize.query("SELECT candidate_looking_for.*, categories.title as category_title from candidate_looking_for LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })
                        candidate_looking_for.then(function (candidate_looking_for) {
                            // var candidate_address_info = sequelize.query("SELECT candidate_address_info.* from candidate_address_info where candidate_address_info.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })
                            // var candidate_address_info = sequelize.query("SELECT candidates.gender, candidates.nationality, candidate_address_info.* from candidate_address_info LEFT JOIN candidates ON candidate_address_info.candidate_id = candidates.candidate_id  where candidate_address_info.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })
                            var candidate_address_info = sequelize.query("SELECT candidates.*, candidate_address_info.cand_address_info_id, candidate_address_info.address, candidate_address_info.city, candidate_address_info.state, candidate_address_info.country, candidate_address_info.zip_code, candidate_address_info.website from candidates LEFT JOIN candidate_address_info ON candidates.candidate_id = candidate_address_info.candidate_id  where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })
                            candidate_address_info.then(function (candidate_address_info) {
                                var candidate_cv = sequelize.query("SELECT profile_picture, provider_image, candidate_id, cv_name  from candidates where candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })
                                candidate_cv.then(function (candidate_cv) {
                                    var candidate_medical_info = sequelize.query("SELECT candidate_medical_info.*  from candidate_medical_info where candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })
                                    candidate_medical_info.then(function (candidate_medical_info) {

                                        if(candidate_address_info.length > 0){

                                            var i = 0;
                                            var candidate_first_name = '';
                                            var candidate_last_name = '';
                                
                                            candidate_address_info.forEach(function(element,index){ 
                                
                                                if(element.first_name !='' && element.first_name!= null){
                                                    candidate_first_name = element.first_name;
                                                    candidate_last_name = element.last_name;
                                                }else if(element.name !='' && element.name!= null){
                                                    candidate_name = element.name.split(' ');
                                                    candidate_first_name = candidate_name[0];
                                                    for( var j =1; j<=candidate_name.length-1; j++){
                                                        candidate_last_name += candidate_name[j]+ " ";
                                                    }
                                                    candidate_last_name = candidate_last_name.substring(0, candidate_last_name.length-1);
                                                }
                                
                                                resultArray.push({
                                                    "candidate_id": element.candidate_id,
                                                    "username": element.username,
                                                    "first_name": candidate_first_name,
                                                    "last_name": candidate_last_name,
                                                    "email": element.email,
                                                    "mob_country_code": element.mob_country_code,
                                                    "mobile": element.mobile,
                                                    "gender": element.gender,
                                                    "nationality": element.nationality,
                                                    "cand_address_info_id": element.cand_address_info_id,
                                                    "address": element.address,
                                                    "city": element.city,
                                                    "state": element.state,
                                                    "country": element.country,
                                                    "zip_code": element.zip_code,
                                                    "website": element.website
                                                });    
                                                i++;             
                                            });
                                        }
                                        

                                        if(candidate_cv[0].profile_picture !='' && candidate_cv[0].profile_picture != null){
                                            //var profile_picture = 'contents/candidates/'+ candidate_id +'/'+ candidate_cv[0].profile_picture;
                                            var profile_picture = req.app.locals.baseurl+candidate_cv[0].profile_picture;
                                        }else if(candidate_cv[0].provider_image !='' && candidate_cv[0].provider_image != null){
                                            var profile_picture = candidate_cv[0].provider_image;
                                        }else{
                                            var profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                                        }

                                        if(candidate_cv[0].cv_name !='' && candidate_cv[0].cv_name != null){
                                            // var path='contents/candidates/'+ candidate_id +'/'+candidate_cv[0].cv_name;
                                            var path= req.app.locals.baseurl+candidate_cv[0].cv_name;
                                        }else{
                                            var path='';
                                        }

                                        

                                        res.status(200).send({ status:200, success: true, value:candidate_experience_details, qualification_details:candidate_qualification_details, language_details:candidate_language_details, skill_details:candidate_skill_details, candidate_looking_for:candidate_looking_for, candidate_address_info:resultArray, candidate_cv: path, profile_picture:profile_picture, candidate_medical_info:candidate_medical_info }); 

                                    }).catch(function(error) {
                                        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                                    });
                                }).catch(function(error) {
                                    res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                                });
                            }).catch(function(error) {
                                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                            });
                        }).catch(function(error) {
                            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                        });
                    }).catch(function(error) {
                        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                    });
                }).catch(function(error) {
                    res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                });
            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });

        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });
        
    }else{
        res.status(200).json({ success: false, message: "Candidate id is required!"});
    }  
}

/************************* candidate dashboard details ends *******************************/

// /************************* candidate About You start *******************************/
// exports.candidate_about_you = function(req, res, next) {
// var candidate_id = req.body.data.candidate_id;
// var profile_picture = req.body.data.profile_picture;
// var path = 'contents/candidates/'+ candidate_id +'/profile_picture.jpg'

// if(candidate_id !==''){
//    models.candidates.update({
//        profile_picture : path
//    },{where:{candidate_id:candidate_id}})

//    try {
//         const path = path;
//         const imgdata = candidate_cv;
//         const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
//         fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
//     } catch (e) {
//         next(e);
//     }
//     res.status(200).send({ success: "true", message: "successfully uploaded", path:'contents/candidates/'+ candidate_id +'/profile_picture.jpg'});

// }else{

// }
// }


// /************************* candidate About You ends *******************************/


/************************* candidate cv upload start *******************************/

exports.candidate_cv_upload = function(req, res, next) {
    
    var candidate_id = req.body.data.candidate_id;
    var candidate_cv = req.body.data.candidate_cv;
    var cv_extension = req.body.data.cv_extension;

    // console.log(candidate_cv);

    
    if(candidate_id && candidate_id !='' && candidate_cv && candidate_cv !='' && cv_extension && cv_extension !='' ){

        if(cv_extension == 'pdf'){
            
            models.candidates.update({
                cv_name:'contents/candidates/'+ candidate_id +'/cv.'+ cv_extension,
            },{where:{candidate_id:candidate_id}})

            try {
                const path = './public/contents/candidates/'+ candidate_id +'/cv.pdf';
                const imgdata = candidate_cv;
                const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
            } catch (e) {
                next(e);
            }
            res.status(200).send({ success: "true", message: "CV successfully uploaded", path:req.app.locals.baseurl+'contents/candidates/'+ candidate_id +'/cv.pdf'});
        }else if(cv_extension =='doc' || cv_extension =='docx' ){

            models.candidates.update({
                cv_name:'contents/candidates/'+ candidate_id +'/cv.'+ cv_extension,
            },{where:{candidate_id:candidate_id}})

            try {
                const path = './public/contents/candidates/'+ candidate_id +'/cv.'+ cv_extension;
                const imgdata = candidate_cv;
                const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
            } catch (e) {
                next(e);
            }
            res.status(200).send({ success: "true", message: "Cv successfully uploaded", path:req.app.locals.baseurl+'contents/candidates/'+ candidate_id +'/cv.'+ cv_extension });
        }else{
            res.status(200).json({ success: "false",message: "Only .pdf, .doc, .docx supported"});
        }
        
    }else{
        res.status(200).json({ success: "false",message: "All fileds are required!"});
    }  
}

/************************* candidate cv upload ends *******************************/


/************************* candidate work experience upload start *******************************/

exports.candidate_work_experience_upload = async function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var job_title = req.body.data.job_title;
    var company = req.body.data.company;
    var from_month = req.body.data.from_month;
    var from_year = req.body.data.from_year;
    var to_month = req.body.data.to_month;
    var to_year = req.body.data.to_year;
    var description = req.body.data.description;
    var is_current_employer = req.body.data.terms;
    var experience_id = req.body.data.experience_id;

    //var experience_year = '';
    //var experience_month = '';
    var months_array = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    console.log("________________________________________________________");
    console.log(is_current_employer);
    // if(candidate_id !='' && job_title !='' && company !='' && from_month !='' && from_year !='' && to_month !='' && to_year !='' ){
    if(candidate_id !='' && job_title !='' && company !='' && from_month !='' && from_year !='' ){
        var candidate_current_employer_details =await sequelize.query("SELECT candidate_experience_details.* from candidate_experience_details where candidate_experience_details.candidate_id ="+candidate_id+" and candidate_experience_details.is_current_employer = 1 ",{ type: Sequelize.QueryTypes.SELECT });
        
        //from = new Date(from_year, from_month);
        //to = new Date(to_year, to_month);
        //console.log("________________________________________________________");
        //console.log(calcDate(to, from));
        //console.log("________________________________________________________");
        // var years = to_year - from_year;
        // var months = months_array.indexOf() 
        // if(is_current_employer == 1){
        //     var current_employer = 'true';
        // }else{
        //     var current_employer = 'false';
        // }

        // now = new Date(); 
        // year = now.getFullYear(); 
        if(is_current_employer == 1){
            now = new Date(); 
            
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            
            var current_month = month[now.getMonth()];
            current_year = now.getFullYear();
            var experience_year = current_year-from_year;
            var experience_month = '';
        }else{
            var experience_year = to_year-from_year;
            var experience_month = '';
        }
        

        if(!experience_id){

            models.candidate_experience_details.create({ 

                candidate_id: candidate_id,
                job_title: job_title,
                company: company,
                from_month: from_month,
                from_year: from_year,
                to_month: to_month,
                to_year: to_year,
                description: description,
                experience_in_year: experience_year,
                experience_in_month: experience_month,
                is_current_employer: is_current_employer,
                createdBy: candidate_id,

            }).then(async function(candidate_experience_details) {
                //update table is_current_employer='' where id != candidate_experience_details.id and candidate_id = candidate_id
                if(is_current_employer == 1){
                    models.candidate_experience_details.update({
                        
                        is_current_employer: 0,
        
                    },{where:{cand_experience_details_id: {$ne: candidate_experience_details.cand_experience_details_id},candidate_id:candidate_id}})

                    if(candidate_current_employer_details.length > 0){

                        models.candidate_experience_details.update({
                        
                            to_month: from_month,
                            to_year: from_year,
            
                        },{where:{cand_experience_details_id: candidate_current_employer_details[0].cand_experience_details_id}})

                    }
                }

            res.status(200).send({ success: true, message: "Candidate experience successfully added" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{

            models.candidate_experience_details.update({ 

                candidate_id: candidate_id,
                job_title: job_title,
                company: company,
                from_month: from_month,
                from_year: from_year,
                to_month: to_month,
                to_year: to_year,
                description: description,
                experience_in_year: experience_year,
                experience_in_month: experience_month,
                is_current_employer: is_current_employer,
                updatedBy: candidate_id,

            },{where:{cand_experience_details_id:experience_id}}).then(async function(candidate_experience_details) {

                if(is_current_employer == 1){
                    models.candidate_experience_details.update({
                        
                        is_current_employer: 0,
        
                    },{where:{cand_experience_details_id: {$ne: experience_id},candidate_id:candidate_id}})

                    if(candidate_current_employer_details.length > 0){

                        models.candidate_experience_details.update({
                        
                            to_month: from_month,
                            to_year: from_year,
            
                        },{where:{cand_experience_details_id: candidate_current_employer_details[0].cand_experience_details_id}})

                    }

                }

            res.status(200).send({ success: true, message: "Candidate experience successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }
    }else{
        res.status(200).json({ success: false, message: "All fileds are required!"});
    } 
}

/************************* candidate work experience upload ends *******************************/

/************************* candidate work experience delete start *******************************/

exports.candidate_work_experience_delete = function(req, res, next) {
    console.log(req.body.data);
    var experience_id = req.body.data.experience_id;

    if(experience_id !='' ){

        models.candidate_experience_details.destroy({ 
            where:{cand_experience_details_id:experience_id}
        }).then(function(value) {
            res.status(200).send({ success: true, message: "Candidate experience successfully deleted" });
        });

    }else{
        res.status(200).json({ success: false, message: "Experience id is required!"});
    } 
}

/************************* candidate work experience delete ends *******************************/

/************************* candidate qualification upload start *******************************/

exports.candidate_qualification_upload = function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var qualification_type = req.body.data.qualification_type;
    var school_college = req.body.data.school_college;
    var from_year = req.body.data.from_year;
    var to_year = req.body.data.to_year;
    var specialization = req.body.data.specialization;
    var marks_grade = req.body.data.marks_grade;
    var qualification_id = req.body.data.qualification_id;

    if(candidate_id !='' && qualification_type !='' && school_college !='' && from_year !='' && to_year !='' && specialization !='' && marks_grade!='' ){
        console.log('11111111111111111111111');
        if(!qualification_id){
            console.log('22222222222222222222222');
            models.candidate_qualification.create({ 

                candidate_id: candidate_id,
                qualification_type: qualification_type,
                school_college: school_college,
                from_year: from_year,
                to_year: to_year,
                specialization: specialization,
                marks_grade: marks_grade,
                createdBy: candidate_id,

            }).then(function(candidate_qualification) {

            res.status(200).send({ success: true, message: "Candidate qualification successfully added" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.candidate_qualification.update({ 

                candidate_id: candidate_id,
                qualification_type: qualification_type,
                school_college: school_college,
                from_year: from_year,
                to_year: to_year,
                specialization: specialization,
                marks_grade: marks_grade,
                updatedBy: candidate_id,

            },{where:{candidate_qualification_id:qualification_id}}).then(function(candidate_qualification) {

                res.status(200).send({ success: true, message: "Candidate qualification successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ success: false, message: "All fileds are required!"});
    } 
}

/************************* candidate qualification upload ends *******************************/

/************************* candidate qualification delete start *******************************/

exports.candidate_qualification_delete = function(req, res, next) {
    console.log(req.body.data);
    var qualification_id = req.body.data.qualification_id;

    if(qualification_id !='' ){

        models.candidate_qualification.destroy({ 
            where:{candidate_qualification_id:qualification_id}
        }).then(function(value) {
            res.status(200).send({ success: true, message: "Candidate qualification successfully deleted" });
        });

    }else{
        res.status(200).json({ success: false, message: "Qualification id is required!"});
    } 
}

/************************* candidate qualification delete ends *******************************/

/************************* candidate language upload start *******************************/

exports.candidate_language_upload = async function(req, res, next) {
    console.log(req.body.data);

    // var candidate_id = req.body.data.candidate_id;
    // var language = req.body.data.language;
    // var fluency = req.body.data.fluency;
    // var language_id = req.body.data.language_id;

    // var languageArr = req.body.data.language.split(",");
    // console.log(languageArr);
    // console.log(languageArr[0]);

    // if(candidate_id !='' && language !='' && fluency !='' ){
    //     console.log('11111111111111111111111');
    //     if(!language_id){
    //         console.log('22222222222222222222222');
    //         models.candidate_language.create({ 

    //             candidate_id: candidate_id,
    //             language: languageArr[0],
    //             fluency: fluency,
    //             createdBy: candidate_id,

    //         }).then(function(candidate_language) {

    //         res.status(200).send({ success: true, message: "Candidate language successfully added" });
    //         })
    //         .catch(function(error) {
    //             return res.send( {success: false, error});
    //         });

    //     }else{
    //         console.log('333333333333333333333333333');
    //         models.candidate_language.update({ 

    //             candidate_id: candidate_id,
    //             language: languageArr[0],
    //             fluency: fluency,
    //             updatedBy: candidate_id,

    //         },{where:{candidate_language_id:language_id}}).then(function(candidate_language) {

    //             res.status(200).send({ success: true, message: "Candidate language successfully updated" });
    //         })
    //         .catch(function(error) {
    //             return res.send( {success: false, error});
    //         });

    //     }

    // }else{
    //     res.status(200).json({ success: false, message: "All fileds are required!"});
    // } 

    var candidate_id = req.body.data.candidate_id;
    var language = req.body.data.language;
    var fluency = req.body.data.fluency;
    var language_id = req.body.data.language_id;

    if(candidate_id && candidate_id !='' && language && language !='' && fluency && fluency !='' ){
        console.log('11111111111111111111111');
        var languageArr = req.body.data.language.split(",");
        var captilazedLanguage = languageArr[0].charAt(0).toUpperCase() + languageArr[0].substring(1);

        if(!language_id){
            console.log('22222222222222222222222');
            var candidate_language_verification =await sequelize.query("SELECT candidate_language.candidate_language_id, candidate_language.candidate_id, candidate_language.language, candidate_language.fluency from candidate_language where candidate_id ="+candidate_id+" and language = '"+captilazedLanguage+"'",{ type: Sequelize.QueryTypes.SELECT });

            if(candidate_language_verification.length > 0){
                console.log('11111111111111111111111');
    
                res.status(200).json({ status:200, success: false, message: "Language already added"});
                
            }else{
                models.candidate_language.create({ 

                    candidate_id: candidate_id,
                    language: captilazedLanguage,
                    fluency: fluency,
                    createdBy: candidate_id,

                }).then(function(candidate_language) {

                res.status(200).send({ success: true, message: "Candidate language successfully added" });
                })
                .catch(function(error) {
                    return res.send( {success: false, error});
                });
            }

        }else{

            var candidate_language_verification =await sequelize.query("SELECT candidate_language.candidate_language_id, candidate_language.candidate_id, candidate_language.language, candidate_language.fluency from candidate_language where candidate_id ="+candidate_id+" and language = '"+captilazedLanguage+"' and fluency = '"+fluency+"'",{ type: Sequelize.QueryTypes.SELECT });

            if(candidate_language_verification.length > 0){
                console.log('11111111111111111111111');
    
                res.status(200).json({ status:200, success: false, message: "Language already added"});
                
            }else{

                console.log('333333333333333333333333333');
                models.candidate_language.update({ 

                    candidate_id: candidate_id,
                    language: captilazedLanguage,
                    fluency: fluency,
                    updatedBy: candidate_id,

                },{where:{candidate_language_id:language_id}}).then(function(candidate_language) {

                    res.status(200).send({ success: true, message: "Candidate language successfully updated" });
                })
                .catch(function(error) {
                    return res.send( {success: false, error});
                });
            }
        }

    }else{
        res.status(200).json({ success: false, message: "All fileds are required!"});
    } 
} 

/************************* candidate language upload ends *******************************/

/************************* candidate language delete start *******************************/

exports.candidate_language_delete = function(req, res, next) {
    console.log(req.body.data);
    var language_id = req.body.data.language_id;

    if(language_id !='' ){

        models.candidate_language.destroy({ 
            where:{candidate_language_id:language_id}
        }).then(function(value) {
            res.status(200).send({ success: true, message: "Candidate language successfully deleted" });
        });

    }else{
        res.status(200).json({ success: false, message: "Language id is required!"});
    } 
}

/************************* candidate language delete ends *******************************/



/************************* candidate skill upload start *******************************/

// exports.candidate_skill_upload = function(req, res, next) {
//     console.log(req.body.data);

//     var candidate_id = req.body.data.candidate_id;
//     var skill = req.body.data.skill;
//     //var skill_id = req.body.data.skill_id;

//     var candidate_skill = req.body.data.skill.split(",");
//     console.log('1111111111111111111111');
//     console.log(candidate_skill);

//     if(candidate_id !='' && skill !='' ){

//         var i=0;                       
//         candidate_skill.forEach(function(skl){
//             models.candidate_skill.create({
//                 candidate_id: candidate_id,
//                 skill:candidate_skill[i],
//                 createdBy: candidate_id,                            
//             });
//             i++;		
//         }, this);

//         res.status(200).send({ success: true, message: "Candidate skill successfully added" });

//     }else{
//         res.status(200).json({ success: false, message: "All fileds are required!"});
//     } 
// }


exports.candidate_skill_upload = async function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var skill = req.body.data.skill;
    //var skill_id = req.body.data.skill_id;

    var candidate_skill = req.body.data.skill.split(",");
    console.log('1111111111111111111111');
    console.log(candidate_skill);

    if(candidate_id !='' && skill !='' ){

        var i=0;                       
        candidate_skill.forEach( async function(skl){

            var candidate_skill_details =await sequelize.query("SELECT * from candidate_skill where candidate_id = "+candidate_id+" and skill = '"+skl.trim()+"'",{ type: Sequelize.QueryTypes.SELECT });

            if(candidate_skill_details.length <= 0){

                models.candidate_skill.create({
                    candidate_id: candidate_id,
                    skill:skl.trim(),
                    createdBy: candidate_id,                            
                });
            }
                
            i++;		
        }, this);

        res.status(200).send({ success: true, message: "Candidate skill successfully added" });

    }else{
        res.status(200).json({ success: false, message: "All fileds are required!"});
    } 
}

/************************* candidate skill upload ends *******************************/

/************************* candidate skill delete start *******************************/

exports.candidate_skill_delete = function(req, res, next) {
    console.log(req.body.data);
    var skill_id = req.body.data.skill_id;

    if(skill_id !='' ){

        models.candidate_skill.destroy({ 
            where:{candidate_skill_id:skill_id}
        }).then(function(value) {
            res.status(200).send({ success: true, message: "Candidate skill successfully deleted" });
        });

    }else{
        res.status(200).json({ success: false, message: "Skill id is required!"});
    } 
}

/************************* candidate skill delete ends *******************************/

/************************* candidate looking for upload start *******************************/

exports.candidate_looking_for_upload = function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var desired_job_title = req.body.data.desired_job_title;
    var job_type = req.body.data.job_type;
    var salary = req.body.data.salary;
    var currency = req.body.data.currency;
    var category = req.body.data.category;
    var location = req.body.data.location;
    var looking_for_id = req.body.data.looking_for_id;

    if(candidate_id !='' && desired_job_title !='' && job_type !='' && salary !='' && category !='' && location !='' && currency && currency !='' ){
        console.log('11111111111111111111111');
        if(!looking_for_id){
            console.log('22222222222222222222222');
            models.candidate_looking_for.create({ 

                candidate_id: candidate_id,
                desired_job_title: desired_job_title,
                job_type: job_type,
                salary: salary,
                currency: currency,
                category: category,
                location: location,
                createdBy: candidate_id,

            }).then(function(candidate_looking_for) {

            res.status(200).send({ success: true, message: "Candidate looking for successfully added" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.candidate_looking_for.update({ 

                candidate_id: candidate_id,
                desired_job_title: desired_job_title,
                job_type: job_type,
                salary: salary,
                currency: currency,
                category: category,
                location: location,
                updatedBy: candidate_id,

            },{where:{candidate_looking_for_id:looking_for_id}}).then(function(candidate_looking_for) {

                res.status(200).send({ success: true, message: "Candidate looking for successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ success: false, message: "All fileds are required!"});
    } 
}

/************************* candidate looking for upload ends *******************************/


/************************* candidate address info upload start *******************************/

exports.candidate_address_info_upload = async function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    // var father_name = req.body.data.father_name;
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var address = req.body.data.address;
    var city = req.body.data.city;
    var state = req.body.data.state;
    var country = req.body.data.country;
    var zip_code = req.body.data.zip_code;
    var phn_no_country_code = req.body.data.phn_no_country_code;
    var phone_no = req.body.data.phone_no;
    // var alt_no_country_code = req.body.data.alt_no_country_code;
    // var alternate_no = req.body.data.alternate_no ? req.body.data.alternate_no : '';
    // var alternate_email_id = req.body.data.alternate_email_id;
    var website = req.body.data.website ? req.body.data.website : '';
    var address_info_id = req.body.data.address_info_id;

    var gender = req.body.data.gender;
    var nationality = req.body.data.nationality;

    if(candidate_id && candidate_id !='' && first_name && first_name !='' && last_name && last_name !='' && address && address !='' && zip_code && zip_code !='' && phn_no_country_code && phn_no_country_code !='' && phone_no && phone_no !='' && gender && gender !='' && nationality && nationality !=''){
        // var candidate_details = await sequelize.query("SELECT candidates.* from candidates where candidate_id = "+candidate_id,{ type: Sequelize.QueryTypes.SELECT });

        // if(candidate_details[0].first_name !='' && candidate_details[0].first_name!= null){
        //     var candidate_name = candidate_details[0].first_name+' '+candidate_details[0].last_name;
        // }else if(candidate_details[0].name !='' && candidate_details[0].name!= null){
        //     var candidate_name = candidate_details[0].name;
        // }else{
        //     var candidate_name = '';
        // }

        models.candidates.update({
            first_name: first_name,
            last_name: last_name,
            mob_country_code: phn_no_country_code,
            mobile: phone_no,
            gender: gender,
            nationality: nationality,
        },{where:{candidate_id:candidate_id}})

        console.log('999999999999999999')
        if(!address_info_id){
            console.log('22222222222222222222222');
            models.candidate_address_info.create({ 

                candidate_id: candidate_id,
                // candidate_name: candidate_name,
                // father_name: father_name,
                address: address,
                city: city,
                state: state,
                country: country,
                zip_code: zip_code,
                // phn_no_country_code: phn_no_country_code,
                // phone_no: phone_no,
                // alt_no_country_code: alt_no_country_code,
                // alternate_no: alternate_no,
                // email_id: candidate_details[0].email,
                // alternate_email_id: alternate_email_id,
                website: website,
                createdBy: candidate_id,

            }).then(function(candidate_address_info) {

            res.status(200).send({ success: true, message: "Candidate address info successfully added" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.candidate_address_info.update({ 

                candidate_id: candidate_id,
                // candidate_name: candidate_name,
                // father_name: father_name,
                address: address,
                city: city,
                state: state,
                country: country,
                zip_code: zip_code,
                // phn_no_country_code: phn_no_country_code,
                // phone_no: phone_no,
                // alt_no_country_code: alt_no_country_code,
                // alternate_no: alternate_no,
                // email_id: candidate_details[0].email,
                // alternate_email_id: alternate_email_id,
                website: website,
                updatedBy: candidate_id,

            },{where:{cand_address_info_id:address_info_id}}).then(function(candidate_address_info) {

                res.status(200).send({ success: true, message: "Candidate address info successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ success: false, message: "All fileds are required accept alternate no and website!"});
    } 
}

/************************* candidate address info upload ends *******************************/


/************************* candidate profile picture upload start *******************************/

exports.candidate_profile_pic_upload = function(req, res, next) {
    
    var candidate_id = req.body.data.candidate_id;
    var profile_pic = req.body.data.profile_pic;

    // console.log(req.body.data);

    
    if(candidate_id !='' && profile_pic !='' ){
            
        models.candidates.update({
            profile_picture: 'contents/candidates/'+ candidate_id +'/profile_picture.jpg',
        },{where:{candidate_id:candidate_id}})

        try {
            const path = './public/contents/candidates/'+ candidate_id +'/profile_picture.jpg';
            const imgdata = profile_pic;
            const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
            fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
        } catch (e) {
            next(e);
        }
        res.status(200).send({ success: "true", message: "Profile picture successfully uploaded", path:req.app.locals.baseurl+'contents/candidates/'+ candidate_id +'/profile_picture.jpg'});
        
    }else{
        res.status(200).json({ success: "false",message: "All fileds are required!"});
    }  
}

/************************* candidate profile picture upload ends *******************************/


/************************* candidate medical info upload start *******************************/

exports.candidate_medical_info_upload = function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var doh = req.body.data.doh;
    var doh_license_no = req.body.data.doh_license_no;
    var doh_license_title = req.body.data.doh_license_title;
    var dha = req.body.data.dha;
    var dha_license_no = req.body.data.dha_license_no;
    var dha_license_title = req.body.data.dha_license_title;
    var moh = req.body.data.moh;
    var moh_license_no = req.body.data.moh_license_no;
    var moh_license_title = req.body.data.moh_license_title;
    var other_country_name = req.body.data.other_country_name;
    var other_country_license_no = req.body.data.other_country_license_no;
    var other_country_license_title = req.body.data.other_country_license_title;
    var medical_info_id = req.body.data.medical_info_id;

    // if(candidate_id && candidate_id !='' && healthcare_license && healthcare_license !='' && healthcare_license_no && healthcare_license_no !='' ){
        if(candidate_id && candidate_id !='' ){
        console.log('11111111111111111111111');
        if(!medical_info_id){
            console.log('22222222222222222222222');
            models.candidate_medical_info.create({ 

                candidate_id: candidate_id,
                doh: doh,
                doh_license_no: doh_license_no,
                doh_license_title: doh_license_title,
                dha: dha,
                dha_license_no: dha_license_no,
                dha_license_title: dha_license_title,
                moh: moh,
                moh_license_no: moh_license_no,
                moh_license_title: moh_license_title,
                other_country_name: other_country_name,
                other_country_license_no: other_country_license_no,
                other_country_license_title: other_country_license_title,
                createdBy: candidate_id,

            }).then(function(candidate_medical_info) {

            res.status(200).send({ success: true, message: "Candidate medical info successfully added" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.candidate_medical_info.update({ 

                candidate_id: candidate_id,
                doh: doh,
                doh_license_no: doh_license_no,
                doh_license_title: doh_license_title,
                dha: dha,
                dha_license_no: dha_license_no,
                dha_license_title: dha_license_title,
                moh: moh,
                moh_license_no: moh_license_no,
                moh_license_title: moh_license_title,
                other_country_name: other_country_name,
                other_country_license_no: other_country_license_no,
                other_country_license_title: other_country_license_title,
                updatedBy: candidate_id,

            },{where:{candidate_medical_info_id:medical_info_id}}).then(function(candidate_medical_info) {

                res.status(200).send({ success: true, message: "Candidate medical info successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ success: false, message: "Candidate id is required!"});
    } 
}

/************************* candidate medical info upload ends *******************************/


/************************* candidate related(similar) job start *******************************/

exports.candidate_related_job = async function(req, res, next) {
    console.log(req.body.data);
    var candidate_id = req.body.data.candidate_id;
    var replacekey = '';

    if(candidate_id && candidate_id !='' ){
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaa');

        var candidate_category = await sequelize.query("SELECT candidate_looking_for.*, categories.title as category_title from candidate_looking_for LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.candidate_id ="+candidate_id ,{ type: Sequelize.QueryTypes.SELECT }); 
        var candidate_skill =await sequelize.query("SELECT candidate_skill.candidate_skill_id, candidate_skill.candidate_id, candidate_skill.skill from candidate_skill where candidate_id ="+candidate_id+" ORDER BY `candidate_skill_id` DESC",{ type: Sequelize.QueryTypes.SELECT });

        if(candidate_category.length > 0 || candidate_skill.length >0){
            console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbb');

            if(candidate_category.length > 0 && candidate_skill.length >0){
                console.log('cccccccccccccccccccccc');

                if(candidate_skill.length == 1){
                    console.log('ddddddddddddddddddd');
        
                    keystr = "jobs.skill_set LIKE '%"+candidate_skill[0].skill.trim()+"%'";
        
                }else{
                    console.log('eeeeeeeeeeeeeeeeeeeeeee');
                    var i=0; 
                    candidate_skill.forEach(function(skl){
        
                        replacekey += "jobs.skill_set LIKE '%"+candidate_skill[i].skill.trim()+"%' or ";
        
                        i++;
                    }, this);
        
                    var keystr = replacekey.slice(0,-4);
                }

                var related_job_with_category_and_skill = await sequelize.query("select jobs.job_id, jobs.job_title, jobs.currency, jobs.person_salary, jobs.basic_info_cuntry, jobs.basic_info_state, jobs.basic_info_city, jobs.job_category, jobs.skill_set FROM `jobs` where jobs.status='publish' and job_category ="+candidate_category[0].category+" and ("+keystr+") order by jobs.job_id DESC limit 4",{ type: Sequelize.QueryTypes.SELECT });

                if(related_job_with_category_and_skill.length > 0){
                    res.status(200).send({ status:200, success: true, related_job: related_job_with_category_and_skill });
                }else{
                    res.status(200).send({ status:200, success: false, message: "No job right now" });
                }

            }else if(candidate_category.length > 0){
                console.log('ffffffffffffffffffffffffffffff');

                var related_job_with_category = await sequelize.query("select jobs.job_id, jobs.job_title, jobs.currency, jobs.person_salary, jobs.basic_info_cuntry, jobs.basic_info_state, jobs.basic_info_city, jobs.job_category, jobs.skill_set FROM `jobs` where jobs.status='publish' and job_category ="+candidate_category[0].category+" order by jobs.job_id DESC limit 4",{ type: Sequelize.QueryTypes.SELECT });

                if(related_job_with_category.length > 0){
                    res.status(200).send({ status:200, success: true, related_job: related_job_with_category });
                }else{
                    res.status(200).send({ status:200, success: false, message: "No job right now" });
                }

            }else if(candidate_skill.length >0){
                console.log('gggggggggggggggggggggggg');

                if(candidate_skill.length == 1){
                    console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhh');
        
                    keystr = "jobs.skill_set LIKE '%"+candidate_skill[0].skill.trim()+"%'";
        
                }else{
                    console.log('iiiiiiiiiiiiiiiiiiiiiiii');
                    var i=0; 
                    candidate_skill.forEach(function(skl){
        
                        replacekey += "jobs.skill_set LIKE '%"+candidate_skill[i].skill.trim()+"%' or ";
        
                        i++;
                    }, this);
        
                    var keystr = replacekey.slice(0,-4);
                }

                var related_job_with_skill = await sequelize.query("select jobs.job_id, jobs.job_title, jobs.currency, jobs.person_salary, jobs.basic_info_cuntry, jobs.basic_info_state, jobs.basic_info_city, jobs.job_category, jobs.skill_set FROM `jobs` where jobs.status='publish' and "+keystr+" order by jobs.job_id DESC limit 4",{ type: Sequelize.QueryTypes.SELECT });

                if(related_job_with_skill.length > 0){
                    res.status(200).send({ status:200, success: true, related_job: related_job_with_skill });
                }else{
                    res.status(200).send({ status:200, success: false, message: "No job right now" });
                }
            }
            
        }else{
            res.status(200).send({ status:200, success: false, message: "No job right now" });
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
    } 
}

/************************* candidate related(similar) job ends *******************************/


function calcDate(date1,date2) {
    var diff = Math.floor(date1.getTime() - date2.getTime());
    var day = 1000 * 60 * 60 * 24;

    var days = Math.floor(diff/day);
    var months = Math.floor(days/31);
    var years = Math.floor(months/12);

    /*var message = date2.toDateString();
    message += " was "
    message += days + " days " 
    message += months + " months "
    message += years + " years ago \n"*/
    var message = years + "||" + months.toString();
    return message;
}
