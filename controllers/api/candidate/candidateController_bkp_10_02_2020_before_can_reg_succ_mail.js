var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);

/*************************candidate sign-up start *******************************/

exports.candidate_registration = async function(req, res, next) {

    console.log(req.body.data);
    
    var password = req.body.data.password;
    var hash = bcrypt.hashSync(password);
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var email = req.body.data.email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var terms = req.body.data.terms;
    var category_id = req.body.data.category_id;

    
    if(first_name !='' && last_name !='' && email !='' && mobile !='' && password !='' && mob_country_code !='' && category_id !=''){

        var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
        var check_mobile =await sequelize.query("SELECT * FROM candidates  WHERE mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

        var email_msg='';
        var mobile_msg='';
        if(check_email.length>0){
            email_msg= "Email ID already exists!";
        }if (check_mobile.length>0){
            mobile_msg= "Mobile number already exists!";
        }

        if(check_email.length==0 && check_mobile.length==0){
            console.log('111111111111111111111111111');

            models.candidates.create({
                
                first_name : first_name,
                last_name : last_name,
                email : email,
                username : email,
                mob_country_code : mob_country_code,
                mobile : mobile,
                password : hash,
                type : "candidate", 
                terms : terms      
                
            }).then(async function(candidate) { 

                if(candidate){
                    console.log('222222222222222222222222');
                    models.candidate_looking_for.create({
                        candidate_id: candidate.candidate_id,
                        category : category_id,
                    }).then(async function(candidate_looking_for){
            
                        var dir = './public/contents/candidates/'+candidate.candidate_id; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }

                        var candidate_full_data =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate.candidate_id,{ type: Sequelize.QueryTypes.SELECT });

                        var token = jwt.sign({candidate}, SECRET, { expiresIn: 18000 });
                        res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  });         
                    });
                }else{
                    res.status(200).json({ success: "false",message: "Registration failed!"});
                }

                // var dir = './public/contents/candidates/'+candidate.candidate_id; 
                // console.log(dir);
                // if (!fs.existsSync(dir)){
                //     fs.mkdirSync(dir);                  
                // }
                // var token = jwt.sign({candidate}, SECRET, { expiresIn: 18000 });

                
                // res.status(200).send({ success: "true", message: "User successfully registered. You can login now.",token:token, details:candidate  });         
            })
        }else{
            res.status(200).json({ success: "false", message: "Email or mobile already already exists!" });
        }
    }else{
        res.status(200).json({ success: "false",message: "All fileds are required!"});
    }  
}

/*************************candidate sign-up ends *******************************/

/*************************candidate sign-in start *******************************/

exports.candidate_signin = async function(req, res, next) {

    var username = req.body.data.username;
    var password = req.body.data.password;
   //var hash = bcrypt.hashSync(password);

    //var candidate =await sequelize.query("SELECT candidates.*, candidate_looking_for.category from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id where candidates.username ='"+username+"'",{ type: Sequelize.QueryTypes.SELECT });
    var candidate =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.username ='"+username+"'",{ type: Sequelize.QueryTypes.SELECT });

    //models.candidates.findOne({ where: {username :username} }).then(function(candidate) {
        if(candidate.length > 0){
            if(!bcrypt.compareSync(password, candidate[0].password)){
                res.status(200).send({ success: "false", message: "Invalid username and password!" });
            }else{
                var token =    jwt.sign({candidate}, SECRET, { expiresIn: 18000 });
                res.status(200).send({ success: "true", message:"successfully login", candidatedetail:candidate[0], token: token }); 
            }
            
        }else{				
            res.status(200).send({ success: "false", message: "Invalid username and password!" });
        }  

    //});
   
    // models.candidates.findOne({ where: {username :username} }).then(function(candidate) {
    //     if(candidate!=null){
    //         cand = candidate.toJSON();	   
    //         if(!bcrypt.compareSync(password, cand.password)) {		   
    //             res.status(200).send({ message: "Invalid username and password!" });
    //         } else {	  
    //             if (username == candidate.username) {              
    //             var token =    jwt.sign({candidate}, SECRET, { expiresIn: 18000 });
    //             res.status(200).send({ success: "true", message:"successfully login", candidatedetail:candidate, token: token });                   
    //             }else{				
    //             res.status(200).send({ message: "Invalid username and password!" });
    //             }            
    //         }//password check end                 
    //     }else{                
    //         res.status(200).send({ message: "No user found" });
    //     }    
    // })
    // .catch(function(error) {
    //     return res.send(error);        
    // }); 

}

/*************************candidate sign-in ends *******************************/

/************************* candidate category list api start *******************************/

exports.candidate_category_list = async function(req, res, next) {

    var category_list = await sequelize.query("SELECT categories.category_id, categories.title, (SELECT COUNT(*) FROM candidate_looking_for WHERE categories.category_id=candidate_looking_for.category) as candidate_count_by_category FROM categories where categories.status='active' order by categories.title ASC",{ type: Sequelize.QueryTypes.SELECT });
    var location_list = await sequelize.query("SELECT COUNT(*) as candidate_count_by_location, location as candidate_location FROM candidate_looking_for GROUP BY location",{ type: Sequelize.QueryTypes.SELECT });
    
    //if(category_list){
        res.status(200).send({ success: true, category_list: category_list, location_list: location_list});
    // }else{
    //     res.status(200).send({ message: "No category found" });
    // }
    
}

/************************* candidate category list api ends *******************************/


/************************* candidate list api start *******************************/

exports.candidateList = async function(req, res, next) {
    console.log(req.body.data);

    var category_id = req.body.data.category_id ? req.body.data.category_id : '';
    var job_location = req.body.data.job_location ? req.body.data.job_location : '';
    var resultArray = [];

    if(job_location !='' && category_id !=''  ){
        console.log('111111111111111111111111111');
        
        var candidate_list_by_cat_and_loc = await sequelize.query("SELECT candidate_looking_for.candidate_id, candidate_looking_for.salary, candidate_looking_for.currency, candidate_looking_for.desired_job_title, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, categories.title as category_title, (SELECT SUM(candidate_experience_details.experience_in_year) FROM `candidate_experience_details` WHERE `candidate_experience_details`.candidate_id = candidate_looking_for.candidate_id) as experience_in_year, (SELECT GROUP_CONCAT(candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = candidates.candidate_id) as candidate_skill from candidate_looking_for LEFT JOIN candidates ON candidate_looking_for.candidate_id = candidates.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.category = "+category_id+" and candidate_looking_for.location = '"+job_location+"'",{ type: Sequelize.QueryTypes.SELECT });
        
        if(candidate_list_by_cat_and_loc.length > 0 ){
            console.log('2222222222222222222222');

            var i = 0;
            candidate_list_by_cat_and_loc.forEach(function(element,index){ 

                if(element.first_name !='' && element.first_name!= null){
                    var candidate_name = element.first_name+' '+element.last_name;
                }else if(element.name !='' && element.name!= null){
                    var candidate_name = element.name;
                }else{
                    var candidate_name = '';
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var candidate_profile_picture = element.provider_image;
                }else{
                    var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "candidate_id":element.candidate_id,
                    "desired_job_title":element.desired_job_title,
                    "name":candidate_name,
                    "candidate_picture":candidate_profile_picture,
                    "experience_in_year":element.experience_in_year,
                    "salary":element.salary,
                    "currency":element.currency,
                    "category_title":element.category_title,
                    "candidate_skill": element.candidate_skill
                });    
                i++;
                // if(i== candidate_list_by_cat_and_loc.length){
                //     res.status(200).send({ success: true, candidate_list: resultArray });
                // }                
            });

            res.status(200).send({ success: true, candidate_list: resultArray });
        }else{
            res.status(200).send({ success: false, message: "No candidate found" });
        }

    }else if(job_location =='' && category_id !='' ){
        console.log('333333333333333333');

        // var candidate_list = await sequelize.query("SELECT candidate_looking_for.candidate_id, candidate_looking_for.desired_job_title, candidates.name, candidate_experience_details.experience_in_year from candidate_looking_for LEFT JOIN candidates ON candidate_looking_for.candidate_id = candidates.candidate_id LEFT JOIN candidate_experience_details ON candidate_looking_for.candidate_id = candidate_experience_details.candidate_id where category = "+category_id,{ type: Sequelize.QueryTypes.SELECT });
        // var candidate_list = await sequelize.query("SELECT candidate_looking_for.candidate_id, candidate_looking_for.desired_job_title, candidates.name, (SELECT SUM(candidate_experience_details.experience_in_year) FROM `candidate_experience_details` WHERE `candidate_experience_details`.candidate_id = candidate_looking_for.candidate_id) as experience_in_year from candidate_looking_for LEFT JOIN candidates ON candidate_looking_for.candidate_id = candidates.candidate_id where candidate_looking_for.category = "+category_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_list_by_category = await sequelize.query("SELECT candidate_looking_for.candidate_id, candidate_looking_for.salary, candidate_looking_for.currency, candidate_looking_for.desired_job_title, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, categories.title as category_title, (SELECT SUM(candidate_experience_details.experience_in_year) FROM `candidate_experience_details` WHERE `candidate_experience_details`.candidate_id = candidate_looking_for.candidate_id) as experience_in_year, (SELECT GROUP_CONCAT(candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = candidates.candidate_id) as candidate_skill from candidate_looking_for LEFT JOIN candidates ON candidate_looking_for.candidate_id = candidates.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.category = "+category_id,{ type: Sequelize.QueryTypes.SELECT });
        
        if(candidate_list_by_category.length > 0){
            console.log('4444444444444444');

            var i = 0;
            candidate_list_by_category.forEach(function(element,index){ 

                if(element.first_name !='' && element.first_name!= null){
                    var candidate_name = element.first_name+' '+element.last_name;
                }else if(element.name !='' && element.name!= null){
                    var candidate_name = element.name;
                }else{
                    var candidate_name = '';
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var candidate_profile_picture = element.provider_image;
                }else{
                    var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "candidate_id":element.candidate_id,
                    "desired_job_title":element.desired_job_title,
                    "name":candidate_name,
                    "candidate_picture":candidate_profile_picture,
                    "experience_in_year":element.experience_in_year,
                    "salary":element.salary,
                    "currency":element.currency,
                    "category_title":element.category_title,
                    "candidate_skill": element.candidate_skill
                });    
                i++;
                // if(i== candidate_list_by_category.length){
                //     res.status(200).send({ success: true, candidate_list: resultArray });
                // }                
            }); 

            res.status(200).send({ success: true, candidate_list: resultArray });
        }else{
            res.status(200).send({ success: false, message: "No candidate found" });
        }

    }else if(job_location !='' && category_id =='' ){
        console.log('5555555555555555555');

        var candidate_list_by_location = await sequelize.query("SELECT candidate_looking_for.candidate_id, candidate_looking_for.salary, candidate_looking_for.currency, candidate_looking_for.desired_job_title, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, categories.title as category_title, (SELECT SUM(candidate_experience_details.experience_in_year) FROM `candidate_experience_details` WHERE `candidate_experience_details`.candidate_id = candidate_looking_for.candidate_id) as experience_in_year, (SELECT GROUP_CONCAT(candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = candidates.candidate_id) as candidate_skill from candidate_looking_for LEFT JOIN candidates ON candidate_looking_for.candidate_id = candidates.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.location = '"+job_location+"'",{ type: Sequelize.QueryTypes.SELECT });
        
        if(candidate_list_by_location.length > 0){
            console.log('66666666666666666666666');

            var i = 0;
            candidate_list_by_location.forEach(function(element,index){ 

                if(element.first_name !='' && element.first_name!= null){
                    var candidate_name = element.first_name+' '+element.last_name;
                }else if(element.name !='' && element.name!= null){
                    var candidate_name = element.name;
                }else{
                    var candidate_name = '';
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var candidate_profile_picture = element.provider_image;
                }else{
                    var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "candidate_id":element.candidate_id,
                    "desired_job_title":element.desired_job_title,
                    "name":candidate_name,
                    "candidate_picture":candidate_profile_picture,
                    "experience_in_year":element.experience_in_year,
                    "salary":element.salary,
                    "currency":element.currency,
                    "category_title":element.category_title,
                    "candidate_skill": element.candidate_skill
                });    
                i++;
                // if(i== candidate_list_by_location.length){
                //     res.status(200).send({ success: true, candidate_list: resultArray });
                // }                
            });

            res.status(200).send({ success: true, candidate_list: resultArray });
        }else{
            res.status(200).send({ success: false, message: "No candidate found" });
        }

    }else{
        console.log('777777777777777777');
        res.status(200).send({ success: false, message: "Please provide category or location" });
    }


    // var candidates_list = await sequelize.query("select a.first_name, a.last_name, designation, b.experience_in_year, b.experience_in_month from candidates as a left join candidate_experience_details as b on b.candidate_id=a.candidate_id",{ type: Sequelize.QueryTypes.SELECT });
    
    // if(candidates_list){
    //     res.status(200).send({ success: "true", details: candidates_list });
    // }else{
    //     res.status(200).send({ message: "No clients found" });
    // }
    
}

/************************* candidate list api ends *******************************/


/************************* candidate details api start *******************************/

exports.candidate_details = async function(req, res, next) {

    var candadite_id = req.body.data.candadite_id;
    if(candadite_id !=''){

        var candidate_personal_details = await sequelize.query("SELECT candidates.candidate_id as candidate_id, candidates.name as candidate_name, candidates.email as candidate_email, candidates.profile_picture, candidates.provider_image, candidates.cv_name, candidate_address_info.father_name, candidate_address_info.address, candidate_address_info.zip_code, candidate_address_info.phone_no, candidate_address_info.alternate_no, candidate_address_info.website from candidates LEFT JOIN candidate_address_info ON candidates.candidate_id = candidate_address_info.candidate_id where candidates.candidate_id = "+candadite_id,{ type: Sequelize.QueryTypes.SELECT });
        // var candidate_personal_details = await sequelize.query("SELECT candidates.candidate_id as candidate_id, candidates.name as candidate_name, candidates.email as candidate_email, candidates.profile_picture, candidates.provider_image, (SELECT candidate_experience_details.job_title from candidate_experience_details where candidate_experience_details.candidate_id = "+candadite_id+" and candidate_experience_details.is_current_employer =1)  as candidate_job_title, candidate_address_info.father_name, candidate_address_info.address, candidate_address_info.zip_code, candidate_address_info.phone_no, candidate_address_info.alternate_no, candidate_address_info.website from candidates LEFT JOIN candidate_address_info ON candidates.candidate_id = candidate_address_info.candidate_id where candidates.candidate_id = "+candadite_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_qualification_details = await sequelize.query("SELECT candidate_qualification.candidate_id, candidate_qualification.qualification_type, candidate_qualification.school_college, candidate_qualification.from_year, candidate_qualification.to_year, candidate_qualification.specialization, candidate_qualification.marks_grade from candidate_qualification where candidate_id = "+candadite_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_last_job_title = await sequelize.query("SELECT candidate_experience_details.job_title from candidate_experience_details where candidate_experience_details.candidate_id = "+candadite_id+" and candidate_experience_details.is_current_employer =1",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_experience_details = await sequelize.query("SELECT candidate_experience_details.* from candidate_experience_details where candidate_experience_details.candidate_id ="+candadite_id+" ORDER BY `cand_experience_details_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_language_details = await sequelize.query("SELECT candidate_language.candidate_language_id, candidate_language.candidate_id, candidate_language.language, candidate_language.fluency from candidate_language where candidate_id ="+candadite_id+" ORDER BY `candidate_language_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_skill_details = await sequelize.query("SELECT candidate_skill.candidate_skill_id, candidate_skill.candidate_id, candidate_skill.skill from candidate_skill where candidate_id ="+candadite_id+" ORDER BY `candidate_skill_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_looking_for = await sequelize.query("SELECT candidate_looking_for.*, categories.title as category_title from candidate_looking_for LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidate_looking_for.candidate_id ="+candadite_id,{ type: Sequelize.QueryTypes.SELECT });

        if(candidate_personal_details[0].profile_picture !='' && candidate_personal_details[0].profile_picture != null){
            var profile_picture = req.app.locals.baseurl+candidate_personal_details[0].profile_picture;
        }else if(candidate_personal_details[0].provider_image !='' && candidate_personal_details[0].provider_image != null){
            var profile_picture = candidate_personal_details[0].provider_image;
        }else{
            // var profile_picture = 'contents/no_image_redford.jpg';
            var profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
        }
        
        if(candidate_personal_details[0].cv_name !='' && candidate_personal_details[0].cv_name != null){
            var path= req.app.locals.baseurl+candidate_personal_details[0].cv_name;
        }else{
            var path='';
        }
        
        if(candidate_personal_details.length > 0){
            res.status(200).send({ success: true, personal_details: candidate_personal_details, qualification_details:candidate_qualification_details, candidate_profile_picture:profile_picture, candidate_last_job_title:candidate_last_job_title[0].job_title, candidate_cv_path: path, candidate_experience_details:candidate_experience_details, candidate_language_details:candidate_language_details, candidate_skill_details:candidate_skill_details, candidate_looking_for:candidate_looking_for });
        }else{
            res.status(200).send({ success: false, message: "No candidate found" });
        }

    }else{
        res.status(200).json({ success: false, message: "Candidate id is required!"});
    }
    
}

/************************* candidate details api ends *******************************/

/************************* candidate provider registration start *******************************/

exports.candidate_provider_registration =async function(req, res, next) {
    
    var email = req.body.email;
    var provider_id = req.body.id;
    var provider = req.body.provider;
    var provider_image = req.body.image;
    var provider_name = req.body.name;
    
    if(email !='' && provider_id !='' && provider !='' && provider_image !='' && provider_name !=''){
        var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

        var email_msg='';
        if(check_email.length>0){
            email_msg= "Email ID already exists!";
        }

        if(check_email.length==0){
            models.candidates.create({
                email : email,
                username : email,
                name : provider_name,
                provider_id : provider_id,
                provider : provider,
                provider_image : provider_image,
                type : "candidate"
            }).then(function(candidate){
                if(candidate){
                    var dir = './public/contents/candidates/'+candidate.candidate_id; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }
                    var token = jwt.sign({candidate}, SECRET, { expiresIn: 18000 });
                    res.status(200).send({ success: "true", message: "Candidate successfully register",token:token, details:candidate  });
                }else{
                    res.status(200).json({ success: "false",message: "Registration failed!"});
                }
           
            });
        }else{
            res.status(200).json({ success: "false",email_msg:email_msg});
        }
 
    }else{
        res.status(200).json({ success: "false",message: "All fileds are required!"});
    }  
}

/************************* candidate provider registration ends *******************************/


/************************* candidate provider sign-in start *******************************/

exports.candidate_provider_signin = async function(req, res, next) {

    console.log('11111111111111111111')
    console.log(req.body)
    console.log('2222222222222222222')

    var username = req.body.email;
    var provider_id = req.body.id;
    var provider_image = req.body.image;

    if(username !='' && provider_id !=''){

        var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username='"+username+"' and provider_id="+provider_id+" limit 1",{ type: Sequelize.QueryTypes.SELECT });
        if(check_email.length > 0 ){
            //candidate = await models.candidates.findOne({ where: {username :username, provider_id :provider_id} }).then(function(candidates) {
            console.log("-------------------------------------------------------------------------------------------")
            candidates = await sequelize.query("select a.*, b.category from candidates as a left join candidate_looking_for as b on b.candidate_id = a.candidate_id where a.username='"+username+"' and a.provider_id='"+provider_id+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
            if(candidates!=null){
                if(candidates[0].provider_image == provider_image){
                    var token =    jwt.sign({candidates}, SECRET, { expiresIn: 18000 });
                    res.status(200).send({ success: "true", token: token, details:candidates[0] }); 
                }else{

                    models.candidates.update({ 
                        provider_image: provider_image,
                    },{where:{candidate_id:candidates[0].candidate_id}}).then(async function(candidates_update) {

                        update_candidates = await sequelize.query("select a.*, b.category from candidates as a left join candidate_looking_for as b on b.candidate_id = a.candidate_id where a.username='"+username+"' and a.provider_id='"+provider_id+"'",{ type: Sequelize.QueryTypes.SELECT });
                        if(update_candidates != null) {
                            var token = jwt.sign({candidates}, SECRET, { expiresIn: 18000 });
                            res.status(200).send({ success: "true", token: token, details:update_candidates[0] });
                        }
                        // models.candidates.findOne({ where: {username :username, provider_id :provider_id} }).then(function(update_candidates) {
                        //     var token =    jwt.sign({candidates}, SECRET, { expiresIn: 18000 });
                        //     res.status(200).send({ success: "true", token: token, details:update_candidates });
                        // })
                        // .catch(function(error) {
                        //     return res.send(error);        
                        // });
                    })
                    .catch(function(error) {
                        return res.send( {status:200, success: "false", message: "error happened"});
                    });
                }
            }else{                
                res.status(200).send({ message: "Candidate not found" });
            } 
        }else{
            res.status(200).json({ success: "false", message: "Candidate not registered!" });
        }    
    }else{
        res.status(200).send({ message: "All fields are required!" });
    } 
}
/************************* candidate provider sign-in ends *******************************/


/************************* Check candidate category exists or not  *******************************/

exports.check_candidate_category = async function(req, res, next) {
 
    var candidate_id = req.body.data.candidate_id;
    var candidate_category = '';
    if(candidate_id !=''){
        //var candidateDetails = await sequelize.query("SELECT * FROM clients  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_details = await models.candidate_looking_for.findOne({where:{candidate_id:candidate_id},attributes: ['category']});
        if(candidate_details != null) 
            return res.send( {status:200, success: true, candidate_category:candidate_details.category});
        else 
        return res.send( {status:200, success: true, candidate_category:'no catgeory'});
    }else{
        return res.send( {status:200, success: false, candidate_category:'no catgeory'});
    } 
}
/************************* Check candidate category exists or not *******************************/


/************************* provider candidate update start **************************************/
exports.provider_candidate_update = async function(req, res, next) {

    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var category_id = req.body.data.category_id;
    var candidate_id = req.body.data.candidate_id;

    if(candidate_id && candidate_id !=''){
         
        provider_candidate = await sequelize.query("select candidates.* from candidates where candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
            
        if(provider_candidate.length > 0){

            if(mob_country_code && mob_country_code !='' && mobile && mobile !='' && category_id && category_id !=''){
               
                    models.candidates.update({ 
                        mob_country_code: mob_country_code,
                        mobile: mobile,

                    },{where:{candidate_id:candidate_id}}).then(async function(candidates_update) {

                        if(candidates_update){

                            candidate_looking_for = await sequelize.query("select candidate_looking_for.* from candidate_looking_for where candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
                            if(candidate_looking_for.length > 0){

                                models.candidate_looking_for.update({ 
                                    category: category_id,
            
                                },{where:{candidate_id:candidate_id}}).then(async function(candidate_looking_for_update) {

                                    var candidate_full_data =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
                                    var token = jwt.sign({candidates_update}, SECRET, { expiresIn: 18000 });
                                    res.status(200).send({ success: "true", message:"Candidate update successful", candidatedetail:candidate_full_data[0], token: token });
                                })

                            }else{

                                models.candidate_looking_for.create({
                                    candidate_id: candidate_id,
                                    category : category_id,
                                }).then(async function(candidate_looking_for){
                        
                                    var candidate_full_data =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
                                    var token = jwt.sign({candidates_update}, SECRET, { expiresIn: 18000 });
                                    res.status(200).send({ success: "true", message:"Candidate update successful", candidatedetail:candidate_full_data[0], token: token }); 
                                });
                            }

                        }else{
                            res.status(200).json({status:200, success: "false",message: "Candidate update failed!"});
                        }
                    })
                    .catch(function(error) {
                        return res.send( {status:200, success: "false", message: "error happened"});
                    });
            }else{
                res.status(200).send({status:200, success: "false", message: "All fields are required!" });
            }
        }else{                
            res.status(200).send({status:200, success: "false", message: "Candidate not registered" });
        }     
    }else{
        res.status(200).send({status:200, success: "false", message: "Candidate id is required!" });
    } 
}
/************************* provider candidate update ends *******************************/


/************************* candidate password update start *******************************/

exports.candidate_password_update = async function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var current_password = req.body.data.current_password;
    var new_password = req.body.data.new_password;
   
    if(candidate_id && candidate_id !=''){
        console.log('11111111111111111111');

        var candidate_details = await sequelize.query("SELECT candidates.* from candidates where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });

        if(candidate_details.length > 0){
            console.log('22222222222222222222222');

            if(current_password && current_password !='' && new_password && new_password !='' ){
                console.log('555555555555555555');

                if(!bcrypt.compareSync(current_password, candidate_details[0].password)){
                    res.status(200).send({ status:200, success: false, message: "Current password does not match!" });
                }else{
                    var hash = bcrypt.hashSync(new_password);
                    models.candidates.update({ 

                        password : hash,
    
                    },{where:{candidate_id:candidate_id}}).then(function(candidates) {
                        res.status(200).send({ status:200, success: true, message: "Password is updated successfully" });
                    })
                }

            }else{
                res.status(200).json({ status:200, success: false, message: "All fields are required!"});
            }

        }else{
            res.status(200).json({ status:200, success: false, message: "Candidate not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
    }    
}


/************************* candidate password update ends *******************************/


/************************* candidate preferred job listing start *******************************/

exports.candidate_preferred_job_listing = async function(req, res, next) {
    
    // if(typeof req.body.data.job_category_id !== 'undefined') {
        var candidate_id = req.body.data.candidate_id;
        var resultArray = [];
        if(candidate_id && candidate_id != '') {
            // var preferred_job_listing = await sequelize.query("SELECT b.job_title, b.date_opened, c.company_name, b.person_salary, b.basic_info_country, b.basic_info_state, b.basic_info_city, b.job_type from candidate_favourite_job as a " +
            //                                                   "left join jobs as b on b.job_id=a.job_id " + 
            //                                                   "left join companies on c.company_id=a.company_id " +
            //                                                   "where candidate_id="+candidate_id,{ type: Sequelize.QueryTypes.SELECT })

            var preferred_job_listing = await sequelize.query("SELECT a.candidate_favourite_job_id, b.*, c.company_name, c.logo from candidate_favourite_job as a left join jobs as b on b.job_id=a.job_id left join companies as c on c.company_id=b.company_id where candidate_id="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
            if(preferred_job_listing.length > 0) {

                var i = 0;
                preferred_job_listing.forEach(function(element,index){ 
    
                    if(element.logo !='' && element.logo!= null){
                        var company_logo = req.app.locals.baseurl+element.logo;
                    }else{
                        var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                    }
    
                    resultArray.push({
                        "job_id": element.job_id,
                        "job_title": element.job_title,
                        "job_type": element.job_type,
                        "basic_info_cuntry": element.basic_info_cuntry,
                        "basic_info_state": element.basic_info_state,
                        "basic_info_city": element.basic_info_city,
                        "job_category": element.job_category,
                        "person_salary": element.person_salary,
                        "client_id": element.client_id,
                        "job_description": element.job_description,
                        "skill_set": element.skill_set,
                        "createdAt": element.createdAt,
                        "company_id": element.company_id,
                        "company_name": element.company_name,
                        "company_logo": company_logo,
                        "candidate_favourite_job_id": element.candidate_favourite_job_id
                    });    
                    i++;             
                });

                res.status(200).send({ status:200, success: true, preferred_job_listing:resultArray}); 
            } else {
                res.status(200).send({ status:200, success: false, message:"No preferred job found for this candidate."}); 
            }                                                            
        }else{
            res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
        } 
    // }   
}
/************************* candidate preferred job listing ends *******************************/


/************************* candidate preferred job delete start *******************************/

exports.candidate_preferred_job_delete = function(req, res, next) {
    console.log(req.body.data);
    var candidate_favourite_job_id = req.body.data.candidate_favourite_job_id;

    if(candidate_favourite_job_id !='' ){

        models.candidate_favourite_job.destroy({ 
            where:{candidate_favourite_job_id:candidate_favourite_job_id}
        }).then(function(value) {
            res.status(200).send({ status:200, success: true, message: "Candidate preferred job successfully deleted" });
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate preferred job id is required!"});
    } 
}

/************************* candidate preferred job delete ends *******************************/


/************************* candidate applied job listing start *******************************/

exports.candidate_applied_job_listing = async function(req, res, next) {

    var candidate_id = req.body.data.candidate_id;
    var resultArray = [];
    if(candidate_id && candidate_id != '') {

        var applied_job_listing = await sequelize.query("SELECT b.*, c.company_name, c.logo from candidate_job_applied as a left join jobs as b on b.job_id=a.job_id left join companies as c on c.company_id=b.company_id where candidate_id="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
        if(applied_job_listing.length > 0) {

            var i = 0;
            applied_job_listing.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo
                });    
                i++;             
            });

            res.status(200).send({ status:200, success: true, applied_job_listing:resultArray}); 
        } else {
            res.status(200).send({ status:200, success: false, message:"No applied job found for this candidate."}); 
        }                                                            
    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
    }  
}
/************************* candidate applied job listing ends *******************************/



/************************* candidate add-edit start *******************************/

exports.candidate_addedit = async function(req, res, next) {

    console.log(req.body.data);
    
    var password = req.body.data.password;
    var hash = bcrypt.hashSync(password);
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var email = req.body.data.email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var category_id = req.body.data.category_id;

    if(first_name && first_name !='' && last_name && last_name !='' && email && email !='' && mobile && mobile !='' && password && password !='' && mob_country_code && mob_country_code !='' && category_id && category_id !=''){

        var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
        var check_mobile =await sequelize.query("SELECT * FROM candidates  WHERE mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

        var email_msg='';
        var mobile_msg='';
        if(check_email.length>0){
            email_msg= "Email ID already exists!";
        }if (check_mobile.length>0){
            mobile_msg= "Mobile number already exists!";
        }

        if(check_email.length==0 && check_mobile.length==0){
            console.log('111111111111111111111111111');

            models.candidates.create({
                
                first_name : first_name,
                last_name : last_name,
                email : email,
                username : email,
                mob_country_code : mob_country_code,
                mobile : mobile,
                password : hash,
                type : "candidate",
                
            }).then(async function(candidate) { 

                if(candidate){
                    console.log('222222222222222222222222');
                    models.candidate_looking_for.create({
                        candidate_id: candidate.candidate_id,
                        category : category_id,
                    }).then(async function(candidate_looking_for){
            
                        var dir = './public/contents/candidates/'+candidate.candidate_id; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }

                        //var candidate_full_data =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate.candidate_id,{ type: Sequelize.QueryTypes.SELECT });

                        res.status(200).send({ status:200, success: true, message: "Candidate successfully created." });         
                    });
                }else{
                    res.status(200).json({ status:200, success: false,message: "Candidate creation failed"});
                }

            })
        }else{
            res.status(200).json({ status:200, success: false, message: "Email or mobile already exists!" });
        }

    }else{
        res.status(200).json({ status:200, success: false,message: "All fileds are required!"});
    } 
}

/************************* candidate add-edit ends *******************************/



/************************* candidate delete start *******************************/

exports.candidate_delete = function(req, res, next) {
    console.log(req.body.data);
    var candidate_id = req.body.data.candidate_id;

    if(candidate_id && candidate_id !='' ){

        models.candidates.destroy({ 
            where:{candidate_id:candidate_id}
        }).then(function(value) {

            if(value){
                models.candidate_looking_for.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_address_info.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_experience_details.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_favourite_job.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_job_applied.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_language.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_medical_info.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_qualification.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_shortlisted_job.destroy({ 
                    where:{candidate_id:candidate_id}
                })

                models.candidate_skill.destroy({ 
                    where:{candidate_id:candidate_id}
                })
                    res.status(200).send({ status:200, success: true, message: "Candidate successfully deleted" });
               
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Candidate not successfully deleted" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
    } 
}

/************************* candidate delete ends *******************************/



/************************* candidate forgot password start *******************************/

exports.candidate_forgot_password =async function(req, res, next) {
    console.log(req.body.data);
    
    var email = req.body.data.email;
    var type = req.body.data.type;
    
    if( email && email !='' && type && type !=''){
        console.log('1111111111111111111');
        var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

        if(check_email.length > 0){
            console.log('222222222222222222');
            var random_otp = Math.floor(1000 + Math.random() * 9000); 

            models.forgot_password.create({
                email : email,
                type : type,
                otp : random_otp,
            }).then(function(candidate){
                console.log('3333333333333');

                if(candidate){
                    console.log('44444444444444444444');
                    return new Promise((resolve, reject) => {
                        console.log('5555555555555555');
                        var data = {
                            from: 'Redford User <me@samples.mailgun.org>',
                            to: [email],
                            subject: 'Reset password',
                            html: '<!DOCTYPE html>'+
                            '<html>'+    
                            '<head>'+
                            '</head>'+    
                            '<body>'+
                                '<div>'+
                                    '<p>Hello Sir/Madam,</p>'+
                                    '<p>Your reset password code is: </p>'+ 
                                    '<h3> <b>'+random_otp+'</b></h3>'+  
                                    '<p>For any kind of query reach us at: <br />'+
                                    'Mail: redford@gmail.com<br />'+
                                    'Phone: +91 9876543210<br />'+
                                    'Have a nice day.</p>'+
                                    '<p>Thanks.</p>'+
                                    '<p>Regards,<br />'+
                                    'redford recruiters team<br />'+
                                    'UAE</p>'+								
                                '</div>'+       
                            '</body>'+    
                            '</html>' 
                        };
                        mailgun.messages().send(data, function (error, body) {
                            console.log('6666666666666');
                            console.log(body);
                            if (error) {
                                return reject(res.status(200).send({ status:200, success: true, message: "Somthing went wrong. Email dose not send.", error:error}));
                            }
                            return resolve(res.status(200).send({ status:200, success: true, message: "A code has been sent to your provided email id. Please check your email.", body:body}));
                        });
                        console.log('7777777777777777777');
                    });

                    //res.status(200).send({ status:200, success: true, message: "A code has been sent to your provided email id. Please check your email.",token:token  });
                }else{
                    console.log('88888888888888888');
                    res.status(200).json({ status:200, success: false, message: "Somthing went wrong"});
                }
            
            });

        }else{
            console.log('99999999999999999999');
            res.status(200).send({ status:200, success: false, message: "Email ID does not registered!" });
        }
 
    }else{
        console.log('13131311313');
        res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
    }  
}

/************************* candidate forgot password ends *******************************/