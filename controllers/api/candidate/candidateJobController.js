var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);

/************************* candidate job listing start *******************************/

exports.candidate_job_listing = function(req, res, next) {
    
    // var job_category_id = req.body.data.job_category_id ? req.body.data.job_category_id : '';
    // var job_location = req.body.data.job_location ? req.body.data.job_location : '';
    // console.log(candidate_cv);
    
    //if(candidate_id !='' ){

        var job_by_category = sequelize.query("SELECT categories.category_id, categories.title, (SELECT COUNT(*) FROM jobs WHERE categories.category_id=jobs.job_category) as job_count_by_category FROM categories where categories.status='active' order by categories.title ASC",{ type: Sequelize.QueryTypes.SELECT })
        job_by_category.then(function (job_by_category) {
            var job_by_location = sequelize.query("SELECT COUNT(*) as job_count_by_location, basic_info_cuntry as job_location FROM jobs GROUP BY basic_info_cuntry",{ type: Sequelize.QueryTypes.SELECT })
            job_by_location.then(function (job_by_location) {
                var job_list_by_cat = sequelize.query("SELECT jobs.* from jobs where job_category ="+job_by_category[0].category_id,{ type: Sequelize.QueryTypes.SELECT })
                job_list_by_cat.then(function (job_list_by_default_cat) {

                    // if(job_category_id =='' && job_location !='' ){

                    //     var job_list_by_filter = sequelize.query("SELECT jobs.* from jobs where basic_info_cuntry ='"+job_location+"' ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
                    //     job_list_by_filter.then(function (job_list_by_filter) {
                    //         res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat, job_list_by_filter:job_list_by_filter });
                    //     }).catch(function(error) {
                    //         res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                    //     });

                    // }else if(job_location =='' && job_category_id !='' ){

                    //     var job_list_by_filter = sequelize.query("SELECT jobs.* from jobs where job_category ="+job_category_id+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
                    //     job_list_by_filter.then(function (job_list_by_filter) {
                    //         res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat, job_list_by_filter:job_list_by_filter });
                    //     }).catch(function(error) {
                    //         res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                    //     });

                    // }else if(job_location !='' && job_category_id !='' ){

                    //     var job_list_by_filter = sequelize.query("SELECT jobs.* from jobs where basic_info_cuntry ='"+job_location+"' and job_category ="+job_category_id+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
                    //     job_list_by_filter.then(function (job_list_by_filter) {
                    //         res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat, job_list_by_filter:job_list_by_filter });
                    //     }).catch(function(error) {
                    //         res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                    //     });

                    // }else{
                    //     res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat, job_list_by_filter:[] });
                    // }

                    res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat });

                }).catch(function(error) {
                    res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                });
            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });
        
    //}else{
        //res.status(200).json({ success: "false",message: "Candidate id is required!"});
    //}  
}

/************************* candidate job listing ends *******************************/




