var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);


exports.company_sizeList = async function(req, res, next) {
    var company_size_list = await models.company_size.findAll({where:{status:"active"}});
    if(company_size_list){
        res.status(200).send({ status:200,success: "true", details:company_size_list });
    }else{
        res.status(200).send({ status:200,success: "false"});
    }
    
}

exports.company_typeList = async function(req, res, next) {
    var company_type_list = await models.company_type.findAll({where:{status:"active"}});
    if(company_type_list){
        res.status(200).send({ status:200,success: "true", details:company_type_list });
    }else{
        res.status(200).send({ status:200,success: "false"});
    }
   
}