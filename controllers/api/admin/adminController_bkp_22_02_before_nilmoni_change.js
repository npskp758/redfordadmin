var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);


/*************************admin sign-in start *******************************/

exports.admin_signin_222 = function(req, res, next) {

    var username = req.body.data.username;
    var password = req.body.data.password;

    if(username !='' && password !=''){
        models.admins.findOne({ where: {username :username} }).then(function(admin) {
            if(admin!=null){
                adm = admin.toJSON();	   
                if(!bcrypt.compareSync(password, adm.password)) {		   
                    res.status(200).send({ message: "No user found" });
                } else {	  
                    if (username == admin.username) {              
                        var token =    jwt.sign({admin}, SECRET, { expiresIn: 18000 });  
                        res.status(200).send({ success: "true", message:"successfully login", token :token, admin_id:admin.admin_id,user_name:admin.username,mobile: admin.mobile });
                    }else{				
                        res.status(200).send({ message: "No user found" });
                    }            
                }//password check end                 
            }else{                
                res.status(200).send({ message: "No user found" });
            }    
        })
        .catch(function(error) {
            return res.send(error);        
        }); 
    }else{
        res.status(200).send({ message: "All fields are required!" });
    }
}
/*************************admin sign-in ends *******************************/


/*************************admin sign-in start *******************************/

exports.admin_signin = async function(req, res, next) {

    var username = req.body.data.username;
    var password = req.body.data.password;
   
    if(username && username !='' && password && password !=''){

    var admin_user =await sequelize.query("SELECT admins.* from admins where admins.username ='"+username+"'",{ type: Sequelize.QueryTypes.SELECT });

        if(admin_user.length > 0){
            if(!bcrypt.compareSync(password, admin_user[0].password)){
                res.status(200).send({ success: "false", message: "Invalid username and password!" });
            }else{
                var token =    jwt.sign({admin_user}, SECRET, { expiresIn: 18000 });
                res.status(200).send({ success: "true", message:"successfully login", admindetail:admin_user[0], token: token }); 
            }
            
        }else{				
            res.status(200).send({ success: "false", message: "Invalid username and password!" });
        }
    }else{				
         res.status(200).json({ success: "false", message: "Username and password is required!"});
    }

}

/*************************admin sign-in ends *******************************/


/************************* client listing start 111*******************************/

exports.client_list = async function(req, res, next) {
    console.log(req.body.data);

    var offset = req.body.data.start ? req.body.data.start : 0;
    var limit = req.body.data.limit ? req.body.data.limit : 10;

    var resultArray = [];

    // var client_listing = await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id order by clients.client_id DESC",{ type: Sequelize.QueryTypes.SELECT });
    // var client_listing = await sequelize.query("SELECT clients.*, client_basic_info.*, (select count(*) from companies where clients.client_id=companies.client_id) 'no_of_companies' from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id order by clients.client_id DESC",{ type: Sequelize.QueryTypes.SELECT });
    var client_count = await sequelize.query("SELECT clients.client_id, clients.username, clients.provider_image, clients.profile_picture, clients.createdAt, client_basic_info.client_basic_id, client_basic_info.name, client_basic_info.first_name, client_basic_info.last_name, client_basic_info.city, client_basic_info.state, client_basic_info.country, (select count(*) from companies where clients.client_id=companies.client_id) 'no_of_companies' from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id",{ type: Sequelize.QueryTypes.SELECT });
    var client_listing = await sequelize.query("SELECT clients.client_id, clients.username, clients.provider_image, clients.profile_picture, clients.createdAt, client_basic_info.client_basic_id, client_basic_info.name, client_basic_info.first_name, client_basic_info.last_name, client_basic_info.city, client_basic_info.state, client_basic_info.country, (select count(*) from companies where clients.client_id=companies.client_id) 'no_of_companies' from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id order by clients.client_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

    if(client_listing.length > 0){

        var i = 0;
        client_listing.forEach(function(element,index){ 

            if(element.first_name !='' && element.first_name != null){
                client_first_name = element.first_name;
                client_last_name = element.last_name;
                client_full_name = client_first_name+' '+client_last_name;
            }else if(element.name !='' && element.name != null){
                client_name = element.name.split(' ');
                client_first_name = client_name[0];
                for( var j =1; j<=client_name.length-1; j++){
                    client_last_name += client_name[j]+ " ";
                }
                client_last_name = client_last_name.substring(0, client_last_name.length-1);
                client_full_name = client_first_name+' '+client_last_name;
            }

            if(element.profile_picture !='' && element.profile_picture != null){
                var client_profile_picture = req.app.locals.baseurl+element.profile_picture;
            }else if(element.provider_image !='' && element.provider_image != null){
                var client_profile_picture = element.provider_image;
            }else{
                var client_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
            }

            resultArray.push({
                "client_id": element.client_id,
                "profile_image": client_profile_picture,
                "full_name": client_full_name,
                "no_of_companies": element.no_of_companies,
                "registered_on": element.createdAt,
                "city": element.city,
                "state": element.state,
                "country": element.country
            });    
            i++;             
        });
        res.status(200).send({ success: "true", clientList:resultArray, client_count:client_count.length });
    }else{
        res.status(200).json({ success: "false", message: "No Client found"});
    }
}
/************************* client listing ends 111*******************************/


// /*************************Send client list start *******************************/
// exports.client_list = async function(req, res, next) {
//     // var client_list = await sequelize.query("select a.client_id, IF(b.name != '', b.name, concat(b.first_name,' ', b.last_name)) 'full_name', "+
//     //                                         "IF(b.name != '', b.name, concat(b.first_name,' ', b.last_name)) 'full_name', b.country, b.state, b.city, "+
//     //                                         "IF(a.profile_picture !='', a.profile_picture, IF(a.provider_image != '', a.provider_image, 'contents/no_image_available_redford.jpg')) 'profile_image', " +
//     //                                         "(select count(*) from companies where client_id=a.client_id) 'no_of_companies', a.createdAt 'registered_on' from clients as a "+
//     //                                         "left join client_basic_info as b on b.client_id=a.client_id order by b.name, b.first_name",{ type: Sequelize.QueryTypes.SELECT });
//     var client_list = await sequelize.query("select a.client_id, IF(b.name != '', b.name, concat(b.first_name,' ', b.last_name)) 'full_name', b.country, b.state, b.city, "+
//                                             "IF(a.profile_picture !='', a.profile_picture, IF(a.provider_image != '', a.provider_image, 'contents/no_image_available_redford.jpg')) 'profile_image', " +
//                                             "(select count(*) from companies where client_id=a.client_id) 'no_of_companies', a.createdAt 'registered_on' from clients as a "+
//                                             "left join client_basic_info as b on b.client_id=a.client_id",{ type: Sequelize.QueryTypes.SELECT });
                                      
//     if(client_list.length > 0) {
//         res.status(200).send({ success: "true", clientList:client_list });  
//     } else {
//         res.status(200).send({ success: "false", message: "No client found!" }); 
//     }
// }
// /*************************Send client list ends *******************************/


/*************************Send client details start *******************************/
exports.client_details = async function(req, res, next) {
    var client_id = req.body.data.client_id;
    var resultArray = [];

    if(client_id && client_id !=''){

        var client_profile_info = await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

        if(client_profile_info.length > 0){

            var i = 0;
            var client_first_name = '';
            var client_last_name = '';

            client_profile_info.forEach(function(element,index){ 

                if(element.first_name !='' && element.first_name!= null){
                    client_first_name = element.first_name;
                    client_last_name = element.last_name;
                }else if(element.name !='' && element.name!= null){
                    client_name = element.name.split(' ');
                    client_first_name = client_name[0];
                    for( var j =1; j<=client_name.length-1; j++){
                        client_last_name += client_name[j]+ " ";
                    }
                    client_last_name = client_last_name.substring(0, client_last_name.length-1);
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var client_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var client_profile_picture = element.provider_image;
                }else{
                    var client_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "client_id": element.client_id,
                    "username": element.username,
                    "client_profile_picture": client_profile_picture,
                    "first_name": client_first_name,
                    "last_name": client_last_name,
                    "alternate_email": element.alternate_email,
                    "mob_country_code": element.mob_country_code,
                    "mobile": element.mobile,
                    "alt_mob_country_code": element.alt_mob_country_code,
                    "alternate_mobile": element.alternate_mobile,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "gender": element.gender,
                    "nationality": element.nationality
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, client_details:resultArray[0] });
        }else{
            res.status(200).json({ status:200, success: false, message: "Client not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    } 
}
/*************************Send client details ends *******************************/


/************************* company listing start *******************************/

exports.company_listing = async function(req, res, next) {

    //var client_id = req.body.data.client_id;
    var resultArray = [];

    // if(client_id && client_id !='' ){
        
        var company_listing = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id order by companies.company_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        
        if(company_listing.length > 0){

            var i = 0;
            company_listing.forEach(function(element,index){ 

                if(element.logo !='' && element.logo != null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "company_name": element.company_name,
                    "company_size": element.company_size,
                    "company_email":element.company_email,
                    "company_country_code":element.company_country_code,
                    "company_phone": element.company_phone,
                    "category": element.category,
                    "logo": company_logo,
                    "createdBy": element.createdBy,
                    "updatedBy":element.updatedBy,
                    "createdAt": element.createdAt,
                    "updatedAt": element.updatedAt,
                    "company_contact_info_id": element.company_contact_info_id,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "website": element.website,
                    "contact_person_name": element.contact_person_name,
                    "contact_person_email": element.contact_person_email,
                    "contact_person_country_code": element.contact_person_country_code,
                    "contact_person_mobile": element.contact_person_mobile,
                    "category_title": element.category_title
                });    
                i++;             
            });

            res.status(200).send({ status:200, success: true, company_listing:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Company not found!"});
        }

    // }else{
    //     res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    // }    
}

/************************* company listing ends *******************************/


/************************* company details start *******************************/

exports.company_details = async function(req, res, next) {
    
    var company_id = req.body.data.company_id;
    var resultArray = [];

    if(company_id && company_id !=''){

        // var company_details = await sequelize.query("SELECT companies.*, company_contact_info.*, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });
        var company_details = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });

        if(company_details.length > 0){

            var i = 0;
            company_details.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "company_name": element.company_name,
                    "company_size": element.company_size,
                    "company_email":element.company_email,
                    "company_country_code":element.company_country_code,
                    "company_phone": element.company_phone,
                    "category": element.category,
                    "logo": company_logo,
                    "createdBy": element.createdBy,
                    "updatedBy":element.updatedBy,
                    "createdAt": element.createdAt,
                    "updatedAt": element.updatedAt,
                    "company_contact_info_id": element.company_contact_info_id,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "website": element.website,
                    "contact_person_name": element.contact_person_name,
                    "contact_person_email": element.contact_person_email,
                    "contact_person_country_code": element.contact_person_country_code,
                    "contact_person_mobile": element.contact_person_mobile,
                    "category_title": element.category_title
                });    
                i++;             
            });

            res.status(200).send({ status:200, success: true, company_details:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Company details not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Company id is required!"});
    }  
}

/************************* company details ends *******************************/

/************************* company listing for client start *******************************/

exports.company_listing_for_client = async function(req, res, next) {

    var client_id = req.body.data.client_id;
    var resultArray = [];

    if(client_id && client_id !='' ){
        
        var client_company_listing = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.client_id ="+client_id+" order by companies.company_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        
        if(client_company_listing.length > 0){

            var i = 0;
            client_company_listing.forEach(function(element,index){ 

                if(element.logo !='' && element.logo != null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "company_name": element.company_name,
                    "company_size": element.company_size,
                    "company_email":element.company_email,
                    "company_country_code":element.company_country_code,
                    "company_phone": element.company_phone,
                    "category": element.category,
                    "logo": company_logo,
                    "createdBy": element.createdBy,
                    "updatedBy":element.updatedBy,
                    "createdAt": element.createdAt,
                    "updatedAt": element.updatedAt,
                    "company_contact_info_id": element.company_contact_info_id,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "website": element.website,
                    "contact_person_name": element.contact_person_name,
                    "contact_person_email": element.contact_person_email,
                    "contact_person_country_code": element.contact_person_country_code,
                    "contact_person_mobile": element.contact_person_mobile,
                    "category_title": element.category_title
                });    
                i++;             
            });

            res.status(200).send({ status:200, success: true, client_company_listing:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Company not found for this client!"});
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    }    
}

/************************* company listing for client ends *******************************/


/************************* candidate listing start *******************************/

exports.candidate_listing_for_admin = async function(req, res, next) {

    // var candidate_id = req.body.data.candidate_id;
    var resultArray = [];

    // if(candidate_id && candidate_id !=''){

        var candidate_listing = await sequelize.query("SELECT candidates.*, candidate_address_info.address, candidate_address_info.city, candidate_address_info.state, candidate_address_info.country, candidate_address_info.zip_code, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_address_info ON candidates.candidate_id = candidate_address_info.candidate_id LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id order by candidates.candidate_id DESC",{ type: Sequelize.QueryTypes.SELECT });

        var i = 0;
        if(candidate_listing.length > 0){

            candidate_listing.forEach(function(element,index){ 

              if(element.first_name !='' && element.first_name!= null){
                candidate_first_name = element.first_name;
                candidate_last_name = element.last_name;
                }else if(element.name !='' && element.name!= null){
                    candidate_name = element.name.split(' ');
                    candidate_first_name = candidate_name[0];
                    for( var j =1; j<=candidate_name.length-1; j++){
                        candidate_last_name += candidate_name[j]+ " ";
                    }
                    candidate_last_name = candidate_last_name.substring(0, candidate_last_name.length-1);
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var candidate_profile_picture = element.provider_image;
                }else{
                    var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "candidate_id": element.candidate_id,
                    "username": element.username,
                    "candidate_profile_picture": candidate_profile_picture,
                    "first_name": candidate_first_name,
                    "last_name": candidate_last_name,
                    "email": element.email,
                    "mob_country_code": element.mob_country_code,
                    "mobile": element.mobile,
                    "gender": element.gender,
                    "nationality": element.nationality,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "zip_code": element.zip_code,
                    "category": element.category,
                    "category_title": element.category_title
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, candidate_list:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Candidate not found"});
        }
}
/************************* candidate listing ends *******************************/


/************************* candidate details start *******************************/

exports.candidate_details_for_admin = async function(req, res, next) {
    console.log(req.body.data);

    var candidate_id = req.body.data.candidate_id;
    var resultArray = [];

    if(candidate_id && candidate_id !=''){

        var candidate_listing = await sequelize.query("SELECT candidates.*, candidate_address_info.address, candidate_address_info.city, candidate_address_info.state, candidate_address_info.country, candidate_address_info.zip_code, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_address_info ON candidates.candidate_id = candidate_address_info.candidate_id LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });

        var i = 0;
        if(candidate_listing.length > 0){

            candidate_listing.forEach(function(element,index){ 

              if(element.first_name !='' && element.first_name!= null){
                candidate_first_name = element.first_name;
                candidate_last_name = element.last_name;
                }else if(element.name !='' && element.name!= null){
                    candidate_name = element.name.split(' ');
                    candidate_first_name = candidate_name[0];
                    for( var j =1; j<=candidate_name.length-1; j++){
                        candidate_last_name += candidate_name[j]+ " ";
                    }
                    candidate_last_name = candidate_last_name.substring(0, candidate_last_name.length-1);
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var candidate_profile_picture = element.provider_image;
                }else{
                    var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "candidate_id": element.candidate_id,
                    "username": element.username,
                    "candidate_profile_picture": candidate_profile_picture,
                    "first_name": candidate_first_name,
                    "last_name": candidate_last_name,
                    "email": element.email,
                    "mob_country_code": element.mob_country_code,
                    "mobile": element.mobile,
                    "gender": element.gender,
                    "nationality": element.nationality,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "zip_code": element.zip_code,
                    "category": element.category,
                    "category_title": element.category_title
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, candidate_list:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Candidate not found"});
        }
    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
    }
}
/************************* candidate details ends *******************************/


/*************************client add edit start *******************************/

exports.client_addedit =async function(req, res, next) {

    console.log(req.body.data);
    
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var email = req.body.data.email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var password = req.body.data.password;
    var hash = bcrypt.hashSync(password);

    var client_id = req.body.data.client_id;
    
    if(first_name !='' && last_name !='' && email !='' && mobile !='' && password !='' && mob_country_code !=''){
        

        if(!client_id){

            var check_email =await sequelize.query("SELECT * FROM clients  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
            var check_mobile =await sequelize.query("SELECT * FROM client_basic_info  WHERE mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            var email_msg='';
            var mobile_msg='';
            if(check_email.length>0){
                email_msg= "Email ID already exists!";
            }if (check_mobile.length>0){
                mobile_msg= "Mobile number already exists!";
            }

            if(check_email.length==0 && check_mobile.length==0){

                models.clients.create({
                    username : email,
                    password : hash,
                    type : "client",
                }).then(async function(client){
                    if(client){
                        models.client_basic_info.create({
                            client_id: client.client_id,
                            first_name : first_name,
                            last_name : last_name,
                            mob_country_code : mob_country_code,
                            mobile: mobile,
                        }).then(async function(client_info){
                
                            var dir = './public/contents/clients/'+client.client_id; 
                            console.log(dir);
                            if (!fs.existsSync(dir)){
                                fs.mkdirSync(dir);                  
                            }

                            var client_full_data =await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+client.client_id,{ type: Sequelize.QueryTypes.SELECT });

                            
                            res.status(200).json({ status:200, success: true, message: "Client successfully created.", client_full_data: client_full_data[0] }); 
                            
                        });
                    }else{
                        res.status(200).json({ status:200, success: false,message: "Client creation failed!"});
                    }
            
                });

            }else{
                res.status(200).json({ status:200, success: false, message: "Email or mobile already exists!"});
            }
        }else{

            var check_email =await sequelize.query("SELECT * FROM clients  WHERE client_id != "+client_id+" and username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
            var check_mobile =await sequelize.query("SELECT * FROM client_basic_info  WHERE client_id != "+client_id+" and mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            var email_msg='';
            var mobile_msg='';
            if(check_email.length>0){
                email_msg= "Email ID already exists!";
            }if (check_mobile.length>0){
                mobile_msg= "Mobile number already exists!";
            }

            if(check_email.length==0 && check_mobile.length==0){

                models.clients.update({
                    username : email,
                    type : "client",
                },{where:{client_id:client_id}}).then(async function(client) {
                    models.client_basic_info.update({
                        client_id: client_id,
                        first_name : first_name,
                        last_name : last_name,
                        mob_country_code : mob_country_code,
                        mobile: mobile,
                    },{where:{client_id:client_id}}).then(async function(client_basic_info) {

                        var client_full_data =await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

                        res.status(200).json({ status:200, success: true, message: "Client successfully updated", client_full_data: client_full_data[0] }); 
                        
                    });
            
                });
            }else{
                res.status(200).json({ status:200, success: false, message: "Email or mobile already exists!"});
            }
        }

    }else{
        res.status(200).json({ status:200, success: false,message: "All fileds are required!"});
    }  
}

/*************************client add edit ends *******************************/


/************************* client delete start *******************************/

exports.client_delete = function(req, res, next) {
    console.log(req.body.data);
    var client_id = req.body.data.client_id;

    if(client_id && client_id !='' ){

        models.clients.destroy({ 
            where:{client_id:client_id}
        }).then(function(value) {

            if(value){
                models.client_basic_info.destroy({ 
                    where:{client_id:client_id}
                }).then(function(client_basic_info) {
                    res.status(200).send({ status:200, success: true, message: "Client successfully deleted" });
                });
            }else{
                res.status(200).send({ status:200, success: true, message: "Something went wrong. Client not successfully delete" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    } 
}

/************************* client delete ends *******************************/



/************************* candidate add-edit start *******************************/

exports.candidate_addedit = async function(req, res, next) {

    console.log(req.body.data);
    
    var password = req.body.data.password;
    var hash = bcrypt.hashSync(password);
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var email = req.body.data.email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var category_id = req.body.data.category_id;

    var candidate_id = req.body.data.candidate_id;

    
    // if(first_name !='' && last_name !='' && email !='' && mobile !='' && password !='' && mob_country_code !='' && category_id !=''){

    if(!candidate_id){

        if(first_name && first_name !='' && last_name && last_name !='' && email && email !='' && mobile && mobile !='' && password && password !='' && mob_country_code && mob_country_code !='' && category_id && category_id !=''){

            var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
            var check_mobile =await sequelize.query("SELECT * FROM candidates  WHERE mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            var email_msg='';
            var mobile_msg='';
            if(check_email.length>0){
                email_msg= "Email ID already exists!";
            }if (check_mobile.length>0){
                mobile_msg= "Mobile number already exists!";
            }

            if(check_email.length==0 && check_mobile.length==0){
                console.log('111111111111111111111111111');

                models.candidates.create({
                    
                    first_name : first_name,
                    last_name : last_name,
                    email : email,
                    username : email,
                    mob_country_code : mob_country_code,
                    mobile : mobile,
                    password : hash,
                    type : "candidate",
                    
                }).then(async function(candidate) { 

                    if(candidate){
                        console.log('222222222222222222222222');
                        models.candidate_looking_for.create({
                            candidate_id: candidate.candidate_id,
                            category : category_id,
                        }).then(async function(candidate_looking_for){
                
                            var dir = './public/contents/candidates/'+candidate.candidate_id; 
                            console.log(dir);
                            if (!fs.existsSync(dir)){
                                fs.mkdirSync(dir);                  
                            }

                            var candidate_full_data =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate.candidate_id,{ type: Sequelize.QueryTypes.SELECT });

                            res.status(200).send({ status:200, success: true, message: "Candidate successfully created.", details:candidate_full_data[0] });         
                        });
                    }else{
                        res.status(200).json({ status:200, success: false,message: "Candidate creation failed"});
                    }

                })
            }else{
                res.status(200).json({ status:200, success: false, message: "Email or mobile already exists!" });
            }

        }else{
            res.status(200).json({ status:200, success: false,message: "All fileds are required!"});
        } 

    }else{

        if(first_name && first_name !='' && last_name && last_name !='' && mobile && mobile !='' && mob_country_code && mob_country_code !='' && category_id && category_id !=''){

            var check_email =await sequelize.query("SELECT * FROM candidates  WHERE client_id != "+candidate_id+" and username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
            var check_mobile =await sequelize.query("SELECT * FROM candidates  WHERE client_id != "+candidate_id+" and mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            var email_msg='';
            var mobile_msg='';
            if(check_email.length>0){
                email_msg= "Email ID already exists!";
            }if (check_mobile.length>0){
                mobile_msg= "Mobile number already exists!";
            }

            if(check_email.length==0 && check_mobile.length==0){
                console.log('111111111111111111111111111');

                models.candidates.update({
                    
                    first_name : first_name,
                    last_name : last_name,
                    //email : email,
                    //username : email,
                    mob_country_code : mob_country_code,
                    mobile : mobile,
                    // password : hash,
                    //type : "candidate",

                },{where:{candidate_id:candidate_id}}).then(async function(candidate) {

                    if(candidate){
                        console.log('222222222222222222222222');
                        models.candidate_looking_for.update({

                            candidate_id: candidate_id,
                            category : category_id,

                        },{where:{candidate_id:candidate_id}}).then(async function(candidate_looking_for) {  

                            // var dir = './public/contents/candidates/'+candidate_id; 
                            // console.log(dir);
                            // if (!fs.existsSync(dir)){
                            //     fs.mkdirSync(dir);                  
                            // }

                            var candidate_full_data =await sequelize.query("SELECT candidates.*, candidate_looking_for.category, categories.title as category_title from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where candidates.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });

                            res.status(200).send({ status:200, success: true, message: "Candidate successfully updated.", details:candidate_full_data[0] });         
                        });
                    }else{
                        res.status(200).json({ status:200, success: false,message: "Candidate is not successfully updated"});
                    }

                })
            }else{
                res.status(200).json({ status:200, success: false, message: "Email or mobile already exists!" });
            }

        }else{
            res.status(200).json({ status:200, success: false,message: "All fileds are required!"});
        } 

    }
    // }else{
    //     res.status(200).json({ status:200, success: false,message: "All fileds are required!"});
    // }  
}

/************************* candidate add-edit ends *******************************/


/************************* candidate delete start *******************************/

exports.candidate_delete = function(req, res, next) {
    console.log(req.body.data);
    var candidate_id = req.body.data.candidate_id;

    if(candidate_id && candidate_id !='' ){

        models.candidates.destroy({ 
            where:{candidate_id:candidate_id}
        }).then(function(value) {

            if(value){
                models.candidate_looking_for.destroy({ 
                    where:{candidate_id:candidate_id}
                }).then(function(candidate_looking_for) {
                    res.status(200).send({ status:200, success: true, message: "Candidate successfully deleted" });
                });
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Candidate not successfully deleted" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id is required!"});
    } 
}

/************************* candidate delete ends *******************************/


/************************* job opening status upload start *******************************/

exports.job_opening_status_upload = async function(req, res, next) {
    console.log(req.body.data);

    var title = req.body.data.title;
    var sequence = req.body.data.sequence;
    var status = req.body.data.status;
    var user_id = req.body.data.user_id;

    var job_opening_status_id = req.body.data.job_opening_status_id;

    if(title && title !='' && status && status !='' && user_id && user_id !='' ){
        console.log('11111111111111111111111');
        if(!job_opening_status_id){
            console.log('22222222222222222222222');
            models.job_opening_status.create({ 

                title: title,
                sequence: sequence,
                status: status,
                createdBy: user_id

            }).then(function(job_opening_status) {

            res.status(200).send({ status:200, success: true, message: "Job Opening status successfully added" });
            })
            .catch(function(error) {
                return res.send( { status:200, success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.job_opening_status.update({ 

                title: title,
                sequence: sequence,
                status: status,
                updatedBy: user_id,

            },{where:{job_opening_status_id:job_opening_status_id}}).then(function(job_opening_status) {

                res.status(200).send({ status:200, success: true, message: "Job Opening status successfully updated" });
            })
            .catch(function(error) {
                return res.send( { status:200, success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
    } 
}

/************************* job opening status upload ends *******************************/

/************************* job opening status list start *******************************/
exports.job_opening_status_list = async function(req, res, next) {
    var job_opening_status_list = await sequelize.query("SELECT * FROM `job_opening_status`",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(job_opening_status_list.length > 0) {
        res.status(200).send({ status:200,success: true, job_opening_status_list:job_opening_status_list });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No job opening status found!" }); 
    }
}
/************************* job opening status list ends *******************************/


/************************* job opening status delete start *******************************/

exports.job_opening_status_delete = function(req, res, next) {
    console.log(req.body.data);
    var job_opening_status_id = req.body.data.job_opening_status_id;

    if(job_opening_status_id !='' ){

        models.job_opening_status.destroy({ 
            where:{job_opening_status_id:job_opening_status_id}
        }).then(function(value) {
            res.status(200).send({ status:200, success: true, message: "Job opening status successfully deleted" });
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Job opening status id is required!"});
    } 
}

/************************* job opening status delete ends *******************************/


/************************* website logo upload start *******************************/

exports.website_logo_upload = async function(req, res, next) {
    
    var file_extension = req.body.data.file_extension;
    var website_logo = req.body.data.website_logo;

    // console.log(req.body.data);

    if(website_logo && website_logo !='' && file_extension && file_extension !=''){

        console.log('333333333333333333');
        var site_logo = 'contents/website_logo/site_logo.'+file_extension;
        try {
            const path = './public/'+site_logo;
            const imgdata = website_logo;
            const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
            fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
        } catch (e) {
            next(e);
        }
        console.log('444444444444444444444');

        models.site_settings.update({
            site_logo: site_logo,
        },{where:{site_setting_id:1}})

        res.status(200).send({ status:200,success: true, message: "Logo successfully uploaded" }); 
    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
    } 
}

/************************* website logo upload ends *******************************/


/************************* website logo upload start *******************************/

exports.website_logo_showing = async function(req, res, next) {
    
   var site_settings = await sequelize.query("SELECT * FROM `site_settings`",{ type: Sequelize.QueryTypes.SELECT });

    if(site_settings.length > 0) {
        //var path= req.app.locals.baseurl+site_settings[0].site_logo;
        res.status(200).send({ status:200,success: true, website_logo:req.app.locals.baseurl+site_settings[0].site_logo });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No website logo found!" }); 
    }
}

/************************* website logo upload ends *******************************/


/************************* candidate job opening status upload start *******************************/

exports.candidate_job_opening_status_upload = async function(req, res, next) {
    console.log(req.body.data);

    var title = req.body.data.title;
    var sequence = req.body.data.sequence;
    var status = req.body.data.status;
    var user_id = req.body.data.user_id;

    var candidate_job_opening_status_id = req.body.data.candidate_job_opening_status_id;

    if(title && title !='' && status && status !='' && user_id && user_id !='' ){
        console.log('11111111111111111111111');
        if(!candidate_job_opening_status_id){
            console.log('22222222222222222222222');
            models.candidate_job_opening_status.create({ 

                title: title,
                sequence: sequence,
                status: status,
                createdBy: user_id

            }).then(function(candidate_job_opening_status) {

            res.status(200).send({ status:200, success: true, message: "Candidate job Opening status successfully added" });
            })
            .catch(function(error) {
                return res.send( { status:200, success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.candidate_job_opening_status.update({ 

                title: title,
                sequence: sequence,
                status: status,
                updatedBy: user_id,

            },{where:{candidate_job_opening_status_id:candidate_job_opening_status_id}}).then(function(candidate_job_opening_status) {

                res.status(200).send({ status:200, success: true, message: "Candidate job Opening status successfully updated" });
            })
            .catch(function(error) {
                return res.send( { status:200, success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
    } 
}

/************************* candidate job opening status upload ends *******************************/


// /************************* candidate job opening status list start *******************************/
// exports.candidate_job_opening_status_list = async function(req, res, next) {
//     var candidate_job_opening_status_list = await sequelize.query("SELECT * FROM `candidate_job_opening_status`",{ type: Sequelize.QueryTypes.SELECT });
                                      
//     if(candidate_job_opening_status_list.length > 0) {
//         res.status(200).send({ status:200,success: true, candidate_job_opening_status_list:candidate_job_opening_status_list });  
//     } else {
//         res.status(200).send({ status:200,success: false, message: "No candidate job opening status found!" }); 
//     }
// }
// /************************* candidate job opening status list ends *******************************/


/************************* candidate job opening status list start *******************************/
exports.candidate_job_opening_status_list = async function(req, res, next) {

    var candidate_job_opening_status_list = await sequelize.query("SELECT * FROM `candidate_job_opening_status` order by sequence ASC",{ type: Sequelize.QueryTypes.SELECT });

    if(candidate_job_opening_status_list !=''){
        if(candidate_job_opening_status_list.length > 0){
            var resultArray = [];
            var i = 0;
            candidate_job_opening_status_list.forEach( async function(element){
                var candidate_job_opening_sub_status_list = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where candidate_job_opening_status_id = "+element.candidate_job_opening_status_id+" order by sequence ASC",{ type: Sequelize.QueryTypes.SELECT });

                resultArray.push({
                    "candidate_job_opening_status_id":element.candidate_job_opening_status_id,
                    "title":element.title,
                    "sequence":element.sequence,
                    "status":element.status,
                    "createdBy":element.createdBy,
                    "updatedBy":element.updatedBy,
                    "createdAt":element.createdAt,
                    "updatedAt":element.updatedAt,
                    "sub_status_list":candidate_job_opening_sub_status_list
                });    
                i++;
                if(i== candidate_job_opening_status_list.length){

                    resultArray.sort(function(a,b){return a.sequence - b.sequence});
                    res.status(200).send({ status:200, success: true, candidate_job_opening_status_list:resultArray });
                }                
            });           
        }else{
            res.status(200).send({ status:200, success: true, candidate_job_opening_status_list:resultArray });
        }
    }else{
        res.status(200).json({ status:200, success: false, message: "No candidate job opening status found!"});
    }  





                                      
    // if(candidate_job_opening_status_list.length > 0) {
    //     var resultArray = [];
    //     var i = 0;
    //     candidate_job_opening_status_list.forEach( async function(element){

    //         console.log(element.title)

    //         var candidate_job_opening_sub_status_list = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where candidate_job_opening_status_id = "+element.candidate_job_opening_status_id,{ type: Sequelize.QueryTypes.SELECT });
    //         // sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where candidate_job_opening_status_id = "+element.candidate_job_opening_status_id,{ type: Sequelize.QueryTypes.SELECT }) 
    //         // .then(function(candidate_job_opening_sub_status_list) {

    //             //console.log(candidate_job_opening_sub_status_list[i].title)
    //             //console.log(candidate_job_opening_sub_status_list[i].candidate_job_opening_sub_status_id)
    //             resultArray.push({
    //                 "candidate_job_opening_status_id":element.candidate_job_opening_status_id,
    //                 "title":element.title,
    //                 "sequence":element.sequence,
    //                 "status":element.status,
    //                 "createdBy":element.createdBy,
    //                 "updatedBy":element.updatedBy,
    //                 "createdAt":element.createdAt,
    //                 "updatedAt":element.updatedAt,
    //                 "sub_status_list":candidate_job_opening_sub_status_list
    //             });    
    //             i++;
    //             res.status(200).send({ status:200,success: true, candidate_job_opening_status_list:resultArray });  
    //         // }); 
    //     });
    //     // res.status(200).send({ status:200,success: true, candidate_job_opening_status_list:resultArray });  
    // } else {
    //     res.status(200).send({ status:200,success: false, message: "No candidate job opening status found!" }); 
    // }
}
/************************* candidate job opening status list ends *******************************/


/************************* candidate job opening status delete start *******************************/

exports.candidate_job_opening_status_delete = function(req, res, next) {
    console.log(req.body.data);
    var candidate_job_opening_status_id = req.body.data.candidate_job_opening_status_id;

    if(candidate_job_opening_status_id !='' ){

        models.candidate_job_opening_status.destroy({ 
            where:{candidate_job_opening_status_id:candidate_job_opening_status_id}
        }).then(function(value) {

            if(value){
                models.candidate_job_opening_sub_status.destroy({ 
                    where:{candidate_job_opening_status_id:candidate_job_opening_status_id}
                }).then(function(delete_value) {
                    res.status(200).send({ status:200, success: true, message: "Candidate job opening status successfully deleted" });
                });
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Candidate job opening status not successfully deleted" });
            }

           
            // res.status(200).send({ status:200, success: true, message: "Candidate job opening status successfully deleted" });
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate job opening status id is required!"});
    } 
}

/************************* candidate job opening status delete ends *******************************/


/************************* candidate job opening status dropdown for sub status start *******************************/
exports.candidate_job_opening_status_dropdown = async function(req, res, next) {
    var candidate_job_opening_status_dropdown = await sequelize.query("SELECT candidate_job_opening_status_id, title FROM `candidate_job_opening_status` where status = 'active' order by sequence ASC",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(candidate_job_opening_status_dropdown.length > 0) {
        res.status(200).send({ status:200,success: true, candidate_job_opening_status_list:candidate_job_opening_status_dropdown });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No candidate job opening status found!" }); 
    }
}
/************************* candidate job opening status dropdown for sub status ends *******************************/


/************************* candidate job opening sub status upload start *******************************/

exports.candidate_job_opening_sub_status_upload = async function(req, res, next) {
    console.log(req.body.data);

    var candidate_job_opening_status_id = req.body.data.candidate_job_opening_status_id;
    var title = req.body.data.title;
    var sequence = req.body.data.sequence;
    var status = req.body.data.status;
    var user_id = req.body.data.user_id;

    var candidate_job_opening_sub_status_id = req.body.data.candidate_job_opening_sub_status_id;

    if(candidate_job_opening_status_id && candidate_job_opening_status_id !='' && title && title !='' && status && status !='' && user_id && user_id !='' ){
        console.log('11111111111111111111111');
        if(!candidate_job_opening_sub_status_id){
            console.log('22222222222222222222222');
            models.candidate_job_opening_sub_status.create({ 

                candidate_job_opening_status_id: candidate_job_opening_status_id,
                title: title,
                sequence: sequence,
                status: status,
                createdBy: user_id

            }).then(function(candidate_job_opening_sub_status) {

            res.status(200).send({ status:200, success: true, message: "Candidate job Opening sub status successfully added" });
            })
            .catch(function(error) {
                return res.send( { status:200, success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.candidate_job_opening_sub_status.update({ 

                candidate_job_opening_status_id: candidate_job_opening_status_id,
                title: title,
                sequence: sequence,
                status: status,
                updatedBy: user_id,

            },{where:{candidate_job_opening_sub_status_id:candidate_job_opening_sub_status_id}}).then(function(candidate_job_opening_sub_status) {

                res.status(200).send({ status:200, success: true, message: "Candidate job Opening sub status successfully updated" });
            })
            .catch(function(error) {
                return res.send( { status:200, success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
    } 
}

/************************* candidate job opening sub status upload ends *******************************/

/************************* candidate job opening sub status list start *******************************/
exports.candidate_job_opening_sub_status_list = async function(req, res, next) {
    var candidate_job_opening_sub_status_list = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status`",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(candidate_job_opening_sub_status_list.length > 0) {
        res.status(200).send({ status:200,success: true, candidate_job_opening_sub_status_list:candidate_job_opening_sub_status_list });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No candidate job opening sub status found!" }); 
    }
}
/************************* candidate job opening status list ends *******************************/


/************************* candidate job opening status delete start *******************************/

exports.candidate_job_opening_sub_status_delete = function(req, res, next) {
    console.log(req.body.data);
    var candidate_job_opening_sub_status_id = req.body.data.candidate_job_opening_sub_status_id;

    if(candidate_job_opening_sub_status_id !='' ){

        models.candidate_job_opening_sub_status.destroy({ 
            where:{candidate_job_opening_sub_status_id:candidate_job_opening_sub_status_id}
        }).then(function(value) {
            res.status(200).send({ status:200, success: true, message: "Candidate job opening sub status successfully deleted" });
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate job opening sub status id is required!"});
    } 
}

/************************* candidate job opening status delete ends *******************************/



/************************* email template type list start *******************************/

exports.email_template_type_list = async function(req, res, next) {

    var email_template_type_list = await sequelize.query("SELECT email_template_type_id, title, slug, status FROM `email_template_type` where 1 ORDER BY email_template_type_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(email_template_type_list.length > 0) {
        res.status(200).send({ status:200, success: true, email_template_type_list:email_template_type_list });  
    } else {
        res.status(200).send({ status:200, success: false, message: "No email template type found!" }); 
    }
}

/************************* email template type list ends *******************************/



/************************* email template upload start *******************************/

exports.email_template_upload = async function(req, res, next) {
    console.log(req.body.data);

    var email_template_type = req.body.data.email_template_type;
    var subject = req.body.data.subject;
    var content = req.body.data.content;
    var status = req.body.data.status;
    var user_id = req.body.data.user_id ? req.body.data.user_id : '';

    var email_template_id = req.body.data.email_template_id;

    
    if(email_template_type && email_template_type !='' && subject && subject !='' && content && content !='' && status && status !=''){
        console.log('11111111111111111111111');
        if(!email_template_id){
            console.log('22222222222222222222222');
            models.email_template.create({ 

                email_template_type: email_template_type,
                subject: subject,
                content: content,
                status: status,
                createdBy: user_id

            }).then(function(email_template) {

                res.status(200).send({ status:200, success: true, message: "Email template successfully added" });

            })
            .catch(function(error) {
                return res.send( {status:200, success: false, error: error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.email_template.update({ 

                email_template_type: email_template_type,
                subject: subject,
                content: content,
                status: status,
                updatedBy: user_id

            },{where:{email_template_id:email_template_id}}).then(function(email_template) {

                res.status(200).send({ status:200, success: true, message: "Email template successfully updated" });
            })
            .catch(function(error) {
                return res.send( {status:200, success: false, error: error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
    }
} 

/************************* email template upload ends *******************************/


/************************* email template list start *******************************/

exports.email_template_list = async function(req, res, next) {

    var email_template_list = await sequelize.query("SELECT email_template.*, email_template_type.title FROM `email_template` LEFT JOIN email_template_type ON email_template.email_template_type = email_template_type.email_template_type_id where 1 ORDER BY email_template.email_template_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(email_template_list.length > 0) {
        res.status(200).send({ status:200, success: true, email_template_list:email_template_list });  
    } else {
        res.status(200).send({ status:200, success: false, message: "No email template found!" }); 
    }
}

/************************* email template list ends *******************************/


/************************* email template details start *******************************/

exports.email_template_details = async function(req, res, next) {
    console.log(req.body.data);
    var email_template_id = req.body.data.email_template_id;

    if(email_template_id && email_template_id !='' ){

        var email_template_details = await sequelize.query("SELECT * FROM `email_template` where email_template_id = "+email_template_id,{ type: Sequelize.QueryTypes.SELECT });
                                        
        if(email_template_details.length > 0) {

            res.status(200).send({ status:200, success: true, email_template_details:email_template_details[0] });  
        } else {
            res.status(200).send({ status:200, success: false, message: "No email template found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Email template id is required!"});
    }
}

/************************* email template details ends *******************************/


/************************* email template delete start *******************************/

exports.email_template_delete = function(req, res, next) {
    console.log(req.body.data);
    var email_template_id = req.body.data.email_template_id;

    if(email_template_id && email_template_id !='' ){

        models.email_template.destroy({ 
            where:{email_template_id:email_template_id}
        }).then(function(value) {

            if(value){
                
                res.status(200).send({ status:200, success: true, message: "Email template successfully deleted" });
               
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Email template not successfully deleted" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Email template id is required!"});
    } 
}

/************************* email template delete ends *******************************/



/************************* admin profile information start *************************/

exports.admin_profile_information = async function(req, res, next) {

    var admin_id = req.body.data.admin_id;
    var resultArray = [];

    if(admin_id && admin_id !=''){

        var admin_profile_info = await sequelize.query("SELECT admins.* from admins where admins.admin_id = "+admin_id,{ type: Sequelize.QueryTypes.SELECT });

        if(admin_profile_info.length > 0){

            var i = 0;
            admin_profile_info.forEach(function(element,index){ 

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var admin_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else{
                    var admin_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "admin_id": element.admin_id,
                    "username": element.username,
                    "admin_profile_picture": admin_profile_picture,
                    "name": element.name,
                    "email": element.email,
                    "alternate_email": element.alternate_email,
                    "mob_country_code": element.mob_country_code,
                    "mobile": element.mobile,
                    "alt_mob_country_code": element.alt_mob_country_code,
                    "alternate_mobile": element.alternate_mobile,
                    "address": element.address,
                    "type": element.type
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, admin_details:resultArray[0] });
        }else{
            res.status(200).json({ status:200, success: false, message: "Admin not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Admin id is required!"});
    }    
}


/************************* admin profile information ends *************************/


/************************* admin profile update start *************************/

exports.admin_profile_update = async function(req, res, next) {
    console.log(req.body.data);

    var admin_id = req.body.data.admin_id;
    var name = req.body.data.name;
    var alternate_email = req.body.data.alternate_email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var alt_mob_country_code = req.body.data.alt_mob_country_code;
    var alternate_mobile = req.body.data.alternate_mobile;
    var address = req.body.data.address;

    var profile_pic = req.body.data.profile_pic;
    var file_extension = req.body.data.file_extension;

    if(admin_id && admin_id !=''){
        console.log('11111111111111111111');

        var admin_validation = await sequelize.query("SELECT admins.* from admins where admins.admin_id = "+admin_id,{ type: Sequelize.QueryTypes.SELECT });

        if(admin_validation.length > 0){
            console.log('22222222222222222222222');

            if(name && name !='' && mob_country_code && mob_country_code !='' && mobile && mobile !='' && address && address !='' ){

                if(profile_pic && profile_pic !='' && file_extension && file_extension !=''){

                    var dir = './public/contents/admins/'+admin_id; 
                    console.log(dir);
                    if (!fs.existsSync(dir)){
                        fs.mkdirSync(dir);                  
                    }

                    console.log('333333333333333333');
                    var admin_profile_pic = 'contents/admins/'+admin_id+'/profile_picture.'+file_extension;
                    try {
                        const path = './public/'+admin_profile_pic;
                        const imgdata = profile_pic;
                        const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                        fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                    } catch (e) {
                        next(e);
                    }
                    console.log('444444444444444444444');

                    models.admins.update({
                        name: name,
                        alternate_email: alternate_email,
                        mob_country_code: mob_country_code,
                        mobile: mobile,
                        alt_mob_country_code: alt_mob_country_code,
                        alternate_mobile: alternate_mobile,
                        address: address,
                        profile_picture: admin_profile_pic,
                    },{where:{admin_id:admin_id}}).then(function(admins) {
    
                        res.status(200).send({ status:200, success: true, message: "Admin profile successfully updated" });
                    })

                }else{

                    models.admins.update({
                        name: name,
                        alternate_email: alternate_email,
                        mob_country_code: mob_country_code,
                        mobile: mobile,
                        alt_mob_country_code: alt_mob_country_code,
                        alternate_mobile: alternate_mobile,
                        address: address,
                    },{where:{admin_id:admin_id}}).then(function(admins) {
    
                        res.status(200).send({ status:200, success: true, message: "Admin profile successfully updated" });
                    })
                }

            }else{
                res.status(200).json({ status:200, success: false, message: "All fields are required!"});
            }

        }else{
            res.status(200).json({ status:200, success: false, message: "Admin not found"});
        }
        
    }else{
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaa');
        res.status(200).json({ status:200, success: false, message: "Admin id is required!"});
    }    
}

/************************* admin profile update ends *************************/


/************************* admin password update start *************************/

exports.admin_password_update = async function(req, res, next) {
    console.log(req.body.data);

    var admin_id = req.body.data.admin_id;
    var current_password = req.body.data.current_password;
    var new_password = req.body.data.new_password;
   
    if(admin_id && admin_id !=''){
        console.log('11111111111111111111');

        var admin_details = await sequelize.query("SELECT admins.* from admins where admins.admin_id = "+admin_id,{ type: Sequelize.QueryTypes.SELECT });

        if(admin_details.length > 0){
            console.log('22222222222222222222222');

            if(current_password && current_password !='' && new_password && new_password !='' ){
                console.log('555555555555555555');

                if(!bcrypt.compareSync(current_password, admin_details[0].password)){
                    res.status(200).send({ status:200, success: false, message: "Curremt password does not match!" });
                }else{
                    var hash = bcrypt.hashSync(new_password);
                    models.admins.update({ 

                        password : hash,
    
                    },{where:{admin_id:admin_id}}).then(function(admins) {
                        res.status(200).send({ status:200, success: true, message: "Admin password successfully updated" });
                    })
                }

            }else{
                res.status(200).json({ status:200, success: false, message: "All fields are required!"});
            }

        }else{
            res.status(200).json({ status:200, success: false, message: "Admin not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Admin id is required!"});
    }    
}


/************************* admin password update ends *************************/



/************************* admin dashboard start *******************************/

exports.admin_dashboard = async function(req, res, next) {

    var client_count =await sequelize.query("SELECT COUNT(*) as client_count from clients",{ type: Sequelize.QueryTypes.SELECT });
    var company_count =await sequelize.query("SELECT COUNT(*) as company_count from companies" ,{ type: Sequelize.QueryTypes.SELECT });
    var candidate_count =await sequelize.query("SELECT COUNT(*) as candidate_count from candidates",{ type: Sequelize.QueryTypes.SELECT });
    var job_count =await sequelize.query("SELECT COUNT(*) as job_count from jobs",{ type: Sequelize.QueryTypes.SELECT });

    res.status(200).send({ status:200, success: true, client_count:client_count[0].client_count, company_count:company_count[0].company_count, candidate_count:candidate_count[0].candidate_count, job_count:job_count[0].job_count  });
}

/************************* admin dashboard ends *******************************/


