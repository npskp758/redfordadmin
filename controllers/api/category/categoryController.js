var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);

/************************* Category List Api Start **************************************/
exports.categoryList = async function(req, res, next) {

    // var token= req.headers['token'];
    // jwt.verify(token, SECRET, function(err, decoded) {
    //     if (err) {
    //         res.json('Invalid Token');
    //     }else{
        var categorieslist = await models.category.findAll({where: {status:"active"}});
        var pictures  = 'contents/categories/';
            // models.categories.findAll({where:{status:"active"}}).then(function(categorieslist) {
                //console.log("dfsfsdfsdfsdfsdfsdfsdfsdfsdfsdfsfsdfsdfsfsdfsdfsdf"   +categorieslist);
                if(categorieslist){
                    res.status(200).send({ status:200,success: "true", details:categorieslist,pictures });
                }else{
                    res.status(200).send({ status:200,success: "false", message:"No data found!"});
                }
            
            // });
    //     }
    // });  
}
/************************* Category List Api Ends **************************************/


/*************************Category AddEdit Api Start **************************************/
exports.categoryAddEdit = async function(req, res, next) {
    console.log(req.body.data);
    var token= req.headers['token'];
    // jwt.verify(token, SECRET, async function(err, decoded) {
    //     if (err) {
    //         res.json('Invalid Token');
    //     }else{
                var category_id = req.body.data.category_id;
                var title = req.body.data.title;
                var status = req.body.data.status;
                var user_id = req.body.data.user_id;
                var logo = req.body.data.logo;
                var bg = req.body.data.bg;
                var msg = '';
                var success = '';

            
            if(title !='' && status !=''){
                var image_name = title.replace(/ /g,"-");
                var background_image_name = image_name+'_bg.jpg';
                console.log(background_image_name);
                if(!category_id){
                    models.category.create({
                        title: title,
                        bg_image:background_image_name,
                        status: status,
                        createdBy : user_id
                    }).then(function(addcategory){
                        if(addcategory){
                            msg = "Category added successfuly!";
                            success ="true";
                        
                            //var image_name = title.replace(/ /g,"-");
                            try {
                                const path = './public/contents/categories/'+image_name+'_logo.jpg';
                                const imgdata = logo;
                                const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                                fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                            } catch (e) {
                                next(e);
                            }
                            try {
                                const path = './public/contents/categories/'+image_name+'_bg.jpg';      
                                const imgdata = bg;
                                const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                                fs.writeFileSync(path, base64Data,  {encoding: 'base64'});                
                            } catch (e) {
                                next(e);
                            }
                            
                        }else{
                            msg = "Failed to added category!";
                            success ="false";
                        }
                        res.status(200).send({ status:200, success:success, message:msg, details:addcategory });
                    })
                }else{

                    var category = await models.category.findOne({where:{category_id : category_id}});

                    models.category.update({
                        title: title,
                        bg_image:background_image_name,
                        status: status,
                        updatedBy: user_id
                        
                    },{where:{category_id:category_id}}).then(function(is_update) { 
                        if(is_update){
                            msg = "Category updated successfuly !";
                            success ="true";
                        
                            if(category.title != title) {
                                //ToDO: Delete previous images
                                var dir = './public/contents/categories/';
                                var existing_logo = dir+category.title + "_logo.jpg";
                                var existing_bg   = dir+category.title + "_bg.jpg";
                                fs.unlink(existing_logo, function (err){});
                                fs.unlink(existing_bg, function (err){});
                            }
                            var image_name = title.replace(/ /g,"-");
                            try {
                                const path = './public/contents/categories/'+image_name+'_logo.jpg';
                                const imgdata = logo;
                                const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                                fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                            } catch (e) {
                                next(e);
                            }
                            try {
                                const path = './public/contents/categories/'+image_name+'_bg.jpg';      
                                const imgdata = bg;
                                const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                                fs.writeFileSync(path, base64Data,  {encoding: 'base64'});                
                            } catch (e) {
                                next(e);
                            }
                        
                        }else{
                            msg = "Failed to update category!";
                            success ="false";
                        }
                        res.status(200).send({ status:200, success:success, message:msg });
                    })
                }
            }else{
                res.status(200).send({ status:200, message: "All fields are required!"}); 
            }

    //     }
    // });   
}

/*************************Category AddEdit Api Ends **************************************/


/*************************Category delete Api Start **************************************/

exports.categoryDelete =  function(req, res, next) {

    var token= req.headers['token'];
    jwt.verify(token, SECRET, async function(err, decoded) {
        if (err) {
            res.json('Invalid Token');
        }else{
            var msg = '';
            var success = '';
            var category_id = req.body.data.category_id;
            
            if(category_id != '') {
                var category = await models.category.findOne({where:{category_id : category_id}});
                if(category != '') {
                    var isdeleted = await models.category.destroy({where:{category_id:category_id} });
                    if(isdeleted){
                        msg = "Category successfully deleted!";
                        success ="true";
                        var dir = './public/contents/categories/';
                        var image_name = category.title .replace(/ /g,"-");
                        var existing_logo = dir+image_name+ "_logo.jpg";
                        var existing_bg   = dir+image_name + "_bg.jpg";
                        try {
                            fs.unlink(existing_logo, function (err){});
                            fs.unlink(existing_bg, function (err){});
                        } catch (e) {
                            //next(e);
                            msg = "Failed to delete image(s). Please remove it manually."
                            success ="false";
                        }
                    }else{
                        msg = "Failed to delete category!";
                        success ="false";
                    }
                } else {
                    msg = "Category not found!";
                    success ="false"; 
                }
            } else {
                msg = "Category not found!";
                success ="false"; 
            }
            res.status(200).send({ status:200, success: success, message:msg});   
        }
    }); 
}; 

/*************************Category delete Api Start **************************************/

