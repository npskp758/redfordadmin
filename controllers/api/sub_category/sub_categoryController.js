var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);

/************************* Sub Category List Api Start **************************************/
exports.sub_categoryList = async function(req, res, next) {

    var token= req.headers['token'];
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.json('Invalid Token');
        }else{
            // var categorylist = await models.category.findAll({where:{status:"active"}});
            models.sub_category.findAll({where:{status:"active"} }).then(function(sub_categorylist) {
                console.log(sub_categorylist);
                if(sub_categorylist){
                    res.status(200).send({ status:200,success: "true", details:sub_categorylist });
                }else{
                    res.status(200).send({ status:200,success: "false"});
                }
            
            });
        }
    });  
}

/************************* Sub Category List Api Ends **************************************/


/************************* Sub Category List Api according to sub category Start **************************************/
exports.categorytosub_categoryList = async function(req, res, next) {
    var category_id = req.body.data.category_id;

    // var token= req.headers['token'];
    // jwt.verify(token, SECRET, function(err, decoded) {
    //     if (err) {
    //         res.json('Invalid Token');
    //     }else{
            // var categorylist = await models.category.findAll({where:{status:"active"}});
            models.sub_category.findAll({where:{status:"active",category_id:category_id} }).then(function(sub_categorylist) {
                console.log(sub_categorylist);
                if(sub_categorylist){
                    res.status(200).send({ status:200,success: "true", details:sub_categorylist });
                }else{
                    res.status(200).send({ status:200,success: "false"});
                }
            
            });
    //     }
    // });  
}

/************************* Sub Category List Api according to sub category Ends **************************************/


/************************* Sub Category Add Edit Api Start **************************************/
exports.sub_categoryAddEdit = function(req, res, next) {

    var token= req.headers['token'];
    jwt.verify(token, SECRET, async function(err, decoded) {
        if (err) {
            res.json('Invalid Token');
        }else{
            var user_id = req.body.data.user_id;
            var sub_category_id = req.body.data.sub_category_id;
            var category_id = req.body.data.category_id;
            var title = req.body.data.title;
            var status = req.body.data.status;
            var msg = '';
            var success = '';

            if(category_id !='' && title !='' && status !=''){
                if(!sub_category_id){
                    models.sub_category.create({
                        category_id : category_id,
                        title: title,
                        status: status,
                        createdBy : user_id
                    }).then(function(addsubcategory){
                        if(addsubcategory){
                            msg = "Category added successfuly!";
                            success ="true";
                        }else{
                            msg = "Failed to added category!";
                            success ="false";
                        }
                        res.status(200).send({ status:200, success:success, message:msg, details:addsubcategory });
                    })
                }else{

                    models.sub_category.update({
                        category_id : category_id,
                        title: title,
                        status: status,
                        updatedBy: user_id
                        
                    },{where:{sub_category_id:sub_category_id}}).then(function(is_update) { 
                        if(is_update){
                            msg = "Sub category updated successfuly !";
                            success ="true";
                        }else{
                            msg = "Failed to update sub category!";
                            success ="false";
                        }
                        res.status(200).send({ status:200, success:success, message:msg });
                    })
                }
            }else{
                res.status(200).send({ status:200, message: "All fields are required!"}); 
            }
        }
    });   
}
/************************* Sub Category Add Edit Api Ends **************************************/


/************************* Sub Category delete Api Start **************************************/
exports.sub_categoryDelete =  function(req, res, next) {

    var token= req.headers['token'];
    jwt.verify(token, SECRET, async function(err, decoded) {
        if (err) {
            res.json('Invalid Token');
        }else{
            var msg = '';
            var success = '';
            var sub_category_id = req.body.data.sub_category_id;
            
            if(sub_category_id != '') {
                if(category != '') {
                    var isdeleted = await models.category.destroy({where:{sub_category_id:sub_category_id} });
                    if(isdeleted){
                        msg = "Sub category successfully deleted!";
                        success ="true";
                    }else{
                        msg = "Failed to delete sub category!";
                        success ="false";
                    }
                } else {
                    msg = "Sub category not found!";
                    success ="false"; 
                }
            } else {
                msg = "Sub category not found!";
                success ="false"; 
            }
            res.status(200).send({ status:200,success: success, message:msg});   
        }
    });    
}; 

/************************* Sub Category delete Api Start  **************************************/