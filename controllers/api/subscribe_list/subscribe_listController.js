var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);


exports.subscribe = async function(req, res, next) {
    var email = req.body.data.email;
    // var status = req.body.data.status; 
    if(email && email !='' ){

        var email_verification =await sequelize.query("select subscribe_list.* FROM `subscribe_list` where email = '"+email+"'",{ type: Sequelize.QueryTypes.SELECT });
        if(email_verification.length >= 1){
            res.status(200).json({ status:200, success: false, message: "Email ID already subscribed."});
        }else{

            models.subscribe_list.create({
                email: email,
                status: 'Unconfirmed'    
            }).then (async function(subscribe){
                if(subscribe){

                    ///////////////////// Newsletter Subscription Confirmation email start ///////////

                    var email_template_data =await sequelize.query("SELECT email_template.*, email_template_type.title from email_template LEFT JOIN email_template_type ON email_template.email_template_type = email_template_type.email_template_type_id where email_template.email_template_id = 9",{ type: Sequelize.QueryTypes.SELECT });
                    var final_data = email_template_data[0].content.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
                    // return new Promise((resolve, reject) => {
                        console.log('5555555555555555');
                        var data = {
                            from: 'Redford Team <admin@redfordrecruiters.com>',
                            to: [email],
                            subject: 'Redford Recruiters : '+email_template_data[0].subject+'',
                            html: '<!DOCTYPE html>'+
                            ''+final_data+'' 
                        };
                        mailgun.messages().send(data, function (error, body) {
                            console.log('6666666666666');
                            console.log(body);
                            // if (error) {
                                // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", client_full_data: client_full_data[0], token:token }));
                            // }
                            // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                        });
                        console.log('7777777777777777777');
                    // });

                    ///////////////////// Newsletter Subscription Confirmation email end ///////////

                    res.status(200).send({ status:200, success: true, message: "Thank you for subscribing.", details:subscribe });
                }else{
                    res.status(200).send({ status:200, success: false});
                }
            })

        }
    }else{
        res.status(200).json({ status:200, success: false, message: "Email ID is required."});
    } 
}