var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);



/************************* client registration form Api Start *************************/
exports.client_registration_form = async function(req, res, next) {
    
    var categorieslist = await models.category.findAll({where: {status:"active"},attributes: ['category_id', 'title']});

    if(categorieslist){
        res.status(200).send({ status:200,success: true, categorieslist:categorieslist });
    }else{
        res.status(200).send({ status:200,success: false, message:"No data found!"});
    }

}
/************************* client registration form Api Ends *************************/


/************************* client sign-up start *************************/

exports.client_registration =async function(req, res, next) {

    console.log(req.body.data);
    
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var email = req.body.data.email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var company_name = req.body.data.company_name;
    var company_size = req.body.data.company_size;
    var company_type = req.body.data.company_type;
    //var terms = req.body.data.terms;
    var password = req.body.data.password;
    var hash = bcrypt.hashSync(password);
    var terms = req.body.data.terms;
    
    
    if(first_name !='' && last_name !='' && email !='' && mobile !='' && password !='' && mob_country_code !=''){
        var check_email =await sequelize.query("SELECT * FROM clients  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
        var check_mobile =await sequelize.query("SELECT * FROM client_basic_info  WHERE mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

        var email_msg='';
        var mobile_msg='';
        if(check_email.length>0){
            email_msg= "Email ID already exists!";
        }if (check_mobile.length>0){
            mobile_msg= "Mobile number already exists!";
        }

        if(check_email.length==0 && check_mobile.length==0){
            models.clients.create({
                username : email,
                password : hash,
                type : "client",
                terms : terms
            }).then(async function(client){
                if(client){
                    models.client_basic_info.create({
                        client_id: client.client_id,
                        first_name : first_name,
                        last_name : last_name,
                        mob_country_code : mob_country_code,
                        mobile: mobile,
                        //company_name : company_name,
                        //company_type : company_type,
                        //company_size : company_size
                    }).then(async function(client_info){

                        models.companies.create({
                            client_id: client.client_id,
                            // first_name : first_name,
                            // last_name : last_name,
                            // mob_country_code : mob_country_code,
                            // mobile: mobile,
                            company_name : company_name,
                            company_size : company_size,
                            category : company_type
                        }).then(async function(companies){
            
                            var dir = './public/contents/clients/'+client.client_id; 
                            console.log(dir);
                            if (!fs.existsSync(dir)){
                                fs.mkdirSync(dir);                  
                            }

                            var client_full_data =await sequelize.query("SELECT clients.*, client_basic_info.*, companies.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id LEFT JOIN companies ON clients.client_id = companies.client_id where clients.client_id ="+client.client_id,{ type: Sequelize.QueryTypes.SELECT });
                            var token = jwt.sign({client}, SECRET, { expiresIn: 18000 });

                            ///////////////////// email to client for successfully registration start ///////////

                            var email_template_data =await sequelize.query("SELECT email_template.*, email_template_type.title from email_template LEFT JOIN email_template_type ON email_template.email_template_type = email_template_type.email_template_type_id where email_template.email_template_id = 5 limit 1",{ type: Sequelize.QueryTypes.SELECT });
                            var final_data = email_template_data[0].content.replace(/{{company_name}}/g, ''+company_name+'');
                            var client_email_final_data = final_data.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
                            // return new Promise((resolve, reject) => {
                                console.log('5555555555555555');
                                var data = {
                                    from: 'Redford Team <admin@redfordrecruiters.com>',
                                    to: [email],
                                    subject: 'Redford Recruiters : '+email_template_data[0].subject+'',
                                    html: '<!DOCTYPE html>'+
                                    ''+client_email_final_data+'' 
                                };
                                mailgun.messages().send(data, function (error, body) {
                                    console.log('6666666666666');
                                    console.log(body);
                                    // if (error) {
                                        // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", client_full_data: client_full_data[0], token:token }));
                                    // }
                                    // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                                });
                                console.log('7777777777777777777');
                            // });

                            ///////////////////// email to client for successfully registration end ///////////

                            ///////////////////// email(ack) to admin for client successfully registration start ///////////
                        
                            var email_template_data_for_admin =await sequelize.query("SELECT email_template.* from email_template where email_template.email_template_id = 6",{ type: Sequelize.QueryTypes.SELECT });
                            var client_company_category =await sequelize.query("SELECT categories.* from categories where category_id = "+company_type,{ type: Sequelize.QueryTypes.SELECT });
                            var replace = email_template_data_for_admin[0].content.replace(/{{company_name}}/g, ''+company_name+'');
                            var final_data = replace.replace(/{{company_category}}/g, ''+client_company_category[0].title+'');
                            var admin_email_final_data = final_data.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');

                            var admin_details =await sequelize.query("SELECT admins.* from admins where admin_id = 8",{ type: Sequelize.QueryTypes.SELECT });
                            // return new Promise((resolve, reject) => {
                                console.log('5555555555555555');
                                var data1 = {
                                    from: 'Redford Team <admin@redfordrecruiters.com>',
                                    to: [admin_details[0].username],
                                    subject: 'Redford Recruiters : '+email_template_data_for_admin[0].subject+'',
                                    html: '<!DOCTYPE html>'+
                                    ''+admin_email_final_data+'' 
                                };
                                mailgun.messages().send(data1, function (error, body) {
                                    console.log('6666666666666');
                                    console.log(body);
                                    // if (error) {
                                        // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  }));
                                    // }
                                    // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                                });
                                console.log('7777777777777777777');
                            // });
                            ///////////////////// email(ack) to admin for client successfully registration end ///////////

                            res.status(200).json({ success: "true", message: "User successfully registered. You can login now.", client_full_data: client_full_data[0], token:token }); 
                        });
                    });
                }else{
                    res.status(200).json({ success: "false",message: "Registration failed!"});
                }
           
            });
        }else{
            res.status(200).json({ success: "false", message: "Email or mobile already exists!"});
        }
 
    }else{
        res.status(200).json({ success: "false",message: "All fileds are required!"});
    }  
}

/************************* client sign-up ends *************************/


/************************* client sign-in start *************************/

exports.client_signin =async function(req, res, next) {

    var username = req.body.data.username;
    var password = req.body.data.password;

    if(username !='' && password !=''){
        models.clients.findOne({ where: {username :username} }).then(async function(client) {

            if(client){
                //var client_fulldata = sequelize.query("SELECT clients.*, client_basic_info.name as name, client_basic_info.first_name as first_name, client_basic_info.middle_name as middle_name, client_basic_info.last_name as last_name from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+client.client_id,{ type: Sequelize.QueryTypes.SELECT })
                //client_fulldata.then(function (client_fulldata) {
                var client_full_data =await sequelize.query("SELECT clients.*, client_basic_info.*, companies.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id LEFT JOIN companies ON clients.client_id = companies.client_id where clients.client_id ="+client.client_id,{ type: Sequelize.QueryTypes.SELECT });

                    if(!bcrypt.compareSync(password, client.password)){
                        res.status(200).send({ success: "false", message: "Invalid username and password!" });
                    }else{
                        var token =    jwt.sign({client}, SECRET, { expiresIn: 18000 });
                        res.status(200).send({ success: "true", message:"successfully login", details:client_full_data[0], token: token }); 
                    }

                //}).catch(function(error) {
               //     res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                //}); 

            // if(client!=null){
            //     adm = client.toJSON();	   
            //     if(!bcrypt.compareSync(password, adm.password)) {		   
            //         res.status(200).send({ message: "Invalid username and password!" });
            //     } else {	  
            //         if (username == client.username) {              
            //             var token =    jwt.sign({client}, SECRET, { expiresIn: 18000 });
            //             res.status(200).send({ success: "true", message:"successfully login", details:client, token: token });                
            //         }else{				
            //             res.status(200).send({ message: "Invalid username and password!" });
            //         }            
            //     } 

            }else{                
                res.status(200).send({ success: "false", message: "Invalid username and password!" });
            }    
        })
        .catch(function(error) {
            return res.send(error);        
        }); 
    }else{
        res.status(200).send({ success: "false", message: "All fields are required!" });
    } 
}
/************************* client sign-in ends *************************/

/************************* client provider registration start *************************/

exports.client_provider_registration =async function(req, res, next) {
    
    var email = req.body.email;
    var provider_id = req.body.id;
    var provider = req.body.provider;
    var provider_image = req.body.image;
    var provider_name = req.body.name;
    
    if(email !='' && provider_id !='' && provider !='' && provider_image !='' && provider_name !=''){
        var check_email =await sequelize.query("SELECT * FROM clients  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

        var email_msg='';
        if(check_email.length>0){
            email_msg= "Email ID already exists!";
        }

        if(check_email.length==0){
            models.clients.create({
                username : email,
                provider_id : provider_id,
                provider : provider,
                provider_image : provider_image,
                type : "client"
            }).then(function(client){
                if(client){
                    models.client_basic_info.create({
                        client_id: client.client_id,
                        name : provider_name,
                    }).then(function(client_info){
            
                        var dir = './public/contents/clients/'+client.client_id; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }
                        var token = jwt.sign({client}, SECRET, { expiresIn: 18000 });
                        res.status(200).json({ success: "true",message: "User successfully register",token:token, client_email:client.username,client_info:client_info }); 
                    });
                }else{
                    res.status(200).json({ success: "false",message: "Registration failed!"});
                }
           
            });
        }else{
            res.status(200).json({ success: "false",email_msg:email_msg});
        }
 
    }else{
        res.status(200).json({ success: "false",message: "All fileds are required!"});
    }  
}

/************************* client provider registration ends *************************/


/************************* client provider sign-in start *************************/

exports.client_provider_signin = async function(req, res, next) {

    var username = req.body.email;
    var provider_id = req.body.id;
    var provider_image = req.body.image;
    var client_company = '';

    if(username !='' && provider_id !=''){

        var client =await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id  where clients.username ='"+username+"' and clients.provider_id ='"+provider_id+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
        if(client.length > 0){

            var client_company_check =await sequelize.query("SELECT companies.* FROM companies  WHERE client_id="+client[0].client_id,{ type: Sequelize.QueryTypes.SELECT }); 
            if(client_company_check.length > 0 ){
                var client_company = 'true';
            }else{
                var client_company = 'false';
            }
            
            if(client[0].provider_image == provider_image){
                var token =    jwt.sign({client}, SECRET, { expiresIn: 18000 });
                res.status(200).send({ success: "true", token: token, details:client[0], client_company:client_company });
            }else{

                models.clients.update({ 
                    provider_image: provider_image,
                },{where:{client_id:client[0].client_id}}).then(async function(client_update) {
                   
                    var update_client =await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id  where clients.username ='"+username+"' and clients.provider_id ='"+provider_id+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
                    var token =    jwt.sign({client}, SECRET, { expiresIn: 18000 });
                    res.status(200).send({ success: "true", token: token, details:update_client[0], client_company:client_company  });
                })
                .catch(function(error) {
                    return res.send( {success: 'false', message: "error happened"});
                });
            }
            
        }else{                
            res.status(200).send({ success: 'false', message: "Client not registered" });
        }
    }else{
        res.status(200).send({ message: "All fields are required!" });
    } 
}
/************************* client provider sign-in ends *************************/


/************************* provider client update start *************************/
exports.provider_client_update = async function(req, res, next) {

    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var company_name = req.body.data.company_name;
    var company_size = req.body.data.company_size;
    var company_type = req.body.data.company_type;
    var client_id = req.body.data.client_id;

    if(client_id && client_id !=''){
         
        provider_client = await sequelize.query("select clients.* from clients where client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });
            
        if(provider_client.length > 0){

            if(mob_country_code && mob_country_code !='' && mobile && mobile !='' && company_name && company_name !='' && company_size && company_size !='' && company_type && company_type !=''){
               
                    models.client_basic_info.update({ 
                        mob_country_code: mob_country_code,
                        mobile: mobile,

                    },{where:{client_id:client_id}}).then(async function(client_basic_info_update) {

                        if(client_basic_info_update){

                            models.companies.create({
                                client_id: client_id,
                                company_name : company_name,
                                company_size : company_size,
                                category : company_type,
                            }).then(async function(companies){
                    
                                var client_full_data =await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });
                                var token = jwt.sign({client_basic_info_update}, SECRET, { expiresIn: 18000 });

                                ///////////////////// email to client for successfully registration start ///////////

                                var email_template_data =await sequelize.query("SELECT email_template.*, email_template_type.title from email_template LEFT JOIN email_template_type ON email_template.email_template_type = email_template_type.email_template_type_id where email_template.email_template_id = 5 limit 1",{ type: Sequelize.QueryTypes.SELECT });
                                var final_data = email_template_data[0].content.replace(/{{company_name}}/g, ''+company_name+'');
                                var client_email_final_data = final_data.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
                                // return new Promise((resolve, reject) => {
                                    console.log('5555555555555555');
                                    var data = {
                                        from: 'Redford Team <admin@redfordrecruiters.com>',
                                        to: [client_full_data[0].username],
                                        subject: 'Redford Recruiters : '+email_template_data[0].subject+'',
                                        html: '<!DOCTYPE html>'+
                                        ''+client_email_final_data+'' 
                                    };
                                    mailgun.messages().send(data, function (error, body) {
                                        console.log('6666666666666');
                                        console.log(body);
                                        // if (error) {
                                            // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", client_full_data: client_full_data[0], token:token }));
                                        // }
                                        // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                                    });
                                    console.log('7777777777777777777');
                                // });

                                ///////////////////// email to client for successfully registration end ///////////

                                ///////////////////// email(ack) to admin for client successfully registration start ///////////
                            
                                var email_template_data_for_admin =await sequelize.query("SELECT email_template.* from email_template where email_template.email_template_id = 6",{ type: Sequelize.QueryTypes.SELECT });
                                var client_company_category =await sequelize.query("SELECT categories.* from categories where category_id = "+company_type,{ type: Sequelize.QueryTypes.SELECT });
                                var replace = email_template_data_for_admin[0].content.replace(/{{company_name}}/g, ''+company_name+'');
                                var final_data = replace.replace(/{{company_category}}/g, ''+client_company_category[0].title+'');
                                var admin_email_final_data = final_data.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');

                                var admin_details =await sequelize.query("SELECT admins.* from admins where admin_id = 8",{ type: Sequelize.QueryTypes.SELECT });
                                // return new Promise((resolve, reject) => {
                                    console.log('5555555555555555');
                                    var data1 = {
                                        from: 'Redford Team <admin@redfordrecruiters.com>',
                                        to: [admin_details[0].username],
                                        subject: 'Redford Recruiters : '+email_template_data_for_admin[0].subject+'',
                                        html: '<!DOCTYPE html>'+
                                        ''+admin_email_final_data+'' 
                                    };
                                    mailgun.messages().send(data1, function (error, body) {
                                        console.log('6666666666666');
                                        console.log(body);
                                        // if (error) {
                                            // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  }));
                                        // }
                                        // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                                    });
                                    console.log('7777777777777777777');
                                // });
                                ///////////////////// email(ack) to admin for client successfully registration end ///////////
                                
                                res.status(200).send({ success: "true", message:"Client update successful", clientdetail:client_full_data[0], token: token }); 
                            });

                        }else{
                            res.status(200).json({status:200, success: "false",message: "Client update failed!"});
                        }
                    })
                    .catch(function(error) {
                        return res.send( {status:200, success: "false", message: "error happened"});
                    });
            }else{
                res.status(200).send({status:200, success: "false", message: "All fields are required!" });
            }
        }else{                
            res.status(200).send({status:200, success: "false", message: "Client not registered" });
        }     
    }else{
        res.status(200).send({status:200, success: "false", message: "Client id is required!" });
    } 
}
/************************* provider client update ends *************************/



/************************* client company submit start *************************/

exports.client_company_submit = async function(req, res, next) {
    console.log(req.body.data);
    
    var client_id = req.body.data.client_id;
    var company_name = req.body.data.company_name;
    var company_size = req.body.data.company_size;
    var category = req.body.data.category;
    var company_email = req.body.data.company_email;
    var company_country_code = req.body.data.company_country_code;
    var company_phone = req.body.data.company_phone;
    var company_description = req.body.data.company_description;
    var address = req.body.data.address;
    var city = req.body.data.city;
    var state = req.body.data.state;
    var country = req.body.data.country;
    var website = req.body.data.website;
    var contact_person_name = req.body.data.contact_person_name;
    var contact_person_email = req.body.data.contact_person_email;
    var contact_person_country_code = req.body.data.contact_person_country_code;
    var contact_person_mobile = req.body.data.contact_person_mobile;

    var company_id = req.body.data.company_id;
    var company_contact_info_id = req.body.data.company_contact_info_id;

    var file_extension = req.body.data.file_extension;
    var company_logo = req.body.data.company_logo;
    // var logo = './public/contents/clients/'+client_id+'/companies/'+company_name.toString().toLowerCase() .replace(/\s+/g, '-')+'.'+file_extension;

    //var logo = req.body.data.logo;
    // return res.send(req.body.data)

    if(client_id && client_id !='' && company_name && company_name !='' && company_size && company_size !='' && category && category !='' && company_email && company_email !='' && address && address !='' && city && city !='' && state && state !='' && country && country !='' && contact_person_name && contact_person_name !='' && contact_person_email && contact_person_email !='' && contact_person_country_code && contact_person_country_code!='' && contact_person_mobile && contact_person_mobile !=''){
        console.log('99999999999999999');
        
        if(!company_id){

            if(company_logo && company_logo !='' && file_extension && file_extension !=''){
                var logo = 'contents/clients/'+client_id+'/companies/'+company_name.toString().toLowerCase().replace(/\s+/g, '-')+'.'+file_extension;
            }else{
                var logo ='';
            }

            models.companies.create({ 

                client_id: client_id,
                company_name: company_name,
                company_size: company_size,
                category: category,
                company_email: company_email,
                company_country_code: company_country_code,
                company_phone: company_phone,
                company_description: company_description,
                logo: logo,
                //createdBy: client_id

            }).then(async function(companies) {

                if(companies){
                    console.log('8888888888888888888');
                    if(company_logo && company_logo !='' && file_extension && file_extension !=''){
                        var dir = './public/contents/clients/'+client_id+'/companies'; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }

                        try {
                            const path = './public/'+logo;
                            const imgdata = company_logo;
                            const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                            fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                        } catch (e) {
                            next(e);
                        }
                    }

                    models.company_contact_info.create({ 

                        company_id: companies.company_id,
                        address: address,
                        city: city,
                        state: state,
                        country: country,
                        website: website,
                        contact_person_name: contact_person_name,
                        contact_person_email: contact_person_email,
                        contact_person_country_code: contact_person_country_code,
                        contact_person_mobile: contact_person_mobile,
                        //createdBy: client_id
            
                    }).then(async function(company_contact_info) {
                        console.log('7777777777777777777777');

                        res.status(200).send({ status:200, success: true, message: "Company information successfully added" });
                    })

                }else{
                    res.status(200).json({ status:200, success: false, message: "Company information not submitted"});
                }
            })

        }else{
            console.log('333333333333333333333333333');
            models.companies.update({ 

                client_id: client_id,
                company_name: company_name,
                company_size: company_size,
                category: category,
                company_email: company_email,
                company_country_code: company_country_code,
                company_phone: company_phone,
                company_description: company_description,
                //logo: logo,

            },{where:{company_id:company_id}}).then(async function(companies) {

                if(companies){
                    console.log('8888888888888888888');

                    if(company_logo && company_logo !='' && file_extension && file_extension !=''){

                        var logo = 'contents/clients/'+client_id+'/companies/'+company_name.toString().toLowerCase().replace(/\s+/g, '-')+'.'+file_extension;

                        var dir = './public/contents/clients/'+client_id+'/companies'; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }

                        try {
                            const path = './public/'+logo;
                            const imgdata = company_logo;
                            const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                            fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                        } catch (e) {
                            next(e);
                        }

                        models.companies.update({
                            logo: logo,
                        },{where:{company_id:company_id}})
                    }

                    if(company_contact_info_id && company_contact_info_id !=''){

                        models.company_contact_info.update({ 

                            company_id: company_id,
                            address: address,
                            city: city,
                            state: state,
                            country: country,
                            website: website,
                            contact_person_name: contact_person_name,
                            contact_person_email: contact_person_email,
                            contact_person_country_code: contact_person_country_code,
                            contact_person_mobile: contact_person_mobile,
                
                        },{where:{company_contact_info_id:company_contact_info_id}}).then(async function(company_contact_info) {
                            console.log('7777777777777777777777');

                            res.status(200).send({ status:200, success: true, message: "Company information successfully updated" });
                        })

                    }else{
                        //res.status(200).json({ status:200, success: false, message: "Company contact info id is required!"});

                        models.company_contact_info.create({ 

                            company_id: company_id,
                            address: address,
                            city: city,
                            state: state,
                            country: country,
                            website: website,
                            contact_person_name: contact_person_name,
                            contact_person_email: contact_person_email,
                            contact_person_country_code: contact_person_country_code,
                            contact_person_mobile: contact_person_mobile,
                            //createdBy: client_id
                
                        }).then(async function(company_contact_info) {
                            console.log('7777777777777777777777');
    
                            res.status(200).send({ status:200, success: true, message: "Company information successfully updated" });
                        })
                    }

                }else{
                    res.status(200).json({ status:200, success: false, message: "Company information not submitted"});
                }
            })
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "All fields are required!"});
    }  
}

/************************* client company submit ends *************************/


/************************* client company listing start *************************/

exports.client_company_listing = async function(req, res, next) {
    console.log(req.body.data);

    var offset = req.body.data.start ? req.body.data.start : 0;
    var limit = req.body.data.limit ? req.body.data.limit : 10;

    var client_id = req.body.data.client_id;
    var user_type = req.body.data.user_type;
    var resultArray = [];

    if(client_id && client_id !='' ){
        
        var client_company_count = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });
        var client_company_listing = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.client_id ="+client_id+" order by companies.company_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });
        
        if(client_company_listing.length > 0){

            var i = 0;
            client_company_listing.forEach(function(element,index){ 

                if(element.logo !='' && element.logo != null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "company_name": element.company_name,
                    "company_size": element.company_size,
                    "company_email":element.company_email,
                    "company_country_code":element.company_country_code,
                    "company_phone": element.company_phone,
                    "category": element.category,
                    "logo": company_logo,
                    "company_description": element.company_description,
                    "createdBy": element.createdBy,
                    "updatedBy":element.updatedBy,
                    "createdAt": element.createdAt,
                    "updatedAt": element.updatedAt,
                    "company_contact_info_id": element.company_contact_info_id,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "website": element.website,
                    "contact_person_name": element.contact_person_name,
                    "contact_person_email": element.contact_person_email,
                    "contact_person_country_code": element.contact_person_country_code,
                    "contact_person_mobile": element.contact_person_mobile,
                    "category_title": element.category_title
                });    
                i++;  
                if(i== client_company_listing.length){
                    resultArray.sort(function(a,b){
                        var titleA = a.company_name.toLowerCase(), titleB = b.company_name.toLowerCase();
                        if (titleA < titleB) return -1;
                        if (titleA > titleB) return 1;
                        return 0;
                    });
                    res.status(200).send({ status:200, success: true, client_company_listing:resultArray, client_company_count:client_company_count.length });
                }              
            });

            // res.status(200).send({ status:200, success: true, client_company_listing:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Company not found for this client!"});
        }

    }else{

        if(user_type && user_type =='admin' ){

            var client_company_count = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id",{ type: Sequelize.QueryTypes.SELECT });
            var client_company_listing = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id order by companies.company_name ASC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });
            
            if(client_company_listing.length > 0){

                var i = 0;
                client_company_listing.forEach(function(element,index){ 

                    if(element.logo !='' && element.logo != null){
                        var company_logo = req.app.locals.baseurl+element.logo;
                    }else{
                        var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                    }

                    resultArray.push({
                        "company_id": element.company_id,
                        "client_id": element.client_id,
                        "company_name": element.company_name,
                        "company_size": element.company_size,
                        "company_email":element.company_email,
                        "company_country_code":element.company_country_code,
                        "company_phone": element.company_phone,
                        "category": element.category,
                        "logo": company_logo,
                        "company_description": element.company_description,
                        "createdBy": element.createdBy,
                        "updatedBy":element.updatedBy,
                        "createdAt": element.createdAt,
                        "updatedAt": element.updatedAt,
                        "company_contact_info_id": element.company_contact_info_id,
                        "address": element.address,
                        "city": element.city,
                        "state": element.state,
                        "country": element.country,
                        "website": element.website,
                        "contact_person_name": element.contact_person_name,
                        "contact_person_email": element.contact_person_email,
                        "contact_person_country_code": element.contact_person_country_code,
                        "contact_person_mobile": element.contact_person_mobile,
                        "category_title": element.category_title
                    });    
                    i++;             
                });

                res.status(200).send({ status:200, success: true, client_company_listing:resultArray, client_company_count:client_company_count.length });
            }else{
                res.status(200).json({ status:200, success: false, message: "Company not found!"});
            }

        }else{
            res.status(200).json({ status:200, success: false, message: "Client id is required!"});
        }
        
    }    
}

/************************* client company listing ends *************************/


/************************* client company details start *************************/

exports.client_company_details = async function(req, res, next) {
    
    var company_id = req.body.data.company_id;
    var resultArray = [];

    if(company_id && company_id !=''){

        // var company_details = await sequelize.query("SELECT companies.*, company_contact_info.*, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });
        // var company_details = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });
        var company_details = await sequelize.query("SELECT companies.*, company_contact_info.company_contact_info_id, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, company_contact_info.contact_person_name, company_contact_info.contact_person_email, company_contact_info.contact_person_country_code, company_contact_info.contact_person_mobile, categories.title as category_title, (SELECT COUNT(*) from jobs WHERE jobs.company_id = "+company_id+") as total_job from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });

        if(company_details.length > 0){

            var i = 0;
            company_details.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "company_name": element.company_name,
                    "company_size": element.company_size,
                    "company_email":element.company_email,
                    "company_country_code":element.company_country_code,
                    "company_phone": element.company_phone,
                    "category": element.category,
                    "logo": company_logo,
                    "company_description": element.company_description,
                    "createdBy": element.createdBy,
                    "updatedBy":element.updatedBy,
                    "createdAt": element.createdAt,
                    "updatedAt": element.updatedAt,
                    "company_contact_info_id": element.company_contact_info_id,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "website": element.website,
                    "contact_person_name": element.contact_person_name,
                    "contact_person_email": element.contact_person_email,
                    "contact_person_country_code": element.contact_person_country_code,
                    "contact_person_mobile": element.contact_person_mobile,
                    "category_title": element.category_title,
                    "total_job": element.total_job
                });    
                i++;             
            });

            // res.status(200).send({ status:200, success: true, client_company_listing:resultArray });
            res.status(200).send({ status:200, success: true, company_details:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "Company details not found"});
        }

        // res.status(200).send({ status:200, success: true, company_details:company_details });
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Company id is required!"});
    }  
}

/************************* client company details ends *************************/


/************************* client company delete start *************************/

exports.client_company_delete = function(req, res, next) {
    console.log(req.body.data);
    var company_id = req.body.data.company_id;

    if(company_id && company_id !='' ){

        models.companies.destroy({ 
            where:{company_id:company_id}
        }).then(function(value) {

            if(value){
                models.company_contact_info.destroy({ 
                    where:{company_id:company_id}
                }).then(function(company_contact_info) {
                    res.status(200).send({ status:200, success: true, message: "Client company successfully deleted" });
                });
            }else{
                res.status(200).json({ status:200, success: false, message: "Client company not deleted"});
            }

        });

    }else{
        res.status(200).json({ status:200, success: false,message: "Company id is required!"});
    } 
}

/************************* client company delete ends *************************/


/************************* client profile information start *************************/

exports.client_profile_information = async function(req, res, next) {

    var client_id = req.body.data.client_id;
    var resultArray = [];

    if(client_id && client_id !=''){

        var client_profile_info = await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

        if(client_profile_info.length > 0){

            var i = 0;
            var client_first_name = '';
            var client_last_name = '';

            client_profile_info.forEach(function(element,index){ 

                if(element.first_name !='' && element.first_name!= null){
                    client_first_name = element.first_name;
                    client_last_name = element.last_name;
                }else if(element.name !='' && element.name!= null){
                    client_name = element.name.split(' ');
                    client_first_name = client_name[0];
                    for( var j =1; j<=client_name.length-1; j++){
                        client_last_name += client_name[j]+ " ";
                    }
                    client_last_name = client_last_name.substring(0, client_last_name.length-1);
                }

                if(element.profile_picture !='' && element.profile_picture!= null){
                    var client_profile_picture = req.app.locals.baseurl+element.profile_picture;
                }else if(element.provider_image !='' && element.provider_image!= null){
                    var client_profile_picture = element.provider_image;
                }else{
                    var client_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "client_id": element.client_id,
                    "username": element.username,
                    "client_profile_picture": client_profile_picture,
                    "first_name": client_first_name,
                    "last_name": client_last_name,
                    "alternate_email": element.alternate_email,
                    "mob_country_code": element.mob_country_code,
                    "mobile": element.mobile,
                    "alt_mob_country_code": element.alt_mob_country_code,
                    "alternate_mobile": element.alternate_mobile,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "gender": element.gender,
                    "nationality": element.nationality
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, client_details:resultArray[0] });
        }else{
            res.status(200).json({ status:200, success: false, message: "Client not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    }    
}


/************************* client profile information ends *************************/


// /************************* client profile update start *************************/

// exports.client_profile_update = async function(req, res, next) {
//     console.log(req.body.data);

//     var client_id = req.body.data.client_id;
//     var first_name = req.body.data.first_name;
//     var last_name = req.body.data.last_name;
//     var gender = req.body.data.gender;
//     var nationality = req.body.data.nationality;
//     var alternate_email = req.body.data.alternate_email;
//     var mob_country_code = req.body.data.mob_country_code;
//     var mobile = req.body.data.mobile;
//     var alt_mob_country_code = req.body.data.alt_mob_country_code;
//     var alternate_mobile = req.body.data.alternate_mobile;
//     var address = req.body.data.address;
//     var city = req.body.data.city;
//     var state = req.body.data.state;
//     var country = req.body.data.country;
//     var profile_pic = req.body.data.profile_pic;
//     var file_extension = req.body.data.file_extension;

//     if(client_id && client_id !=''){
//         console.log('11111111111111111111');

//         var client_validation = await sequelize.query("SELECT clients.* from clients where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

//         if(client_validation.length > 0){
//             console.log('22222222222222222222222');

//             if(first_name && first_name !='' && last_name && last_name !='' && mob_country_code && mob_country_code !='' && mobile && mobile !='' && gender && gender !='' && address && address !='' && state && state !='' && country && country !=''){

//                 if(profile_pic && profile_pic !='' && file_extension && file_extension !=''){

//                     console.log('333333333333333333');
//                     var client_profile_pic = 'contents/clients/'+client_id+'/profile_picture.'+file_extension;
//                     try {
//                         const path = './public/'+client_profile_pic;
//                         const imgdata = profile_pic;
//                         const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
//                         fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
//                     } catch (e) {
//                         next(e);
//                     }
//                     console.log('444444444444444444444');

//                     models.clients.update({
//                         profile_picture: client_profile_pic,
//                     },{where:{client_id:client_id}})
//                 }
//                 console.log('555555555555555555');

//                 models.client_basic_info.update({ 

//                     first_name: first_name,
//                     last_name: last_name,
//                     gender: gender,
//                     nationality: nationality,
//                     alternate_email: alternate_email,
//                     mob_country_code: mob_country_code,
//                     mobile: mobile,
//                     alt_mob_country_code: alt_mob_country_code,
//                     alternate_mobile: alternate_mobile,
//                     address: address,
//                     city: city,
//                     state: state,
//                     country: country,

//                 },{where:{client_id:client_id}}).then(function(client_basic_info) {

//                     res.status(200).send({ status:200, success: true, message: "Client profile successfully updated" });
//                 })

//             }else{
//                 res.status(200).json({ status:200, success: false, message: "All fields are required!"});
//             }

//         }else{
//             res.status(200).json({ status:200, success: false, message: "Client not found"});
//         }
        
//     }else{
//         res.status(200).json({ status:200, success: false, message: "Client id is required!"});
//     }    
// }


// /************************* client profile update ends *************************/


/************************* client profile update start *************************/

exports.client_profile_update = async function(req, res, next) {
    console.log(req.body.data);

    var client_id = req.body.data.client_id;
    var first_name = req.body.data.first_name;
    var last_name = req.body.data.last_name;
    var email = req.body.data.email;
    var gender = req.body.data.gender;
    var nationality = req.body.data.nationality;
    var alternate_email = req.body.data.alternate_email;
    var mob_country_code = req.body.data.mob_country_code;
    var mobile = req.body.data.mobile;
    var alt_mob_country_code = req.body.data.alt_mob_country_code;
    var alternate_mobile = req.body.data.alternate_mobile;
    var address = req.body.data.address;
    var city = req.body.data.city;
    var state = req.body.data.state;
    var country = req.body.data.country;
    var profile_pic = req.body.data.profile_pic;
    var file_extension = req.body.data.file_extension;

    var password = req.body.data.password;
    var hash = bcrypt.hashSync(password);

    if(client_id && client_id !=''){
        console.log('11111111111111111111');

        var client_validation = await sequelize.query("SELECT clients.* from clients where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

        if(client_validation.length > 0){
            console.log('22222222222222222222222');

            if(first_name && first_name !='' && last_name && last_name !='' && mob_country_code && mob_country_code !='' && mobile && mobile !='' && gender && gender !='' && address && address !='' && state && state !='' && country && country !=''){

                if(profile_pic && profile_pic !='' && file_extension && file_extension !=''){

                    console.log('333333333333333333');
                    var client_profile_pic = 'contents/clients/'+client_id+'/profile_picture.'+file_extension;
                    try {
                        const path = './public/'+client_profile_pic;
                        const imgdata = profile_pic;
                        const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                        fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                    } catch (e) {
                        next(e);
                    }
                    console.log('444444444444444444444');

                    models.clients.update({
                        profile_picture: client_profile_pic,
                    },{where:{client_id:client_id}})
                }
                console.log('555555555555555555');

                models.client_basic_info.update({ 

                    first_name: first_name,
                    last_name: last_name,
                    gender: gender,
                    nationality: nationality,
                    alternate_email: alternate_email,
                    mob_country_code: mob_country_code,
                    mobile: mobile,
                    alt_mob_country_code: alt_mob_country_code,
                    alternate_mobile: alternate_mobile,
                    address: address,
                    city: city,
                    state: state,
                    country: country,

                },{where:{client_id:client_id}}).then(function(client_basic_info) {

                    res.status(200).send({ status:200, success: true, message: "Client profile successfully updated" });
                })

            }else{
                res.status(200).json({ status:200, success: false, message: "All fields are required!"});
            }

        }else{
            res.status(200).json({ status:200, success: false, message: "Client not found"});
        }
        
    }else{
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaa');
        // res.status(200).json({ status:200, success: false, message: "Client id is required!"});

        if(first_name && first_name !='' && last_name && last_name !='' && email && email != '' && password && password != '' && mob_country_code && mob_country_code !='' && mobile && mobile !='' && gender && gender !='' && address && address !='' && state && state !='' && country && country !=''){
            console.log('bbbbbbbbbbbbbbbbbbbbbbbb');
            var check_email =await sequelize.query("SELECT * FROM clients  WHERE username='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
            var check_mobile =await sequelize.query("SELECT * FROM client_basic_info  WHERE mobile='"+mobile+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            var email_msg='';
            var mobile_msg='';
            if(check_email.length>0){
                email_msg= "Email ID already exists!";
            }if (check_mobile.length>0){
                mobile_msg= "Mobile number already exists!";
            }

            if(check_email.length==0 && check_mobile.length==0){
                console.log('ccccccccccccccccccccccccccccc');

                models.clients.create({
                    username : email,
                    password : hash,
                    type : "client",
                }).then(async function(client){
                    if(client){
                        console.log('dddddddddddddddddddddddddddd');

                        var dir = './public/contents/clients/'+client.client_id; 
                        console.log(dir);
                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);                  
                        }
                        
                        models.client_basic_info.create({
                            client_id: client.client_id,
                            first_name: first_name,
                            last_name: last_name,
                            gender: gender,
                            nationality: nationality,
                            alternate_email: alternate_email,
                            mob_country_code: mob_country_code,
                            mobile: mobile,
                            alt_mob_country_code: alt_mob_country_code,
                            alternate_mobile: alternate_mobile,
                            address: address,
                            city: city,
                            state: state,
                            country: country,
                        }).then(async function(client_basic_info){
                            console.log('eeeeeeeeeeeeeeeeeeeeeeeeee');

                            if(profile_pic && profile_pic !='' && file_extension && file_extension !=''){
                                console.log('fffffffffffffffffffffffff');
                               
                                var client_profile_pic = 'contents/clients/'+client.client_id+'/profile_picture.'+file_extension;
                                try {
                                    const path = './public/'+client_profile_pic;
                                    const imgdata = profile_pic;
                                    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                                    fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                                } catch (e) {
                                    next(e);
                                }
                                console.log('ggggggggggggggggggggggg');
            
                                models.clients.update({
                                    profile_picture: client_profile_pic,
                                },{where:{client_id:client.client_id}})
                            }
                            
                            res.status(200).json({ status:200, success: true, message: "Client successfully created."}); 
                            
                        });
                    }else{
                        res.status(200).json({ status:200, success: false,message: "Client creation failed!"});
                    }
            
                });

            }else{
                res.status(200).json({ status:200, success: false, message: "Email or mobile already exists!"});
            }
        }else{
            res.status(200).json({ status:200, success: false, message: "All fields are required!"});
        }
        
    }    
}


/************************* client profile update ends *************************/


/************************* client password update start *************************/

exports.client_password_update = async function(req, res, next) {
    console.log(req.body.data);

    var client_id = req.body.data.client_id;
    var current_password = req.body.data.current_password;
    var new_password = req.body.data.new_password;
   
    if(client_id && client_id !=''){
        console.log('11111111111111111111');

        var client_details = await sequelize.query("SELECT clients.* from clients where clients.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

        if(client_details.length > 0){
            console.log('22222222222222222222222');

            if(current_password && current_password !='' && new_password && new_password !='' ){
                console.log('555555555555555555');

                if(!bcrypt.compareSync(current_password, client_details[0].password)){
                    res.status(200).send({ status:200, success: false, message: "Curremt password does not match!" });
                }else{
                    var hash = bcrypt.hashSync(new_password);
                    models.clients.update({ 

                        password : hash,
    
                    },{where:{client_id:client_id}}).then(function(clients) {
                        res.status(200).send({ status:200, success: true, message: "Client password successfully updated" });
                    })
                }

            }else{
                res.status(200).json({ status:200, success: false, message: "All fields are required!"});
            }

        }else{
            res.status(200).json({ status:200, success: false, message: "Client not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    }    
}


/************************* client password update ends *************************/



/************************* client delete start *************************/

exports.client_delete = function(req, res, next) {
    console.log(req.body.data);
    var client_id = req.body.data.client_id;

    if(client_id && client_id !='' ){

        models.clients.destroy({ 
            where:{client_id:client_id}
        }).then(function(value) {

            if(value){
                models.client_basic_info.destroy({ 
                    where:{client_id:client_id}
                }).then(function(client_basic_info) {
                    res.status(200).send({ status:200, success: true, message: "Client successfully deleted" });
                });
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Client not successfully delete" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    } 
}

/************************* client delete ends *************************/



/************************* candidate job opening status dropdown start *************************/

exports.candidate_job_opening_status = async function(req, res, next) {
    var candidate_job_opening_status = await sequelize.query("SELECT * FROM `candidate_job_opening_status` where status = 'active'",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(candidate_job_opening_status.length > 0) {
        res.status(200).send({ status:200, success: true, candidate_job_opening_status:candidate_job_opening_status });  
    } else {
        res.status(200).send({ status:200, success: false, message: "No candidate job opening status found!" }); 
    }
}

/************************* candidate job opening status dropdown ends *************************/



/************************* candidate job opening sub status dropdown start *************************/

exports.candidate_job_opening_sub_status = async function(req, res, next) {

    var candidate_job_opening_status_id = req.body.data.candidate_job_opening_status_id;

    if(candidate_job_opening_status_id && candidate_job_opening_status_id !='' ){

        var candidate_job_opening_sub_status = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where status = 'active' and candidate_job_opening_status_id = "+candidate_job_opening_status_id,{ type: Sequelize.QueryTypes.SELECT });
                                        
        if(candidate_job_opening_sub_status.length > 0) {
            res.status(200).send({ status:200, success: true, candidate_job_opening_sub_status:candidate_job_opening_sub_status });  
        } else {
            res.status(200).send({ status:200, success: false, message: "No candidate job opening sub status found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate job opening status id is required!"});
    } 
}

/************************* candidate job opening sub status dropdown ends *************************/


/************************* client dashboard start *************************/

exports.client_dashboard = async function(req, res, next) {

    var client_id = req.body.data.client_id;

    if(client_id && client_id !='' ){

        var client_company_count =await sequelize.query("SELECT COUNT(*) as total_company from companies where client_id = "+client_id ,{ type: Sequelize.QueryTypes.SELECT });
        var client_job_count =await sequelize.query("SELECT COUNT(*) as total_job from jobs where client_id = "+client_id ,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_count =await sequelize.query("SELECT COUNT(DISTINCT(a.candidate_id)) as total_candidates from assigned_job_to_candidates as a left join jobs as b on b.job_id = a.job_id where b.client_id = "+client_id ,{ type: Sequelize.QueryTypes.SELECT });
         
        res.status(200).send({ status:200, success: true, client_company_count:client_company_count[0].total_company, client_job_count: client_job_count[0].total_job, candidate_count:candidate_count[0].total_candidates });  
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    } 
}

/************************* client dashboard ends *************************/



/************************* mailgun send mail checking start *************************/
exports.mail_check = async function(req, res, next){

    console.log(req.body.data);

    /////////////////////////////////////// donemailer start ////////////////////////////////////
    // var transporter = nodemailer.createTransport({
    //     service: 'mail.redfordrecruiters.com',
    //     // sendmail:true,
    //     host: "mail.redfordrecruiters.com",
    //     // service: 'smtp.ethereal.email',
    //     port: 465,
    //     secure: true,
    //     auth: {
    //       user: 'test@redfordrecruiters.com',
    //       pass: '123456'
    //     }
    //   });

    //   console.log('1111111111111111111111111');
    //   console.log(transporter);
      
    //   var mailOptions = {
    //     from: 'test@redfordrecruiters.com',
    //     to: 'tanbir.bluehorse@gmail.com',
    //     subject: 'Sending Email using Node.js',
    //     text: 'That was easy!'
    //   };

    //   console.log('66666666666666666666');
    //   console.log(mailOptions);
      
    //   transporter.sendMail(mailOptions, function(error, info){
    //     console.log('222222222222222222222');
    //     if (error) {
    //         console.log('333333333333333333');
    //       console.log(error);
    //       res.status(200).json({ status:200, success: false, message: "send failed", error: error});
    //     } else {
    //         console.log('4444444444444444444444');
    //       console.log('Email sent: ' + info.response);
    //       res.status(200).json({ status:200, success: true, message: "send", info: info.response});
    //     }
    //   });
    // console.log('55555555555555555555');
/////////////////////////////////////// nodemailer end ////////////////////////////////////



// /////////////////////////////////////// dynamic mailgun start ////////////////////////////////////
var email_content =await sequelize.query("SELECT * FROM `email_template` WHERE `email_template_id` = 4" ,{ type: Sequelize.QueryTypes.SELECT });
var random_otp = Math.floor(1000 + Math.random() * 9000); 
var replace = email_content[0].content.replace(/{{secret_code}}/g, ''+random_otp+'');

var email = req.body.data.email;

    // return new Promise((resolve, reject) => {

        var data = {
            from: 'Redford User <me@samples.mailgun.org>',
            // to: ['tanbir.bluehorse@gmail.com','mitrajit.samanta@gmail.com'],
            to: [email],
            subject: 'Redford Recruiters : '+email_content[0].subject+'',
            html: '<!DOCTYPE html>'+
            ''+replace+''
        };
        console.log(data);
        mailgun.messages().send(data, function (error, body) {
            console.log(body);
            if (error) {
                // return reject(res.status(200).send({ status:200, success: false, message: "unsuccess", error:error}));
                res.status(200).send({ status:200, success: false, message: "unsuccess", error:error});         
            }
            // return resolve(res.status(200).send({ status:200, success: true, message: "success", body:body}));
            res.status(200).send({ status:200, success: true, message: "success", body:body}); 
        });
    // });

// /////////////////////////////// multiple email send start /////////////////////////////////////
//     return new Promise((resolve, reject) => {

//         var data1 = {
//             from: 'Redford User 111 <me@samples.mailgun.org>',
//             // to: ['tanbir.bluehorse@gmail.com','mitrajit.samanta@gmail.com'],
//             to: ['samiran.bluehorse@gmail.com'],
//             // to: [email],
//             subject: 'Redford Recruiters 111 : '+email_content[0].subject+'',
//             html: '<!DOCTYPE html>'+
//             ''+replace+''
//         };
//         console.log(data1);
//         mailgun.messages().send(data1, function (error, body) {
//             console.log(body);
//             // if (error) {
//             //     return reject(res.status(200).send({ status:200, success: false, message1: "unsuccess", error:error}));
//             // }
//             // return resolve(res.status(200).send({ status:200, success: true, message1: "success", body:body}));
//         });

//         var data = {
//             from: 'Redford User <me@samples.mailgun.org>',
//             // to: ['tanbir.bluehorse@gmail.com','mitrajit.samanta@gmail.com'],
//             to: [email],
//             subject: 'Redford Recruiters : '+email_content[0].subject+'',
//             html: '<!DOCTYPE html>'+
//             ''+replace+''
//         };
//         console.log(data);
//         mailgun.messages().send(data, function (error, body) {
//             console.log(body);
//             if (error) {
//                 return reject(res.status(200).send({ status:200, success: false, message: "unsuccess", error:error}));
//             }
//             return resolve(res.status(200).send({ status:200, success: true, message: "success", body:body}));
//         });
//     });
//     /////////////////////////////// multiple email send end /////////////////////////////////////

    // return new Promise((resolve, reject) => {
    //     var data = {
    //         from: 'Redford User <me@samples.mailgun.org>',
    //         // to: ['tanbir.bluehorse@gmail.com','mitrajit.samanta@gmail.com'],
    //         to: [email],
    //         subject: 'Redford Recruiters : '+email_content[0].subject+'',
    //         html: '<!DOCTYPE html>'+
    //         ''+replace+''
    //     };
    //     console.log(data);
    //     mailgun.messages().send(data, function (error, body) {
    //         console.log(body);
    //         if (error) {
    //             return reject(res.status(200).send({ status:200, success: false, message: "unsuccess", error:error}));
    //         }
    //         return resolve(res.status(200).send({ status:200, success: true, message: "success", body:body}));
    //     });
    // });

//     /////////////////////////////////////// dynamic mailgun end ////////////////////////////////////


// // /////////////////////////////////////// mailgun start ////////////////////////////////////

// var email = req.body.data.email;
//     return new Promise((resolve, reject) => {
//         var data = {
//             from: 'Redford User <me@samples.mailgun.org>',
//             // to: ['tanbir.bluehorse@gmail.com','mitrajit.samanta@gmail.com'],
//             to: [email],
//             subject: 'Testing',
//             html: '<!DOCTYPE html>'+
//             '<html>'+    
//             '<head>'+
//             '</head>'+    
//             '<body>'+
//                 '<div>'+
//                     '<p>Hello Sir/Madam,</p>'+
//                     '<p>Thank you for visiting our site </p>'+  
//                     '<p>For any kind of query reach us at: <br />'+
//                     'Mail: redford@gmail.com<br />'+
//                     'Phone: +91 9876543210<br />'+
//                     'Have a nice day.</p>'+
//                     '<p>Thanks.</p>'+
//                     '<p>Regards,<br />'+
//                     'redford recruiters team<br />'+
//                     'UAE</p>'+								
//                 '</div>'+       
//             '</body>'+    
//             '</html>' 
//         };
//         console.log(data);
//         mailgun.messages().send(data, function (error, body) {
//             console.log(body);
//             if (error) {
//                 return reject(res.status(200).send({ status:200, success: false, message: "unsuccess", error:error}));
//             }
//             return resolve(res.status(200).send({ status:200, success: true, message: "success", body:body}));
//         });
//     });

// //     /////////////////////////////////////// mailgun end ////////////////////////////////////

// var email_content =await sequelize.query("SELECT * FROM `email_template` WHERE `email_template_id` = 4" ,{ type: Sequelize.QueryTypes.SELECT });
// var random_otp = Math.floor(1000 + Math.random() * 9000); 
// var replace = email_content[0].content.replace(/{{secret_code}}/g, ''+random_otp+'');

// var email = req.body.data.email;
//     return new Promise((resolve, reject) => {
//         var data = {
//             from: 'Redford User <xxxxxxxxxxx@samples.com>',
//             // to: ['tanbir.bluehorse@gmail.com'],
//             to: [email],
//             // subject: 'Redford Recruiters : '+email_content[0].subject+'',
//             // html: '<!DOCTYPE html>'+
//             // ''+replace+'' 

//             subject: 'Testing',
//             html: '<!DOCTYPE html>'+
//             '<html>'+    
//             '<head>'+
//             '</head>'+    
//             '<body>'+
//                 '<div>'+
//                     '<p>Hello Sir/Madam,</p>'+
//                     '<p>Thank you for visiting our site </p>'+  
//                     '<p>For any kind of query reach us at: <br />'+
//                     'Mail: redford@gmail.com<br />'+
//                     'Phone: +91 9876543210<br />'+
//                     'Have a nice day.</p>'+
//                     '<p>Thanks.</p>'+
//                     '<p>Regards,<br />'+
//                     'redford recruiters team<br />'+
//                     'UAE</p>'+								
//                 '</div>'+       
//             '</body>'+    
//             '</html>'
//         };
//         console.log(data);
//         mailgun.messages().send(data, function (error, body) {
//             console.log(body);
//             if (error) {
//                 return reject(res.status(200).send({ status:200, success: false, message: "unsuccess", error:error}));
//             }
//             return resolve(res.status(200).send({ status:200, success: true, message: "success", body:body}));
//         });
//     });

// res.status(200).send({ status:200, success: true, email_content:email_content[0].content, replace: replace});  

}

/************************* mailgun send mail checking ends *************************/

