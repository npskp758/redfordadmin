var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);



/************************* Jobs List Api Start **************************************/
exports.jobsList = async function(req, res, next) {
    console.log(req.body.data);
    var job_type = req.body.data.job_type;
    var candadite_id = (typeof req.body.data.candadite_id !== 'undefined' ? req.body.data.candadite_id : '');

    if(candadite_id && candadite_id !=''){
        console.log('1111111111111111111111111111');
        var candadite_category = await models.candidate_looking_for.findOne({where: {candidate_id:candadite_id}, attributes: ['candidate_looking_for_id', 'candidate_id', 'category']});
        if(candadite_category){
            console.log('9999999999999999999999999');
            // var filterJobs = await models.jobs.findAll({where: {job_category:candadite_category.category,job_type:job_type},attributes: ['job_id', 'job_title', 'job_type', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
            var filterJobs = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.job_type, jobs.person_salary, jobs.currency, jobs.person_work_exp, jobs.basic_info_cuntry, jobs.basic_info_state, jobs.basic_info_city, companies.company_name from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id where jobs.job_category ="+candadite_category.category+" AND jobs.job_type ='"+job_type+"' ORDER BY jobs.job_id DESC Limit 4",{ type: Sequelize.QueryTypes.SELECT });
        }else{
            console.log('88888888888888888888888888');
            // var filterJobs = await models.jobs.findAll({where: {job_type:job_type},attributes: ['job_id', 'job_title', 'job_type', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
            var filterJobs = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.job_type, jobs.person_salary, jobs.currency, jobs.person_work_exp, jobs.basic_info_cuntry, jobs.basic_info_state, jobs.basic_info_city, companies.company_name from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id where jobs.job_type ='"+job_type+"' ORDER BY jobs.job_id DESC Limit 4",{ type: Sequelize.QueryTypes.SELECT });
        }

        if(filterJobs){
            res.status(200).send({ status:200,success: "true", details:filterJobs });
        }else{
            res.status(200).send({ status:200,success: "false", details:[] });
        }
        
    }else{
        console.log('2222222222222222222222');
        // var filterJobs = await models.jobs.findAll({where: {job_type:job_type},attributes: ['job_id', 'job_title', 'job_type', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
        var filterJobs = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.job_type, jobs.person_salary, jobs.currency, jobs.person_work_exp, jobs.basic_info_cuntry, jobs.basic_info_state, jobs.basic_info_city, companies.company_name from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id where jobs.job_type ='"+job_type+"' ORDER BY jobs.job_id DESC Limit 4",{ type: Sequelize.QueryTypes.SELECT });

        //var allJobs = await models.jobs.findAll({attributes: ['job_id', 'job_title', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
        if(filterJobs){
            res.status(200).send({ status:200,success: "true", details:filterJobs });
        }else{
            res.status(200).send({ status:200,success: "false", details:[] });
        }

    }

}
/************************* Jobs List Api Ends **************************************/


// /************************* Jobs List Api Start **************************************/
// exports.jobsList = async function(req, res, next) {
//     console.log(req.body.data);
//     var job_type = req.body.data.job_type;
//     var candadite_id = (typeof req.body.data.candadite_id !== 'undefined' ? req.body.data.candadite_id : '');

//     if(job_type && job_type !=''){

//         if(candadite_id && candadite_id !=''){
//             console.log('1111111111111111111111111111');
//             var candadite_category = await models.candidate_looking_for.findOne({where: {candidate_id:candadite_id}, attributes: ['candidate_looking_for_id', 'candidate_id', 'category']});
//             if(candadite_category){
//                 console.log('9999999999999999999999999');
//                 // var filterJobs = await models.jobs.findAll({where: {job_category:candadite_category.category,job_type:job_type},attributes: ['job_id', 'job_title', 'job_type', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
//                 var filterJobs = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.person_work_exp, jobs.person_salary, companies.company_name, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id where jobs.job_category ="+candadite_category.category+" and jobs.job_type ='"+job_type+"' and jobs.status = 'publish' order by jobs.job_id DESC limit 5",{ type: Sequelize.QueryTypes.SELECT });
//             }else{
//                 console.log('88888888888888888888888888');
//                 // var filterJobs = await models.jobs.findAll({where: {job_type:job_type},attributes: ['job_id', 'job_title', 'job_type', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
//                 var filterJobs = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.person_work_exp, jobs.person_salary, companies.company_name, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id where jobs.job_type ='"+job_type+"' and jobs.status = 'publish' order by jobs.job_id DESC limit 5",{ type: Sequelize.QueryTypes.SELECT });
//             }

//             if(filterJobs){
//                 res.status(200).send({ status:200,success: "true", details:filterJobs });
//             }else{
//                 res.status(200).send({ status:200,success: "false" });
//             }
            
//         }else{
//             console.log('2222222222222222222222');
//             // var filterJobs = await models.jobs.findAll({where: {job_type:job_type},attributes: ['job_id', 'job_title', 'job_type', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
//             var filterJobs = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.person_work_exp, jobs.person_salary, companies.company_name, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id where jobs.job_type ='"+job_type+"' and jobs.status = 'publish' order by jobs.job_id DESC limit 5",{ type: Sequelize.QueryTypes.SELECT });

//             //var allJobs = await models.jobs.findAll({attributes: ['job_id', 'job_title', 'person_work_exp', 'client_name', 'person_salary', 'client_country', 'client_state', 'client_city', 'client_address'],order:[['job_id','DESC']],limit:4});
//             if(filterJobs){
//                 res.status(200).send({ status:200,success: "true", details:filterJobs });
//             }else{
//                 res.status(200).send({ status:200,success: "false" });
//             }

//         }
//     }else{
//         res.status(200).send({ status:200,success: "false", message: "Job type is required"});
//     }

// }
// /************************* Jobs List Api Ends **************************************/


/************************* home page all count start *******************************/

exports.home_all_type_count_asnc = async function(req, res, next) {

    var company_count =await sequelize.query("SELECT COUNT(*) as clients_count from clients" ,{ type: Sequelize.QueryTypes.SELECT });
    var resume_count =await sequelize.query("SELECT COUNT(*) as clients_count from clients",{ type: Sequelize.QueryTypes.SELECT });
    var job_count =await sequelize.query("SELECT COUNT(*) as job_count from jobs",{ type: Sequelize.QueryTypes.SELECT });

    res.status(200).send({ status:200, success: true, job_count:job_count[0].job_count, member_count:resume_count[0].candidates_count, resume_count:resume_count[0].candidates_count, company_count:company_count[0].clients_count  });
}

/************************* home page all count ends *******************************/


/************************* home page all count start *******************************/

exports.home_all_type_count = function(req, res, next) {

    var company_count = sequelize.query("SELECT COUNT(*) as clients_count from clients ",{ type: Sequelize.QueryTypes.SELECT })
    company_count.then(function (company_count) {
        var resume_count = sequelize.query("SELECT COUNT(*) as candidates_count from candidates ",{ type: Sequelize.QueryTypes.SELECT })
        resume_count.then(function (resume_count) {
            var job_count = sequelize.query("SELECT COUNT(*) as job_count from jobs ",{ type: Sequelize.QueryTypes.SELECT })
            job_count.then(function (job_count) {

                res.status(200).send({ status:200, success: true, job_count:job_count[0].job_count, member_count:resume_count[0].candidates_count, resume_count:resume_count[0].candidates_count, company_count:company_count[0].clients_count  });

            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });

}

/************************* home page all count ends *******************************/


/************************* home page job search start *******************************/ 

exports.home_page_job_search = async function(req, res, next) {

    console.log(req.body.data);
    var keyword= req.body.data.keyword ? req.body.data.keyword : '';
    var town= req.body.data.town ? req.body.data.town : '';
    var keystr = '';
    var replacekey = '';
    var resultArray = [];

    if(!town && !keyword){

        console.log('88888888888888888888888888');
        res.status(200).send({ status:200, success: false, message: "No job keyword and town found" });

    }else if(!keyword){
        console.log('1111111111111111111');

        // var job_search =await sequelize.query("select jobs.* FROM `jobs` where jobs.status='publish' and jobs.basic_info_city LIKE '%"+town+"%'",{ type: Sequelize.QueryTypes.SELECT });
        var job_search =await sequelize.query("select jobs.*, companies.company_name, companies.category, companies.logo, categories.title as category_title FROM `jobs` LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.status='publish' and jobs.basic_info_city LIKE '%"+town+"%'",{ type: Sequelize.QueryTypes.SELECT });

        var i = 0;
        job_search.forEach(function(element,index){ 

            if(element.logo !='' && element.logo!= null){
                var company_logo = req.app.locals.baseurl+element.logo;
            }else{
                var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
            }

            resultArray.push({
                "job_id": element.job_id,
                "job_title": element.job_title,
                "job_type": element.job_type,
                "basic_info_cuntry": element.basic_info_cuntry,
                "basic_info_state": element.basic_info_state,
                "basic_info_city": element.basic_info_city,
                "job_category": element.job_category,
                "person_salary": element.person_salary,
                "currency": element.currency,
                "client_id": element.client_id,
                "job_description": element.job_description,
                "skill_set": element.skill_set,
                "createdAt": element.createdAt,
                "company_id": element.company_id,
                "company_name": element.company_name,
                "company_logo": company_logo
            });    
            i++;             
        });

        res.status(200).send({ status:200, success: true, job_search: resultArray });
        // res.status(200).send({ status:200, success: false, message: "No job keyword found" });

    }else if(!town){
        console.log('2222222222222222222');

        var keyarr = req.body.data.keyword.split(",");

        //var keyy = key.join().replace(/,/g, "%' or jobs.skill_set LIKE '%");
        // var fruit1 = .trim()

        // .replace(/ /g, "")
        if(keyarr.length == 1){
            console.log('33333333333333333');

            keystr = "jobs.skill_set LIKE '%"+keyarr[0].replace(/ /g, "")+"%'";
            jtstr  = "jobs.job_title LIKE '%"+keyarr[0]+"%'";

        }else{
            console.log('4444444444444444444');
            var i=0; 
            keyarr.forEach(function(skl){

                replacekey += "jobs.skill_set LIKE '%"+keyarr[i].replace(/ /g, "")+"%' or ";
                replaceJTkey  = "jobs.job_title LIKE '%"+keyarr[i]+"%' or ";

                i++;
            }, this);

            var keystr = replacekey.slice(0,-4);
            jtstr  = replaceJTkey.slice(0,-4);
        }

        

        // var job_search =await sequelize.query("select jobs.* FROM `jobs` where jobs.status='publish' and (jobs.skill_set LIKE '%"+keyword+"%')",{ type: Sequelize.QueryTypes.SELECT });

        // var job_search =await sequelize.query("select jobs.* FROM `jobs` where jobs.status='publish' and ("+keystr+")",{ type: Sequelize.QueryTypes.SELECT });
        var job_search =await sequelize.query("select jobs.*, companies.company_name, companies.category, companies.logo, categories.title as category_title FROM `jobs` LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.status='publish' and (("+keystr+") or ("+jtstr+"))",{ type: Sequelize.QueryTypes.SELECT });


        var i = 0;
        job_search.forEach(function(element,index){ 

            if(element.logo !='' && element.logo!= null){
                var company_logo = req.app.locals.baseurl+element.logo;
            }else{
                var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
            }

            resultArray.push({
                "job_id": element.job_id,
                "job_title": element.job_title,
                "job_type": element.job_type,
                "basic_info_cuntry": element.basic_info_cuntry,
                "basic_info_state": element.basic_info_state,
                "basic_info_city": element.basic_info_city,
                "job_category": element.job_category,
                "person_salary": element.person_salary,
                "currency": element.currency,
                "client_id": element.client_id,
                "job_description": element.job_description,
                "skill_set": element.skill_set,
                "createdAt": element.createdAt,
                "company_id": element.company_id,
                "company_name": element.company_name,
                "company_logo": company_logo
            });    
            i++;             
        });

        res.status(200).send({ status:200, success: true, job_search: resultArray });
        
        // }else if(!town && !keyword){
        
    }else if(town !='' && keyword != ''){
        console.log('55555555555555555555555');

        var keyarr = req.body.data.keyword.split(",");
        
        if(keyarr.length == 1){
            console.log('6666666666666666666666');

            keystr = "jobs.skill_set LIKE '%"+keyarr[0].replace(/ /g, "")+"%'";
            jtstr  = "jobs.job_title LIKE '%"+keyarr[0]+"%'";

        }else{
            console.log('777777777777777777777');
            var i=0; 
            keyarr.forEach(function(skl){

                replacekey += "jobs.skill_set LIKE '%"+keyarr[i].replace(/ /g, "")+"%' or ";
                replaceJTkey  = "jobs.job_title LIKE '%"+keyarr[i]+"%' or ";

                i++;
            }, this);

            keystr = replacekey.slice(0,-4);
            jtstr  = replaceJTkey.slice(0,-4);
        }

        // var job_search =await sequelize.query("select jobs.* FROM `jobs` where jobs.status='publish' and jobs.basic_info_city LIKE '%"+town+"%' and ("+keystr+")",{ type: Sequelize.QueryTypes.SELECT });
        var job_search =await sequelize.query("select jobs.*, companies.company_name, companies.category, companies.logo, categories.title as category_title FROM `jobs` LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.status='publish' and jobs.basic_info_city LIKE '%"+town+"%' and (("+keystr+") or ("+jtstr+")) ",{ type: Sequelize.QueryTypes.SELECT });

        var i = 0;
        job_search.forEach(function(element,index){ 

            if(element.logo !='' && element.logo!= null){
                var company_logo = req.app.locals.baseurl+element.logo;
            }else{
                var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
            }

            resultArray.push({
                "job_id": element.job_id,
                "job_title": element.job_title,
                "job_type": element.job_type,
                "basic_info_cuntry": element.basic_info_cuntry,
                "basic_info_state": element.basic_info_state,
                "basic_info_city": element.basic_info_city,
                "job_category": element.job_category,
                "person_salary": element.person_salary,
                "currency": element.currency,
                "client_id": element.client_id,
                "job_description": element.job_description,
                "skill_set": element.skill_set,
                "createdAt": element.createdAt,
                "company_id": element.company_id,
                "company_name": element.company_name,
                "company_logo": company_logo
            });    
            i++;             
        });

        res.status(200).send({ status:200, success: true, job_search: resultArray });

    }    

}

/************************* home page job search ends *******************************/



/************************* header credential details start *******************************/

exports.header_credential_details = async function(req, res, next) {

    var user_id = req.body.data.user_id;
    var user_type = req.body.data.user_type;
    var resultArray = [];

    if(user_id && user_id !='' && user_type && user_type !=''){

        if(user_type =='candidate'){

            var candidate_profile_info = await sequelize.query("SELECT candidates.*, candidate_looking_for.category from candidates LEFT JOIN candidate_looking_for ON candidates.candidate_id = candidate_looking_for.candidate_id where candidates.candidate_id ="+user_id,{ type: Sequelize.QueryTypes.SELECT });

            if(candidate_profile_info.length > 0){

                var i = 0;
                var candidate_first_name = '';
                var candidate_last_name = '';
    
                candidate_profile_info.forEach(function(element,index){ 
    
                    if(element.first_name !='' && element.first_name!= null){
                        candidate_first_name = element.first_name;
                        candidate_last_name = element.last_name;
                    }else if(element.name !='' && element.name!= null){
                        candidate_name = element.name.split(' ');
                        candidate_first_name = candidate_name[0];
                        for( var j =1; j<=candidate_name.length-1; j++){
                            candidate_last_name += candidate_name[j]+ " ";
                        }
                        candidate_last_name = candidate_last_name.substring(0, candidate_last_name.length-1);
                    }
    
                    if(element.profile_picture !='' && element.profile_picture!= null){
                        var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
                    }else if(element.provider_image !='' && element.provider_image!= null){
                        var candidate_profile_picture = element.provider_image;
                    }else{
                        var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                    }
    
                    resultArray.push({
                        "user_id": element.candidate_id,
                        "profile_picture": candidate_profile_picture,
                        "first_name": candidate_first_name,
                        "last_name": candidate_last_name,
                        "category": element.category
                    });    
                    i++;             
                });
                res.status(200).send({ status:200, success: true, user_details:resultArray[0] });
            }else{
                res.status(200).json({ status:200, success: false, message: "Candidate details not found"});
            }

        }else if(user_type =='client'){

            var client_profile_info = await sequelize.query("SELECT clients.*, client_basic_info.* from clients LEFT JOIN client_basic_info ON clients.client_id = client_basic_info.client_id where clients.client_id ="+user_id,{ type: Sequelize.QueryTypes.SELECT });

            if(client_profile_info.length > 0){

                var i = 0;
                var client_first_name = '';
                var client_last_name = '';
    
                client_profile_info.forEach(function(element,index){ 
    
                    if(element.first_name !='' && element.first_name!= null){
                        client_first_name = element.first_name;
                        client_last_name = element.last_name;
                    }else if(element.name !='' && element.name!= null){
                        client_name = element.name.split(' ');
                        client_first_name = client_name[0];
                        for( var j =1; j<=client_name.length-1; j++){
                            client_last_name += client_name[j]+ " ";
                        }
                        client_last_name = client_last_name.substring(0, client_last_name.length-1);
                    }
    
                    if(element.profile_picture !='' && element.profile_picture!= null){
                        var client_profile_picture = req.app.locals.baseurl+element.profile_picture;
                    }else if(element.provider_image !='' && element.provider_image!= null){
                        var client_profile_picture = element.provider_image;
                    }else{
                        var client_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                    }
    
                    resultArray.push({
                        "user_id": element.client_id,
                        "profile_picture": client_profile_picture,
                        "first_name": client_first_name,
                        "last_name": client_last_name
                    });    
                    i++;             
                });
                res.status(200).send({ status:200, success: true, user_details:resultArray[0] });
            }else{
                res.status(200).json({ status:200, success: false, message: "Client details not found"});
            }
        }else if(user_type =='admin'){

            var admin_profile_info = await sequelize.query("SELECT admins.* from admins where admins.admin_id ="+user_id,{ type: Sequelize.QueryTypes.SELECT });

            if(admin_profile_info.length > 0){

                var i = 0;
                admin_profile_info.forEach(function(element,index){ 
    
                    if(element.profile_picture !='' && element.profile_picture!= null){
                        var admin_profile_picture = req.app.locals.baseurl+element.profile_picture;
                    }else{
                        var admin_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                    }
    
                    resultArray.push({
                        "user_id": element.admin_id,
                        "profile_picture": admin_profile_picture,
                        "name": element.name
                    });    
                    i++;             
                });
                res.status(200).send({ status:200, success: true, user_details:resultArray[0] });
            }else{
                res.status(200).json({ status:200, success: false, message: "Admin details not found"});
            }
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "User id and type are required"});
    }    
}


/************************* header credential details ends *******************************/



/************************* testimonial upload start *******************************/

exports.testimonial_upload = async function(req, res, next) {
    console.log(req.body.data);

    var name = req.body.data.name;
    var content = req.body.data.content;
    var company_name = req.body.data.company_name;
    var image = req.body.data.image;
    var extension = req.body.data.extension;
    var status = req.body.data.status;

    var testimonial_id = req.body.data.testimonial_id;

    
    if(name && name !='' && content && content !='' && company_name && company_name !='' ){
        console.log('11111111111111111111111');
        if(!testimonial_id){
            console.log('22222222222222222222222');
            models.testimonial.create({ 

                user_name: name,
                content: content,
                company_name: company_name,
                status: status,

            }).then(function(testimonial) {

                if(testimonial){

                    var dir = './public/contents/testimonials/'+testimonial.testimonial_id; 
                    console.log(dir);
                    if (!fs.existsSync(dir)){
                        fs.mkdirSync(dir);                  
                    }

                    if(image && image !='' && extension && extension !=''){

                        console.log('333333333333333333');
                        var testimonial_user_image = 'contents/testimonials/'+testimonial.testimonial_id+'/user_image.'+extension;
                        try {
                            const path = './public/'+testimonial_user_image;
                            const imgdata = image;
                            const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                            fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                        } catch (e) {
                            next(e);
                        }
                        console.log('444444444444444444444');
    
                        models.testimonial.update({
                            user_image: testimonial_user_image,
                        },{where:{testimonial_id:testimonial.testimonial_id}})
                    }

                    res.status(200).send({ status:200, success: true, message: "Testimonial successfully added" });

                }else{
                    res.status(200).json({ status:200, success: false,message: "Testimonial creation failed"});
                }
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.testimonial.update({ 

                user_name: name,
                content: content,
                company_name: company_name,
                status: status,

            },{where:{testimonial_id:testimonial_id}}).then(function(testimonial) {

                var dir = './public/contents/testimonials/'+testimonial_id; 
                console.log(dir);
                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);                  
                }

                if(image && image !='' && extension && extension !=''){

                    console.log('333333333333333333');
                    var testimonial_user_image = 'contents/testimonials/'+testimonial_id+'/user_image.'+extension;
                    try {
                        const path = './public/'+testimonial_user_image;
                        const imgdata = image;
                        const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');                
                        fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
                    } catch (e) {
                        next(e);
                    }
                    console.log('444444444444444444444');

                    models.testimonial.update({
                        user_image: testimonial_user_image,
                    },{where:{testimonial_id:testimonial_id}})
                }

                res.status(200).send({ status:200, success: true, message: "Testimonial successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required"});
    }
} 

/************************* testimonial upload ends *******************************/



/************************* testimonial list start *******************************/
exports.testimonial_list = async function(req, res, next) {

    var resultArray = [];

    var testimonial_list = await sequelize.query("SELECT * FROM `testimonial` where 1",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(testimonial_list.length > 0) {

        var i = 0;
        testimonial_list.forEach(function(element,index){ 

            if(element.user_image !='' && element.user_image!= null){
                var user_image = req.app.locals.baseurl+element.user_image;
            }else{
                var user_image = req.app.locals.baseurl+'contents/no_image_redford.jpg';
            }

            resultArray.push({
                "testimonial_id":element.testimonial_id,
                "user_name":element.user_name,
                "content":element.content,
                "user_image":user_image,
                "company_name":element.company_name,
                "status":element.status
            });    
            i++;             
        }); 

        res.status(200).send({ status:200,success: true, testimonial_list:resultArray });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No testimonial found" }); 
    }
}
/************************* testimonial list ends *******************************/



/************************* testimonial details start *******************************/
exports.testimonial_details = async function(req, res, next) {
    console.log(req.body.data);
    var testimonial_id = req.body.data.testimonial_id;
    var resultArray = [];

    if(testimonial_id && testimonial_id !='' ){

        var testimonial_details = await sequelize.query("SELECT * FROM `testimonial` where testimonial_id = "+testimonial_id,{ type: Sequelize.QueryTypes.SELECT });
                                        
        if(testimonial_details.length > 0) {

            var i = 0;
            testimonial_details.forEach(function(element,index){ 

                if(element.user_image !='' && element.user_image!= null){
                    var user_image = req.app.locals.baseurl+element.user_image;
                }else{
                    var user_image = req.app.locals.baseurl+'contents/no_image_redford.jpg';
                }

                resultArray.push({
                    "testimonial_id":element.testimonial_id,
                    "user_name":element.user_name,
                    "content":element.content,
                    "user_image":user_image,
                    "company_name":element.company_name,
                    "status":element.status
                });    
                i++;             
            }); 

            res.status(200).send({ status:200,success: true, testimonial_details:resultArray[0] });  
        } else {
            res.status(200).send({ status:200,success: false, message: "No testimonial found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Testimonial id is required"});
    }
}
/************************* testimonial details ends *******************************/



/************************* testimonial delete start *******************************/

exports.testimonial_delete = function(req, res, next) {
    console.log(req.body.data);
    var testimonial_id = req.body.data.testimonial_id;

    if(testimonial_id && testimonial_id !='' ){

        models.testimonial.destroy({ 
            where:{testimonial_id:testimonial_id}
        }).then(function(value) {

            if(value){
                
                res.status(200).send({ status:200, success: true, message: "Testimonial successfully deleted" });
               
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Testimonial not successfully deleted" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Testimonial id is required"});
    } 
}

/************************* testimonial delete ends *******************************/


/************************* home testimonial list start *******************************/
exports.home_testimonial = async function(req, res, next) {

    var resultArray = [];

    // var testimonial_list = await sequelize.query("SELECT * FROM `testimonial` where status = 'active' ORDER BY RAND() LIMIT 4",{ type: Sequelize.QueryTypes.SELECT });
    var testimonial_list = await sequelize.query("SELECT * FROM `testimonial` where status = 'active'",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(testimonial_list.length > 0) {

        var i = 0;
        testimonial_list.forEach(function(element,index){ 

            if(element.user_image !='' && element.user_image!= null){
                var user_image = req.app.locals.baseurl+element.user_image;
            }else{
                var user_image = req.app.locals.baseurl+'contents/no_image_redford.jpg';
            }

            resultArray.push({
                "testimonial_id":element.testimonial_id,
                "user_name":element.user_name,
                "content":element.content,
                "user_image":user_image,
                "company_name":element.company_name,
                "status":element.status
            });    
            i++;             
        }); 

        res.status(200).send({ status:200,success: true, testimonial_list:resultArray });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No testimonial found" }); 
    }
}
/************************* home testimonial list ends *******************************/



/************************* cms page upload start *******************************/

exports.cms_upload = async function(req, res, next) {
    console.log(req.body.data);

    var title = req.body.data.title;
    var content = req.body.data.content;
    var status = req.body.data.status;

    var cms_id = req.body.data.cms_id;

    
    if(title && title !='' && content && content !='' && status && status !=''){
        console.log('11111111111111111111111');
        var slug = req.body.data.title.toString().toLowerCase().replace(/\s+/g, '-');
        if(!cms_id){
            console.log('22222222222222222222222');
            models.cms.create({ 

                title: title,
                content: content,
                slug: slug,
                status: status

            }).then(function(cms) {

                res.status(200).send({ status:200, success: true, message: "Cms apge successfully added" });

            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.cms.update({ 

                title: title,
                content: content,
                status: status

            },{where:{cms_id:cms_id}}).then(function(cms) {

                res.status(200).send({ status:200, success: true, message: "Cms page successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required"});
    }
} 

/************************* cms page upload ends *******************************/


/************************* cms list start *******************************/
exports.cms_list = async function(req, res, next) {

    var cms_list = await sequelize.query("SELECT * FROM `cms` where 1 ORDER BY cms_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(cms_list.length > 0) {
        res.status(200).send({ status:200,success: true, cms_list:cms_list });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No cms found" }); 
    }
}
/************************* cms list ends *******************************/


/************************* cms details start *******************************/
exports.cms_details = async function(req, res, next) {
    console.log(req.body.data);
    var cms_id = req.body.data.cms_id;

    if(cms_id && cms_id !='' ){

        var cms_details = await sequelize.query("SELECT * FROM `cms` where cms_id = "+cms_id,{ type: Sequelize.QueryTypes.SELECT });
                                        
        if(cms_details.length > 0) {

            res.status(200).send({ status:200,success: true, cms_details:cms_details[0] });  
        } else {
            res.status(200).send({ status:200,success: false, message: "No cms found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Cms id is required"});
    }
}
/************************* cms details ends *******************************/


/************************* cms delete start *******************************/

exports.cms_delete = function(req, res, next) {
    console.log(req.body.data);
    var cms_id = req.body.data.cms_id;

    if(cms_id && cms_id !='' ){

        models.cms.destroy({ 
            where:{cms_id:cms_id}
        }).then(function(value) {

            if(value){
                
                res.status(200).send({ status:200, success: true, message: "Cms successfully deleted" });
               
            }else{
                res.status(200).send({ status:200, success: false, message: "Something went wrong. Cms not successfully deleted" });
            }
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Cms id is required"});
    } 
}

/************************* cms delete ends *******************************/


/************************* home cms list start *******************************/
exports.home_cms_list = async function(req, res, next) {

    var cms_list = await sequelize.query("SELECT cms_id, title, slug FROM `cms` where status = 'active'",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(cms_list.length > 0) {
        res.status(200).send({ status:200, success: true, cms_list:cms_list });  
    } else {
        res.status(200).send({ status:200, success: false, message: "No cms found" }); 
    }
}
/************************* home cms list ends *******************************/



/************************* home cms details start *******************************/
exports.home_cms_details = async function(req, res, next) {
    console.log(req.body.data);
    var slug = req.body.data.slug;

    if(slug && slug !='' ){

        var cms_details = await sequelize.query("SELECT title, content FROM `cms` where slug = '"+slug+"'",{ type: Sequelize.QueryTypes.SELECT });
                                        
        if(cms_details.length > 0) {

            res.status(200).send({ status:200,success: true, cms_details:cms_details[0] });  
        } else {
            res.status(200).send({ status:200,success: false, message: "No cms found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Cms slug is required"});
    }
}
/************************* home cms details ends *******************************/



/************************* admin and client and candidate forgot password start *******************************/

exports.forgot_password =async function(req, res, next) {
    console.log(req.body.data);
    
    var email = req.body.data.email;
    var type = req.body.data.type;
    
    if( email && email !='' && type && type !='' ){
        console.log('1111111111111111111');
        console.log(type);
        if(type =='client'){
            console.log('222222222222222');
            var check_email =await sequelize.query("SELECT * FROM clients  WHERE username ='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            if(check_email.length > 0){
                console.log('3333333333333333');

                // var random_otp = Math.floor(1000 + Math.random() * 9000); 
                // var random_six_digit_otp = Math.floor(100000 + Math.random() * 900000)
                // var random_verification_code = randomString();

                var email_content =await sequelize.query("SELECT * FROM `email_template` WHERE `email_template_id` = 4" ,{ type: Sequelize.QueryTypes.SELECT });
                var random_otp = Math.floor(1000 + Math.random() * 9000); 
                var replace = email_content[0].content.replace(/{{secret_code}}/g, ''+random_otp+'');
                var final_data = replace.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
    
                models.forgot_password.create({
                    email : email,
                    type : type,
                    otp : random_otp,
                }).then(function(client){
                    console.log('3333333333333');
    
                    if(client){
                        console.log('44444444444444444444');
                        
                        return new Promise((resolve, reject) => {
                            console.log('5555555555555555');
                            ///////////////////////////// email send through template (dynamic ) start////////////////////////////
                            var data = {
                                from: 'Redford Team <admin@redfordrecruiters.com>',
                                to: [email],
                                subject: 'Redford Recruiters : '+email_content[0].subject+'',
                                html: '<!DOCTYPE html>'+
                                ''+final_data+''
                            };
                            ////////////////////// email send through template (dynamic ) end/////////////////////////////////

                            // var data = {
                            //     from: 'Redford Team <xxxxxxxx@redfordrecruiters.com>',
                            //     to: [email],
                            //     subject: 'Redford Recruiters : Reset password',
                            //     html: '<!DOCTYPE html>'+
                            //     '<html>'+    
                            //     '<head>'+
                            //     '</head>'+    
                            //     '<body>'+
                            //         '<div>'+
                            //             '<p>Hello Sir/Madam,</p>'+
                            //             '<p>Your reset password code is: </p>'+ 
                            //             '<h3> <b>'+random_otp+'</b></h3>'+  
                            //             '<p>For any kind of query reach us at: <br />'+
                            //             'xxxxxxxx@redfordrecruiters.com<br />'+
                            //             'Have a nice day.</p>'+
                            //         '</div>'+       
                            //     '</body>'+    
                            //     '</html>' 
                            // };
                             
                            mailgun.messages().send(data, function (error, body) {
                                console.log('6666666666666');
                                console.log(body);
                                if (error) {
                                    return reject(res.status(200).send({ status:200, success: false, message: "Somthing is wrong. Failed to send Email.", error:error}));
                                }
                                return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                            });
                            console.log('7777777777777777777');
                        });
   
                        //res.status(200).send({ status:200, success: true, message: "A code has been sent to your provided email id. Please check your email.",token:token  });
                    }else{
                        console.log('88888888888888888');
                        res.status(200).json({ status:200, success: false, message: "Somthing went wrong"});
                    }
                
                });
    
            }else{
                console.log('666666666666666666666');
                res.status(200).send({ status:200, success: false, message: "Email Address does not registered" });
            }

        }else if(type =='candidate'){
            console.log('77777777777777777');
            var check_email =await sequelize.query("SELECT * FROM candidates  WHERE username ='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            if(check_email.length > 0){
                console.log('222222222222222222');
                // var random_otp = Math.floor(1000 + Math.random() * 9000); 

                var email_content =await sequelize.query("SELECT * FROM `email_template` WHERE `email_template_id` = 4" ,{ type: Sequelize.QueryTypes.SELECT });
                var random_otp = Math.floor(1000 + Math.random() * 9000); 
                var replace = email_content[0].content.replace(/{{secret_code}}/g, ''+random_otp+'');
                var final_data = replace.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
    
                models.forgot_password.create({
                    email : email,
                    type : type,
                    otp : random_otp,
                }).then(function(candidate){
                    console.log('3333333333333');
    
                    if(candidate){
                        console.log('44444444444444444444');
                        return new Promise((resolve, reject) => {
                            console.log('5555555555555555');
                            ///////////////////////////// email send through template (dynamic ) start////////////////////////////
                            var data = {
                                from: 'Redford Team <admin@redfordrecruiters.com>',
                                to: [email],
                                subject: 'Redford Recruiters : '+email_content[0].subject+'',
                                html: '<!DOCTYPE html>'+
                                ''+final_data+''
                            };
                            ////////////////////// email send through template (dynamic ) end/////////////////////////////////

                            // var data = {
                            //     from: 'Redford Team <xxxxxxxx@redfordrecruiters.com>',
                            //     to: [email],
                            //     subject: 'Redford Recruiters : Reset password',
                            //     html: '<!DOCTYPE html>'+
                            //     '<html>'+    
                            //     '<head>'+
                            //     '</head>'+    
                            //     '<body>'+
                            //         '<div>'+
                            //             '<p>Hello Sir/Madam,</p>'+
                            //             '<p>Your secret code for reset password is: </p>'+ 
                            //             '<h3> <b>'+random_otp+'</b></h3>'+  
                            //             '<p>For any kind of query reach us at: <br />'+
                            //             'xxxxxxxx@redfordrecruiters.com<br />'+
                            //             'Have a nice day.</p>'+
                            //         '</div>'+       
                            //     '</body>'+    
                            //     '</html>' 
                            // };
                            mailgun.messages().send(data, function (error, body) {
                                console.log('6666666666666');
                                console.log(body);
                                if (error) {
                                    return reject(res.status(200).send({ status:200, success: false, message: "Somthing is wrong. Failed to send Email.", error:error}));
                                }
                                return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                            });
                            console.log('7777777777777777777');
                        });
    
                        //res.status(200).send({ status:200, success: true, message: "A code has been sent to your provided email id. Please check your email.",token:token  });
                    }else{
                        console.log('88888888888888888');
                        res.status(200).json({ status:200, success: false, message: "Somthing is wrong"});
                    }
                
                });
    
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Email Address does not registered" });
            }
        }else if(type =='admin'){
            console.log('77777777777777777');
            var check_email =await sequelize.query("SELECT * FROM admins  WHERE username ='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });

            if(check_email.length > 0){
                console.log('222222222222222222');
                // var random_otp = Math.floor(1000 + Math.random() * 9000); 

                var email_content =await sequelize.query("SELECT * FROM `email_template` WHERE `email_template_id` = 4" ,{ type: Sequelize.QueryTypes.SELECT });
                var random_otp = Math.floor(1000 + Math.random() * 9000); 
                var replace = email_content[0].content.replace(/{{secret_code}}/g, ''+random_otp+'');
                var final_data = replace.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
    
                models.forgot_password.create({
                    email : email,
                    type : type,
                    otp : random_otp,
                }).then(function(admins){
                    console.log('3333333333333');
    
                    if(admins){
                        console.log('44444444444444444444');
                        return new Promise((resolve, reject) => {
                            console.log('5555555555555555');
                            ///////////////////////////// email send through template (dynamic ) start////////////////////////////
                            var data = {
                                from: 'Redford Team <admin@redfordrecruiters.com>',
                                to: [email],
                                subject: 'Redford Recruiters : '+email_content[0].subject+'',
                                html: '<!DOCTYPE html>'+
                                ''+final_data+''
                            };
                            ////////////////////// email send through template (dynamic ) end/////////////////////////////////

                            // var data = {
                            //     from: 'Redford Team <xxxxxxxx@redfordrecruiters.com>',
                            //     to: [email],
                            //     subject: 'Redford Recruiters : Reset password',
                            //     html: '<!DOCTYPE html>'+
                            //     '<html>'+    
                            //     '<head>'+
                            //     '</head>'+    
                            //     '<body>'+
                            //         '<div>'+
                            //             '<p>Hello Sir/Madam,</p>'+
                            //             '<p>Your secret code for reset password is: </p>'+ 
                            //             '<h3> <b>'+random_otp+'</b></h3>'+  
                            //             '<p>For any kind of query reach us at: <br />'+
                            //             'xxxxxxxx@redfordrecruiters.com<br />'+
                            //             'Have a nice day.</p>'+
                            //         '</div>'+       
                            //     '</body>'+    
                            //     '</html>' 
                            // };
                            mailgun.messages().send(data, function (error, body) {
                                console.log('6666666666666');
                                console.log(body);
                                if (error) {
                                    return reject(res.status(200).send({ status:200, success: false, message: "Somthing is wrong. Failed to send Email.", error:error}));
                                }
                                return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                            });
                            console.log('7777777777777777777');
                        });
    
                        //res.status(200).send({ status:200, success: true, message: "A code has been sent to your provided email id. Please check your email.",token:token  });
                    }else{
                        console.log('88888888888888888');
                        res.status(200).json({ status:200, success: false, message: "Somthing is wrong"});
                    }
                
                });
    
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Email Address does not registered" });
            }
        }
 
    }else{
        console.log('13131311313');
        res.status(200).json({ status:200, success: false, message: "All fileds are required"});
    }  
}

function randomString() {
    var length = 4;
    // var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

/************************* admin and client and candidate forgot password ends *******************************/


/************************* admin and client and candidate forgot password otp check start *******************************/

exports.forgot_password_otp_check =async function(req, res, next) {
    console.log(req.body.data);
    
    var email = req.body.data.email;
    var type = req.body.data.type;
    var otp = req.body.data.otp;
    
    if( email && email !='' && type && type !='' && otp && otp !='' ){
        console.log('1111111111111111111');
        console.log(type);
        if(type =='client'){
            console.log('222222222222222');
            var otp_verification =await sequelize.query("SELECT * FROM forgot_password  WHERE email = '"+email+"' and type = '"+type+"' and otp = "+otp+" limit 1",{ type: Sequelize.QueryTypes.SELECT });

            if(otp_verification.length > 0){
                console.log('222222222222222222');

                var client_check =await sequelize.query("SELECT client_id, username FROM clients  WHERE username ='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
                res.status(200).send({ status:200, success: true, message: "Secret code match", client_id: client_check[0].client_id });
                
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Secret code does not match" });
            }

        }else if(type =='candidate'){
            console.log('77777777777777777');

            var otp_verification =await sequelize.query("SELECT * FROM forgot_password  WHERE email = '"+email+"' and type = '"+type+"' and otp = "+otp+" limit 1",{ type: Sequelize.QueryTypes.SELECT });

            if(otp_verification.length > 0){
                console.log('222222222222222222');

                var candidate_check =await sequelize.query("SELECT candidate_id, username FROM candidates  WHERE username ='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
                res.status(200).send({ status:200, success: true, message: "Secret code match", candidate_id: candidate_check[0].candidate_id });
                
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Secret code does not match" });
            }
        }else if(type =='admin'){
            console.log('77777777777777777');

            var otp_verification =await sequelize.query("SELECT * FROM forgot_password  WHERE email = '"+email+"' and type = '"+type+"' and otp = "+otp+" limit 1",{ type: Sequelize.QueryTypes.SELECT });

            if(otp_verification.length > 0){
                console.log('222222222222222222');

                var admin_check =await sequelize.query("SELECT admin_id, username FROM admins  WHERE username ='"+email+"' limit 1",{ type: Sequelize.QueryTypes.SELECT });
                res.status(200).send({ status:200, success: true, message: "Secret code match", admin_id: admin_check[0].admin_id });
                
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Secret code does not match" });
            }
        }
 
    }else{
        console.log('13131311313');
        res.status(200).json({ status:200, success: false, message: "All fileds are required"});
    }  
}

/************************* admin and client and candidate forgot password otp check ends *******************************/


/************************* admin and client and candidate forgot password update start *******************************/

exports.forgot_password_update =async function(req, res, next) {
    console.log(req.body.data);
    
    var email = req.body.data.email;
    var type = req.body.data.type;
    var password = req.body.data.password;
    var user_id = req.body.data.user_id;
    
    if( email && email !='' && type && type !='' && password && password !='' && user_id && user_id !='' ){
        console.log('1111111111111111111');
        console.log(type);
        if(type =='client'){
            console.log('222222222222222');
            var client_verification =await sequelize.query("SELECT * FROM clients  WHERE client_id = "+user_id+" and username = '"+email+"'",{ type: Sequelize.QueryTypes.SELECT });

            if(client_verification.length > 0){
                console.log('222222222222222222');

                var hash = bcrypt.hashSync(password);
                models.clients.update({ 

                    password : hash,

                },{where:{client_id:client_verification[0].client_id}}).then(function(client) {
                    res.status(200).send({ status:200, success: true, message: "Your password has been reset successfully" });
                })
    
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Client not found." });
            }

        }else if(type =='candidate'){
            console.log('77777777777777777');

            var candidate_verification =await sequelize.query("SELECT * FROM candidates  WHERE candidate_id = "+user_id+" and username = '"+email+"'",{ type: Sequelize.QueryTypes.SELECT });

            if(candidate_verification.length > 0){
                console.log('222222222222222222');

                var hash = bcrypt.hashSync(password);
                models.candidates.update({ 

                    password : hash,

                },{where:{candidate_id:candidate_verification[0].candidate_id}}).then(function(candidates) {
                    res.status(200).send({ status:200, success: true, message: "Your password has been reset successfully" });
                })
    
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Candidate not found." });
            }
        }else if(type =='admin'){
            console.log('77777777777777777');

            var admin_verification =await sequelize.query("SELECT * FROM admins  WHERE admin_id = "+user_id+" and username = '"+email+"'",{ type: Sequelize.QueryTypes.SELECT });

            if(admin_verification.length > 0){
                console.log('222222222222222222');

                var hash = bcrypt.hashSync(password);
                models.admins.update({ 

                    password : hash,

                },{where:{admin_id:admin_verification[0].admin_id}}).then(function(admins) {
                    res.status(200).send({ status:200, success: true, message: "Your password has been reset successfully" });
                })
    
            }else{
                console.log('99999999999999999999');
                res.status(200).send({ status:200, success: false, message: "Admin not found." });
            }
        }
 
    }else{
        console.log('13131311313');
        res.status(200).json({ status:200, success: false, message: "All fileds are required"});
    }  
}

/************************* admin and client and candidate forgot password update ends *******************************/



/************************* who we are page upload start *******************************/

exports.who_we_are_upload = async function(req, res, next) {
    console.log(req.body.data);

    var title = req.body.data.title;
    var content = req.body.data.content;
    var status = req.body.data.status;
    var user_id = req.body.data.user_id ? req.body.data.user_id : '';

    var who_we_are_id = req.body.data.who_we_are_id;

    
    if(title && title !='' && content && content !='' && status && status !=''){
        console.log('11111111111111111111111');
        if(!who_we_are_id){
            console.log('22222222222222222222222');
            models.who_we_are.create({ 

                title: title,
                content: content,
                status: status,
                createdBy: user_id

            }).then(function(who_we_are) {

                res.status(200).send({ status:200, success: true, message: "Who we are successfully added" });

            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.who_we_are.update({ 

                title: title,
                content: content,
                status: status,
                updatedBy: user_id

            },{where:{who_we_are_id:who_we_are_id}}).then(function(who_we_are) {

                res.status(200).send({ status:200, success: true, message: "Who we are successfully updated" });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "All fileds are required"});
    }
} 

/************************* Who we are page upload ends *******************************/


/************************* who we are details start *******************************/
exports.who_we_are_details = async function(req, res, next) {
    console.log(req.body.data);
    //var who_we_are_id = req.body.data.who_we_are_id;
    var user_type = req.body.data.user_type;

    //if(who_we_are_id && who_we_are_id !='' ){

        if(user_type =='admin' ){
            var who_we_are_details = await sequelize.query("SELECT * FROM `who_we_are` where 1 limit 1",{ type: Sequelize.QueryTypes.SELECT });
        }else{
            var who_we_are_details = await sequelize.query("SELECT * FROM `who_we_are` where status = 'active' limit 1",{ type: Sequelize.QueryTypes.SELECT });
        }
                                        
        if(who_we_are_details.length > 0) {

            res.status(200).send({ status:200, success: true, who_we_are_details:who_we_are_details[0] });  
        } else {
            res.status(200).send({ status:200, success: false, message: "Who we are data not found" }); 
        }

    // }else{
    //     res.status(200).json({ status:200, success: false, message: "Who we are id is required!"});
    // }
}
/************************* who we are details ends *******************************/


/************************* contact us upload start *******************************/

exports.contact_us_upload = async function(req, res, next) {
    console.log(req.body.data);

    var name = req.body.data.name;
    var company = req.body.data.company;
    var email = req.body.data.email;
    var mobile_country_code = req.body.data.mobile_country_code;
    var mobile = req.body.data.mobile;
    var message = req.body.data.message;

    var contact_us_id = req.body.data.contact_us_id;

    
    if(name && name !='' && email && email !='' && message && message !=''){
        console.log('11111111111111111111111');
        if(!contact_us_id){
            console.log('22222222222222222222222');
            models.contact_us.create({ 

                name: name,
                company: company,
                email: email,
                mobile_country_code: mobile_country_code,
                mobile: mobile,
                message: message

            }).then(async function(contact_us) {

                // ///////////////////// email(ack) to admin for contact us request successfully submit start ///////////
                        
                // var email_template_data_for_admin =await sequelize.query("SELECT email_template.* from email_template where email_template.email_template_id = 6",{ type: Sequelize.QueryTypes.SELECT });
                // var admin_email_final_data = final_data.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');

                // var admin_details =await sequelize.query("SELECT admins.* from admins where admin_id = 8",{ type: Sequelize.QueryTypes.SELECT });
                // // return new Promise((resolve, reject) => {
                //     console.log('5555555555555555');
                //     var data1 = {
                //         from: 'Redford Team <admin@redfordrecruiters.com>',
                //         to: [admin_details[0].username],
                //         subject: 'Redford Recruiters : '+email_template_data_for_admin[0].subject+'',
                //         html: '<!DOCTYPE html>'+
                //         ''+admin_email_final_data+'' 
                //     };
                //     mailgun.messages().send(data1, function (error, body) {
                //         console.log('6666666666666');
                //         console.log(body);
                //         // if (error) {
                //             // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  }));
                //         // }
                //         // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                //     });
                //     console.log('7777777777777777777');
                // // });
                // ///////////////////// email(ack) to admin for contact us request successfully submit end ///////////

                res.status(200).send({ status:200, success: true, message: "Thank you for contacting us! We will get back to you soon." });

            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }else{
            console.log('333333333333333333333333333');
            models.contact_us.update({ 

                name: name,
                company: company,
                email: email,
                mobile_country_code: mobile_country_code,
                mobile: mobile,
                message: message

            },{where:{contact_us_id:contact_us_id}}).then(function(contact_us) {

                res.status(200).send({ status:200, success: true, message: "Thank you for contacting us! We will get back to you soon." });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });

        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Name, Email, Message fileds are required"});
    }
} 

/************************* contact us upload ends *******************************/


/************************* contact us list start *******************************/
exports.contact_us_list = async function(req, res, next) {

    // var contact_us_list = await sequelize.query("SELECT * FROM `contact_us`",{ type: Sequelize.QueryTypes.SELECT });
                                      
    // if(contact_us_list.length > 0) {
    //     res.status(200).send({ status:200,success: true, contact_us_list:contact_us_list });  
    // } else {
    //     res.status(200).send({ status:200,success: false, message: "No contact us found" }); 
    // }

    var offset = req.body.data.start ? req.body.data.start : 0;
    var limit = req.body.data.limit ? req.body.data.limit : 10;

    var contact_us_count = await sequelize.query("SELECT * FROM `contact_us`",{ type: Sequelize.QueryTypes.SELECT });
    var contact_us_list = await sequelize.query("SELECT * FROM `contact_us` order by contact_us_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

    if(contact_us_list.length > 0) {
        res.status(200).send({ status:200,success: true, contact_us_list:contact_us_list, contact_us_count: contact_us_count.length });  
    } else {
        res.status(200).send({ status:200,success: false, message: "No contact us found" }); 
    }
}
/************************* contact us list ends *******************************/


/************************* contact us details start ********************************/
exports.contact_us_details = async function(req, res, next) {
    console.log(req.body.data);
    var contact_us_id = req.body.data.contact_us_id;
    if(contact_us_id && contact_us_id !=''){

        var contact_us_details = await sequelize.query("SELECT * FROM `contact_us` where contact_us_id ="+contact_us_id,{ type: Sequelize.QueryTypes.SELECT });                                
        
        if(contact_us_details.length > 0) {
            res.status(200).send({ status:200, success: true, contact_us_details:contact_us_details[0] });  
        } else {
            res.status(200).send({ status:200, success: false, message: "Contact us data not found"}); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Contact us id is required!"});
    }
}
/************************* contact us details ends *******************************/