var models = require('../../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var fs = require('file-system');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../../config/config.json');
const emailConfig = require('../../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
    config.development.database, 
    config.development.username,
    config.development.password, {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        // SQLite only
        //storage: 'path/to/database.sqlite'
    }
);


/************************* company list for client on job posting form start *******************************/

exports.client_company_list_on_job_posting_form = async function(req, res, next) {

    var client_id = req.body.data.client_id;

    if(client_id && client_id !='' ){
        
        var client_company_listing = await sequelize.query("SELECT companies.company_id, companies.client_id, companies.company_name from companies where companies.client_id ="+client_id+" order by companies.company_name ASC",{ type: Sequelize.QueryTypes.SELECT });

        if(client_company_listing.length > 0){

            res.status(200).send({ status:200, success: true, client_company_listing: client_company_listing });
        }else{
            res.status(200).json({ status:200, success: false, message: "No Company found for this client!"});
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    }    
}


/************************* company list for client on job posting form ends *******************************/


/************************* company details on job posting form start *******************************/

exports.company_details_on_job_posting_form = async function(req, res, next) {

    var company_id = req.body.data.company_id;
    var resultArray = [];

    if(company_id && company_id !=''){

        var company_details = await sequelize.query("SELECT companies.*, company_contact_info.*, categories.title as category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });

        if(company_details.length > 0){

            var i = 0;
            company_details.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "company_name": element.company_name,
                    "category": element.category,
                    "logo": company_logo,
                    "category_title": element.category_title
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, company_details:resultArray[0] });
        }else{
            res.status(200).json({ status:200, success: false, message: "Company details not found"});
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Company id is required!"});
    }    
}


/************************* company details on job posting form ends *******************************/


/************************* job posting form show start *******************************/

exports.job_posting_form = function(req, res, next) {

    var category_details = sequelize.query("SELECT categories.category_id, categories.title as category_title from categories where status ='active' ORDER BY `category_id` DESC",{ type: Sequelize.QueryTypes.SELECT })
    category_details.then(function (category_details) {

        //if(category_details.length >0 ){            
            res.status(200).send({ status:200, success: true, category_details:category_details });             
        //}else{            
        //    res.status(200).send({ status:200, success: false,message: "Candidate experience not found" });
        //}/

    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });
}

/************************* job posting form show ends *******************************/

/************************* job posting upload start *******************************/

exports.job_posting = async function(req, res, next) {
    console.log(req.body.data);

    var job_opening_status = req.body.data.job_opening_status;
    var job_title = req.body.data.job_title;
    var job_type = req.body.data.job_type;
    var date_opened = req.body.data.date_opened;
    var target_date = req.body.data.target_date;
    var basic_info_cuntry = req.body.data.basic_info_cuntry;
    var basic_info_state = req.body.data.basic_info_state;
    var basic_info_city = req.body.data.basic_info_city;
    var job_category = req.body.data.job_category;
    var person_gender = req.body.data.person_gender;
    var person_nationality = req.body.data.person_nationality;
    var person_work_exp = req.body.data.person_work_exp;
    var person_language = req.body.data.person_language;
    var person_salary = req.body.data.person_salary;
    var person_city = req.body.data.person_city;
    var healthcare_license = req.body.data.healthcare_license;
    var licenses_number = req.body.data.licenses_number;
    var company_id = req.body.data.company_id;
    var client_id = req.body.data.client_id;
    var currency = req.body.data.currency;
    // var client_name = req.body.data.client_name;
    // var client_contact_name = req.body.data.client_contact_name;
    // var client_address = req.body.data.client_address;
    // var client_country = req.body.data.client_country;
    // var client_state = req.body.data.client_state;
    // var client_city = req.body.data.client_city;
    var job_description = req.body.data.job_description;
    var requirements = req.body.data.requirements;
    var skill_set = req.body.data.skill_set;
    var no_of_positions = req.body.data.no_of_positions;
    var responsibilities = req.body.data.responsibilities;
    var min_qualification = req.body.data.min_qualification;
    var save_as_draft = req.body.data.save_as_draft;
    var clone_this_job = req.body.data.clone_this_job;
    var job_id = req.body.data.job_id;

    var user_id = req.body.data.user_id;
    var user_type = req.body.data.user_type;

    var msg = '';

    if(save_as_draft == 'true'){
        //console.log('11111111111111111111');
        var status = 'draft';
    }else{
        //console.log('222222222222222222');
        var status = 'publish';
    }

    if(job_opening_status && job_opening_status !='' && job_title && job_title !='' && job_type && job_type !='' && date_opened && date_opened !='' && target_date && target_date !='' && basic_info_cuntry && basic_info_cuntry !='' && basic_info_state && basic_info_state !='' && basic_info_city && basic_info_city !='' && job_category && job_category != '' && person_work_exp && person_work_exp !='' && company_id && company_id !='' && client_id && client_id!='' && job_description && job_description !='' && no_of_positions && no_of_positions !='' && user_id && user_id !='' && user_type && user_type != '' ){
        
        if(!job_id){
            console.log('33333333333333333333333');

            if(user_type == 'admin'){
                //console.log('11111111111111111111');
                var is_verified = 'Yes';
            }else{
                //console.log('222222222222222222');
                var is_verified = 'No';
            }

            models.jobs.create({ 

                job_opening_status: job_opening_status,
                job_title: job_title,
                job_type: job_type,
                date_opened: date_opened,
                target_date: target_date,
                basic_info_cuntry: basic_info_cuntry,
                basic_info_state: basic_info_state,
                basic_info_city: basic_info_city,
                job_category: job_category,
                person_gender: person_gender,
                person_nationality: person_nationality,
                person_work_exp: person_work_exp,
                person_language: person_language,
                person_salary: person_salary,
                person_city: person_city,
                healthcare_license: healthcare_license,
                licenses_number: licenses_number,
                company_id: company_id,
                client_id: client_id,
                currency: currency,
                // client_name: client_name,
                // client_contact_name: client_contact_name,
                // client_address: client_address,
                // client_country: client_country,
                // client_state: client_state,
                // client_city: client_city,
                job_description: job_description,
                requirements: requirements,
                skill_set: skill_set,
                no_of_positions: no_of_positions,
                responsibilities: responsibilities,
                min_qualification: min_qualification,
                is_verified: is_verified,
                status: status,

            }).then(async function(jobs) {
                if(jobs){
                    models.job_status_log.create({ 

                        job_id: jobs.job_id,
                        job_status: job_opening_status,
                        user_type: user_type,
                        createdBy: user_id
        
                    }).then(async function(job_status_log){

                        if(status == 'draft'){
                            msg = "Job successfully Posted in draft";
                        }else{
                            msg = "Job successfully Posted";
                        }

                        res.status(200).send({ success: true, message: msg, job_details:jobs, job_id: jobs.job_id });
                    })
                    .catch(function(error) {
                        return res.send( {success: false, error});
                    });
                }else{
                    res.status(200).json({ success: false, message: "Something went wrong. Job did't posted!"});
                }
            });

        }else{
            console.log('444444444444444444444');
            
            if(clone_this_job == 'true'){

                var clone_job = await sequelize.query("SELECT jobs.* from jobs where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });

                if(clone_job.length > 0){

                    models.jobs.create({ 

                        job_opening_status: clone_job[0].job_opening_status,
                        job_title: clone_job[0].job_title,
                        job_type: clone_job[0].job_type,
                        date_opened: clone_job[0].date_opened,
                        target_date: clone_job[0].target_date,
                        basic_info_cuntry: clone_job[0].basic_info_cuntry,
                        basic_info_state: clone_job[0].basic_info_state,
                        basic_info_city: clone_job[0].basic_info_city,
                        job_category: clone_job[0].job_category,
                        person_gender: clone_job[0].person_gender,
                        person_nationality: clone_job[0].person_nationality,
                        person_work_exp: clone_job[0].person_work_exp,
                        person_language: clone_job[0].person_language,
                        person_salary: clone_job[0].person_salary,
                        person_city: clone_job[0].person_city,
                        healthcare_license: clone_job[0].healthcare_license,
                        licenses_number: clone_job[0].licenses_number,
                        company_id: clone_job[0].company_id,
                        client_id: clone_job[0].client_id,
                        currency: clone_job[0].currency,
                        // client_name: client_name,
                        // client_contact_name: client_contact_name,
                        // client_address: client_address,
                        // client_country: client_country,
                        // client_state: client_state,
                        // client_city: client_city,
                        job_description: clone_job[0].job_description,
                        requirements: clone_job[0].requirements,
                        skill_set: clone_job[0].skill_set,
                        no_of_positions: clone_job[0].no_of_positions,
                        responsibilities: clone_job[0].responsibilities,
                        min_qualification: clone_job[0].min_qualification,
                        is_verified: clone_job[0].is_verified,
                        status: clone_job[0].status,
        
                    }).then(async function(jobs) {
        
                    
                        msg = "Job successfully cloned";
        
                    res.status(200).send({ success: true, message: msg, job_details:jobs, job_id: jobs.job_id });
                    })
                    .catch(function(error) {
                        return res.send( {success: false, error});
                    });
                }else{
                    res.status(200).json({ status:200, success: false, message: "job not found for clone"});
                }

            }else{
                console.log('222222222222222222');
                var job_opening_status_check = await sequelize.query("SELECT jobs.job_opening_status from jobs where job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });

                models.jobs.update({ 

                    job_opening_status: job_opening_status,
                    job_title: job_title,
                    job_type: job_type,
                    date_opened: date_opened,
                    target_date: target_date,
                    basic_info_cuntry: basic_info_cuntry,
                    basic_info_state: basic_info_state,
                    basic_info_city: basic_info_city,
                    job_category: job_category,
                    person_gender: person_gender,
                    person_nationality: person_nationality,
                    person_work_exp: person_work_exp,
                    person_language: person_language,
                    person_salary: person_salary,
                    person_city: person_city,
                    healthcare_license: healthcare_license,
                    licenses_number: licenses_number,
                    company_id: company_id,
                    client_id: client_id,
                    currency: currency,
                    // client_name: client_name,
                    // client_contact_name: client_contact_name,
                    // client_address: client_address,
                    // client_country: client_country,
                    // client_state: client_state,
                    // client_city: client_city,
                    job_description: job_description,
                    requirements: requirements,
                    skill_set: skill_set,
                    no_of_positions: no_of_positions,
                    responsibilities: responsibilities,
                    min_qualification: min_qualification,
                    // is_verified: is_verified,
                    status: status,
    
                },{where:{job_id:job_id}}).then(async function(jobs) {
                    console.log('33333333333333333');

                    
                    console.log(job_opening_status_check);
                    console.log(job_opening_status);
                    console.log(job_opening_status_check[0].job_opening_status);
                    if(jobs){
                        console.log('44444444444444');
                        if(job_opening_status_check[0].job_opening_status != job_opening_status){
                            console.log('5555555555555555555555555555');

                            models.job_status_log.create({ 
                                job_id: job_id,
                                job_status: job_opening_status,
                                user_type: user_type,
                                createdBy: user_id
                            })
                        }
    
                            if(status == 'draft'){
                                msg = "Job successfully updated in draft";
                            }else{
                                msg = "Job successfully updated";
                            }
            
                            var updated_job = sequelize.query("SELECT jobs.* from jobs where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT })
                            updated_job.then(function (updated_job) {
            
                                res.status(200).send({ success: true, message: msg, job_details:updated_job, job_id: job_id });
            
                            }).catch(function(error) {
                                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                            });
                            // var updated_job = await models.jobs.findOne({where:{job_id : jobs.job_id}});
                            //var updated_job = await sequelize.query("SELECT jobs.* from jobs where job_id ="+jobs.job_id,{ type: Sequelize.QueryTypes.SELECT });

                            //res.status(200).send({ success: true, message: msg, job_details:updated_job, job_id: jobs.job_id });
                        
                    }else{
                        res.status(200).json({ success: false, message: "Something went wrong. Job did't posted!"});
                    }
                })
                .catch(function(error) {
                    return res.send( {success: false, error});
                });
            }   

        }
    }else{
        console.log('55555555555555555555555');
        res.status(200).json({ success: false, message: "All fileds are required!"});
    } 
}

/************************* job posting upload ends *******************************/


/************************* candidate job listing start *******************************/

exports.candidate_job_listing = async function(req, res, next) {
    console.log(req.body.data);

    var offset = req.body.data.start ? req.body.data.start : 0;
    var limit = req.body.data.limit ? req.body.data.limit : 10;

    var job_category_id = req.body.data.job_category_id ? req.body.data.job_category_id : '';
    var job_location = req.body.data.job_location ? req.body.data.job_location : '';
    var candidate_id = req.body.data.candidate_id ? req.body.data.candidate_id : '';
    var resultArray = [];

    var job_by_category = await sequelize.query("SELECT categories.category_id, categories.title, (SELECT COUNT(*) FROM jobs WHERE categories.category_id=jobs.job_category) as job_count_by_category FROM categories where categories.status='active' order by categories.title ASC",{ type: Sequelize.QueryTypes.SELECT });
    var job_by_location = await sequelize.query("SELECT COUNT(*) as job_count_by_location, basic_info_state as job_location FROM jobs GROUP BY basic_info_state",{ type: Sequelize.QueryTypes.SELECT });
    // var job_list_by_default_cat = await sequelize.query("SELECT jobs.* from jobs where job_category ="+job_by_category[0].category_id,{ type: Sequelize.QueryTypes.SELECT });

    

    if(job_category_id =='' && job_location !='' ){
        //console.log('111111111111111111111111111');
        
        if(candidate_id && candidate_id !='' ){
            // var job_list_by_filter = await sequelize.query("SELECT jobs.*, companies.company_name, companies.category, companies.logo, If(client_favourite_job.client_favourite_job_id != 'NULL', 'yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id left join client_favourite_job on client_favourite_job.job_id = jobs.job_id and client_favourite_job.client_id="+client_id+" where jobs.client_id ="+client_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_count_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.basic_info_state ='"+job_location+"' ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.basic_info_state ='"+job_location+"' ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

            var i = 0;
            job_list_by_filter.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": element.favourite
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_filter.length });
            

        }else{
            var job_count_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.basic_info_state ='"+job_location+"' ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.basic_info_state ='"+job_location+"' ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

            var i = 0;
            job_list_by_filter.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": 'No'
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count:job_count_by_filter.length });

        }
        //var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.basic_info_state ='"+job_location+"' ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where basic_info_state ='"+job_location+"' ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });


    }else if(job_location =='' && job_category_id !='' ){
        console.log('22222222222222222222');

        if(candidate_id && candidate_id !='' ){
            var job_count_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

            var i = 0;
            job_list_by_filter.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": element.favourite
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_filter.length });
           

        }else{
            var job_count_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

            var i = 0;
            job_list_by_filter.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": 'No'
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_filter.length });
           

        }

        // var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where job_category ="+job_category_id+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
     

    }else if(job_location !='' && job_category_id !='' ){
        console.log('333333333333333333333333333333333333333');

        if(candidate_id && candidate_id !='' ){
            var job_count_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.basic_info_state ='"+job_location+"' and jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.basic_info_state ='"+job_location+"' and jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

            
            var i = 0;
            job_list_by_filter.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": element.favourite
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_filter.length });
            
        }else{
            var job_count_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.basic_info_state ='"+job_location+"' and jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.basic_info_state ='"+job_location+"' and jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

            var i = 0;
            job_list_by_filter.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": 'No'
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_filter.length });
            
        }

        // var job_list_by_filter = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.basic_info_state ='"+job_location+"' and jobs.job_category ="+job_category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where basic_info_state ='"+job_location+"' and job_category ="+job_category_id+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });


    }else{
        console.log('44444444444444444444444444');
        if(candidate_id && candidate_id !=''){
            console.log('aaaaaaaaaaaaaaaaaaaaa');

            var candidate_category = await sequelize.query("SELECT candidate_looking_for.* from candidate_looking_for where candidate_looking_for.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
            if(candidate_category.length > 0){
                console.log('bbbbbbbbbbbbbbbbbbbbbbbb');
                var job_count_by_candidate_category = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.job_category ="+candidate_category[0].category+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                var job_list_by_candidate_category = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title, If(candidate_favourite_job.candidate_favourite_job_id != 'NULL', 'Yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id left join candidate_favourite_job on candidate_favourite_job.job_id = jobs.job_id and candidate_favourite_job.candidate_id="+candidate_id+" where jobs.job_category ="+candidate_category[0].category+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });
                // var job_list_by_candidate_category = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+candidate_category[0].category+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                // var job_list_by_candidate_category = await sequelize.query("SELECT jobs.* from jobs where job_category ="+candidate_category[0].category+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });

                
                var i = 0;
                job_list_by_candidate_category.forEach(function(element,index){ 
    
                    if(element.logo !='' && element.logo!= null){
                        var company_logo = req.app.locals.baseurl+element.logo;
                    }else{
                        var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                    }
    
                    resultArray.push({
                        "job_id": element.job_id,
                        "job_title": element.job_title,
                        "job_type": element.job_type,
                        "basic_info_cuntry": element.basic_info_cuntry,
                        "basic_info_state": element.basic_info_state,
                        "basic_info_city": element.basic_info_city,
                        "job_category": element.job_category,
                        "person_salary": element.person_salary,
                        "currency": element.currency,
                        "client_id": element.client_id,
                        "job_description": element.job_description,
                        "skill_set": element.skill_set,
                        "createdAt": element.createdAt,
                        "company_id": element.company_id,
                        "company_name": element.company_name,
                        "company_logo": company_logo,
                        "favourite_job": element.favourite
                    });    
                    i++;             
                });
                res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_candidate_category.length });
               
            }else{
                console.log('ccccccccccccccccccc');
                var job_count_by_default_cat = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_by_category[0].category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                var job_list_by_default_cat = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_by_category[0].category_id+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });
                
                var i = 0;
                job_list_by_default_cat.forEach(function(element,index){ 

                    if(element.logo !='' && element.logo!= null){
                        var company_logo = req.app.locals.baseurl+element.logo;
                    }else{
                        var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                    }

                    resultArray.push({
                        "job_id": element.job_id,
                        "job_title": element.job_title,
                        "job_type": element.job_type,
                        "basic_info_cuntry": element.basic_info_cuntry,
                        "basic_info_state": element.basic_info_state,
                        "basic_info_city": element.basic_info_city,
                        "job_category": element.job_category,
                        "person_salary": element.person_salary,
                        "currency": element.currency,
                        "client_id": element.client_id,
                        "job_description": element.job_description,
                        "skill_set": element.skill_set,
                        "createdAt": element.createdAt,
                        "company_id": element.company_id,
                        "company_name": element.company_name,
                        "company_logo": company_logo,
                        "favourite_job": 'No'
                    });    
                    i++;             
                });
                res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_default_cat.length });
               
            }
        }else{
            console.log('dddddddddddddddddddddddddd');
            var job_count_by_default_cat = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_by_category[0].category_id+" ORDER BY jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_default_cat = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_category ="+job_by_category[0].category_id+" ORDER BY jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });
            
            var i = 0;
            job_list_by_default_cat.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "person_salary": element.person_salary,
                    "currency": element.currency,
                    "client_id": element.client_id,
                    "job_description": element.job_description,
                    "skill_set": element.skill_set,
                    "createdAt": element.createdAt,
                    "company_id": element.company_id,
                    "company_name": element.company_name,
                    "company_logo": company_logo,
                    "favourite_job": 'No'
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:resultArray, job_category_id:job_category_id, job_count: job_count_by_default_cat.length });
           
        }
    }
}

/************************* candadite job listing ends *******************************/


/************************* candidate job listing start 111*******************************/

exports.candidate_job_listing_111 = async function(req, res, next) {
    // return res.send(req.body.data)
    console.log(req.body.data);
    var job_category_id = req.body.data.job_category_id ? req.body.data.job_category_id : '';
    var job_location = req.body.data.job_location ? req.body.data.job_location : '';
    var candidate_id = req.body.data.candidate_id ? req.body.data.candidate_id : '';

    // if(job_category_id && job_category_id !=''){

        var job_by_category = await sequelize.query("SELECT categories.category_id, categories.title, (SELECT COUNT(*) FROM jobs WHERE categories.category_id=jobs.job_category) as job_count_by_category FROM categories where categories.status='active' order by categories.title ASC",{ type: Sequelize.QueryTypes.SELECT });
        // var job_by_location = await sequelize.query("SELECT COUNT(*) as job_count_by_location, basic_info_cuntry as job_location FROM jobs GROUP BY basic_info_cuntry",{ type: Sequelize.QueryTypes.SELECT });
        var job_by_location = await sequelize.query("SELECT COUNT(*) as job_count_by_location, basic_info_state as job_location FROM jobs GROUP BY basic_info_state",{ type: Sequelize.QueryTypes.SELECT });
        var job_list_by_default_cat = await sequelize.query("SELECT jobs.* from jobs where job_category ="+job_by_category[0].category_id,{ type: Sequelize.QueryTypes.SELECT });

        //res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat });

        if(job_category_id =='' && job_location !='' ){
            console.log('111111111111111111111111111');
            // var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where basic_info_cuntry ='"+job_location+"' ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
            var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where basic_info_state ='"+job_location+"' ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });

            
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_filter, job_category_id:job_category_id });
            

        }else if(job_location =='' && job_category_id !='' ){
            console.log('22222222222222222222');
            var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where job_category ="+job_category_id+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });

            
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_filter, job_category_id:job_category_id });
           

        }else if(job_location !='' && job_category_id !='' ){
            console.log('333333333333333333333333333333333333333');
            var job_list_by_filter = await sequelize.query("SELECT jobs.* from jobs where basic_info_state ='"+job_location+"' and job_category ="+job_category_id+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });

            
            res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_filter, job_category_id:job_category_id });
            

        }else{
            console.log('44444444444444444444444444');
            if(candidate_id && candidate_id !=''){
                console.log('aaaaaaaaaaaaaaaaaaaaa');

                var candidate_category = await sequelize.query("SELECT candidate_looking_for.* from candidate_looking_for where candidate_looking_for.candidate_id ="+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
                if(candidate_category.length > 0){
                    console.log('bbbbbbbbbbbbbbbbbbbbbbbb');
                    var job_list_by_candidate_category = await sequelize.query("SELECT jobs.* from jobs where job_category ="+candidate_category[0].category+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
                    //var job_list_by_candidate_category = await sequelize.query("SELECT jobs.*, IFNULL(b.candidate_shortlisted_job_id) from jobs where job_category ="+candidate_category[0].category+" ORDER BY `job_id` DESC",{ type: Sequelize.QueryTypes.SELECT });
                    res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_candidate_category, job_category_id:candidate_category[0].category});
                }else{
                    console.log('ccccccccccccccccccc');
                    res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat, job_category_id:job_by_category[0].category_id});
                }
            }else{
                console.log('dddddddddddddddddddddddddd');
                res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat, job_category_id:job_by_category[0].category_id});
            }

            // res.status(200).send({ status:200, success: true, jobs_by_category:job_by_category, jobs_by_location:job_by_location, job_list:job_list_by_default_cat});
        }
        
    // }else{
    //     res.status(200).json({ success: "false",message: "Candidate id is required!"});
    // }  
}

/************************* candadite job listing ends 111*******************************/


/************************* candadite job details start *******************************/

exports.job_details = async function(req, res, next) {
    
    // var job_id = req.body.data.job_id;
    // var candidate_id = req.body.data.candidate_id;

    // if(job_id && job_id !=''){

    //     var candidate_job_already_applied = await sequelize.query("SELECT candidate_job_applied.* from candidate_job_applied where candidate_id ="+candidate_id+" and job_id ="+job_id ,{ type: Sequelize.QueryTypes.SELECT }); 

    //     var job_details = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });

    //     if(candidate_job_already_applied.length > 0 ){
    //         res.status(200).send({ status:200, success: true, job_details:job_details, job_applied: "true" });
    //     }else{

    //         res.status(200).send({ status:200, success: true, job_details:job_details, job_applied: "false" });
    //     }
        
    // }else{
    //     res.status(200).json({ status:200, success: false, message: "Job id is required!"});
    // }

    // return res.send();
    
    var job_id = req.body.data.job_id;
    var candidate_id = req.body.data.candidate_id;

    if(job_id && job_id !=''){

        var job_details = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });

        if(candidate_id && candidate_id !=''){

            var candidate_job_already_applied = await sequelize.query("SELECT candidate_job_applied.* from candidate_job_applied where candidate_id ="+candidate_id+" and job_id ="+job_id ,{ type: Sequelize.QueryTypes.SELECT }); 

            var related_job_with_key_skill = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.person_work_exp, jobs.client_name, jobs.person_salary, jobs.basic_info_city, jobs.basic_info_state, jobs.basic_info_cuntry from jobs where job_id != "+job_details[0].job_id+" and job_category ="+job_details[0].job_category+" and jobs.skill_set LIKE '%"+job_details[0].skill_set+"%' order by job_id DESC limit 5",{ type: Sequelize.QueryTypes.SELECT });
            var related_job_with_category = await sequelize.query("SELECT jobs.job_id, jobs.job_title, jobs.person_work_exp, jobs.client_name, jobs.person_salary, jobs.basic_info_city, jobs.basic_info_state, jobs.basic_info_cuntry from jobs where job_id != "+job_details[0].job_id+" and job_category ="+job_details[0].job_category+" order by job_id DESC limit 5",{ type: Sequelize.QueryTypes.SELECT });

            if(related_job_with_key_skill.length > 0 ){
                var related_job = related_job_with_key_skill;
            }else{
                var related_job = related_job_with_category;
            }

            if(candidate_job_already_applied.length > 0 ){
                res.status(200).send({ status:200, success: true, job_details:job_details, job_applied: "true", related_job:related_job });
            }else{
                res.status(200).send({ status:200, success: true, job_details:job_details, job_applied: "false", related_job:related_job });
            }

        }else{
            res.status(200).send({ status:200, success: true, job_details:job_details, job_applied: "false", related_job:[] });
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "Job id is required!"});
    } 
}

/************************* candadite job details ends *******************************/


/************************* candadite job applied start *******************************/

exports.candidate_job_apply = async function(req, res, next) {
    console.log(req.body.data);

    var job_id = req.body.data.job_id;
    var candidate_id = req.body.data.candidate_id;

    if(job_id && job_id !='' && candidate_id && candidate_id !=''){
        console.log('1111111111111111111111111111');

        // var candidate_default_job_opening_status = await sequelize.query("SELECT * FROM `candidate_job_opening_status` where status = 'active' ORDER BY sequence ASC limit 1",{ type: Sequelize.QueryTypes.SELECT });

        // var candidate_default_job_opening_sub_status = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where candidate_job_opening_status_id = "+candidate_default_job_opening_status[0].candidate_job_opening_status_id+" and status = 'active' ORDER BY sequence ASC limit 1",{ type: Sequelize.QueryTypes.SELECT });

        var candidate_default_job_opening_status = await sequelize.query("SELECT * FROM `candidate_job_opening_status` where status = 'active' ORDER BY sequence ASC limit 1",{ type: Sequelize.QueryTypes.SELECT });
        if(candidate_default_job_opening_status.length > 0 ){
            console.log('222222222222222222222222222');
            var candidate_job_opening_status_id = candidate_default_job_opening_status[0].candidate_job_opening_status_id;
            var candidate_default_job_opening_sub_status = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where candidate_job_opening_status_id = "+candidate_default_job_opening_status[0].candidate_job_opening_status_id+" and status = 'active' ORDER BY sequence ASC limit 1",{ type: Sequelize.QueryTypes.SELECT });
            if(candidate_default_job_opening_sub_status.length > 0 ){
                console.log('333333333333333333333333333');
                var candidate_job_opening_sub_status_id = candidate_default_job_opening_sub_status[0].candidate_job_opening_sub_status_id;
            }else{
                console.log('444444444444444444444');
                var candidate_job_opening_sub_status_id = ''
            }
        }else{
            console.log('555555555555555555555555');
            var candidate_job_opening_status_id = '';
            var candidate_job_opening_sub_status_id = '';
        }
        console.log('6666666666666666');
        models.candidate_job_applied.create({ 

            candidate_id: candidate_id,
            job_id: job_id,

        }).then(async function(candidate_job_applied) {
            console.log('777777777777777777777');

            models.assigned_job_to_candidates.create({
                candidate_id:candidate_id,
                job_id: job_id,
                user_type: 'candidate',
                createdBy: candidate_id,
                candidate_job_opening_status_id: candidate_job_opening_status_id,
                candidate_job_opening_sub_status_id : candidate_job_opening_sub_status_id,                            
            });

            models.candidate_status_log.create({
                candidate_id:candidate_id,
                job_id: job_id,
                user_type: 'candidate',
                createdBy: candidate_id,
                candidate_job_opening_status_id: candidate_job_opening_status_id,
                candidate_job_opening_sub_status_id : candidate_job_opening_sub_status_id,                            
            });
            console.log('88888888888888');
            var candidate_details =await sequelize.query("SELECT candidates.* from candidates where candidate_id = "+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
            var job_details =await sequelize.query("SELECT jobs.* from jobs where job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
            console.log(candidate_details[0].username);
            console.log(job_details[0].job_title);
            ///////////////////// email to candidate for successfully applying a job start ///////////
            var email_template_data_for_candidate =await sequelize.query("SELECT email_template.* from email_template where email_template.email_template_id = 2",{ type: Sequelize.QueryTypes.SELECT });
            var candidate_email_final_data = email_template_data_for_candidate[0].content.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');
            // return new Promise((resolve, reject) => {
                console.log('999999999999');
                var data = {
                    from: 'Redford Team <admin@redfordrecruiters.com>',
                    to: [candidate_details[0].username],
                    subject: 'Redford Recruiters : '+email_template_data_for_candidate[0].subject+'',
                    html: '<!DOCTYPE html>'+
                    ''+candidate_email_final_data+'' 
                };
                mailgun.messages().send(data, function (error, body) {
                    console.log('10101010101011010101');
                    console.log(body);
                    // if (error) {
                        // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  }));
                    // }
                    // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                });
                console.log('1212121212121211');
            // });
            ///////////////////// email to candidate for successfully applying a job end ///////////

            ///////////////////// email(ack) to admin for candidate successfully applying a job start ///////////
            
            var email_template_data_for_admin =await sequelize.query("SELECT email_template.* from email_template where email_template.email_template_id = 7",{ type: Sequelize.QueryTypes.SELECT });
            // var candidate_details =await sequelize.query("SELECT candidates.* from candidates where candidate_id = "+candidate_id,{ type: Sequelize.QueryTypes.SELECT });
            if(candidate_details[0].first_name !='' && candidate_details[0].first_name!= null){
                console.log('1313131313131313');
                var candidate_name = candidate_details[0].first_name+' '+candidate_details[0].last_name;
            }else if(candidate_details[0].name !='' && candidate_details[0].name!= null){
                var candidate_name = candidate_details[0].name;
            }else{
                var candidate_name = '';
            }
            // var job_details =await sequelize.query("SELECT jobs.* from jobs where job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
            
            var replace_for_admin = email_template_data_for_admin[0].content.replace(/{{candidate_name}}/g, ''+candidate_name+'');
            var final_data_for_admin = replace_for_admin.replace(/{{job_title}}/g, ''+job_details[0].job_title+'');
            var admin_email_final_data = final_data_for_admin.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');

            var admin_details =await sequelize.query("SELECT admins.* from admins where admin_id = 8",{ type: Sequelize.QueryTypes.SELECT });
            // return new Promise((resolve, reject) => {
                console.log('1414141414141414');
                var data1 = {
                    from: 'Redford Team <admin@redfordrecruiters.com>',
                    to: [admin_details[0].username],
                    subject: 'Redford Recruiters : '+email_template_data_for_admin[0].subject+'',
                    html: '<!DOCTYPE html>'+
                    ''+admin_email_final_data+'' 
                };
                mailgun.messages().send(data1, function (error, body) {
                    console.log('151515151515151515');
                    console.log(body);
                    // if (error) {
                        // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  }));
                    // }
                    // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                });
                console.log('16161666616161616');
            // });
            ///////////////////// email(ack) to admin for candidate successfully applying a job end ///////////

            ///////////////////// email(ack) to client for candidate successfully applying a job start ///////////
            
            var email_template_data_for_client =await sequelize.query("SELECT email_template.* from email_template where email_template.email_template_id = 8",{ type: Sequelize.QueryTypes.SELECT });
            // var job_details =await sequelize.query("SELECT jobs.* from jobs where job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
            var company_details =await sequelize.query("SELECT companies.* from companies where company_id = "+job_details[0].company_id,{ type: Sequelize.QueryTypes.SELECT });
            
            var replace_for_client = email_template_data_for_client[0].content.replace(/{{company_name}}/g, ''+company_details[0].company_name+'');
            var final_data_for_client = replace_for_client.replace(/{{job_title}}/g, ''+job_details[0].job_title+'');
            var client_email_final_data = final_data_for_client.replace(/email_template_logo/g, ''+req.app.locals.baseurl+'contents/email_template_logo'+'');

            var client_details =await sequelize.query("SELECT clients.* from clients where client_id = "+job_details[0].client_id,{ type: Sequelize.QueryTypes.SELECT });
            // return new Promise((resolve, reject) => {
                console.log('1717171717171717');
                var data2 = {
                    from: 'Redford Team <admin@redfordrecruiters.com>',
                    to: [client_details[0].username],
                    subject: 'Redford Recruiters : '+email_template_data_for_client[0].subject+'',
                    html: '<!DOCTYPE html>'+
                    ''+client_email_final_data+'' 
                };
                mailgun.messages().send(data2, function (error, body) {
                    console.log('1818181818181818');
                    console.log(body);
                    // if (error) {
                        // return reject(res.status(200).send({ success: "true", message: "User successfully registered. You can login now.", details:candidate_full_data[0], token:token  }));
                    // }
                    // return resolve(res.status(200).send({ status:200, success: true, message: "A secret code has been sent to your provided Email Address. Please check your Email inbox.", body:body}));
                });
                console.log('191919191919191919');
            // });
            ///////////////////// email(ack) to client for candidate successfully applying a job end ///////////

            res.status(200).send({ status:200, success: true, message: "Job successfully applied" });
        })
        .catch(function(error) {
            console.log('2020202020202020202020');
            //return res.send( {success: false, error});
            console.log(error);
        });
        console.log('21212121212121211');
        
    }else{
        console.log('2323232323232323');
        res.status(200).json({ status:200, success: false, message: "All fields are required!"});
    }  
}

/************************* candadite job applied ends *******************************/


/************************* candadite shortlisted job applied start *******************************/

exports.candidate_shortlisted_job = async function(req, res, next) {
    
    var job_id = req.body.data.job_id;
    var candidate_id = req.body.data.candidate_id;

    if(job_id && job_id !='' && candidate_id && candidate_id !=''){

        models.candidate_shortlisted_job.create({ 

            candidate_id: candidate_id,
            job_id: job_id,

        }).then(function(candidate_shortlisted_job) {

        res.status(200).send({ status:200, success: true, message: "Job successfully shortlisted" });
        })
        .catch(function(error) {
            return res.send( {status:200, success: false, error});
        });
        
    }else{
        res.status(200).json({ status:200, success: false, message: "All fields are required!"});
    }  
}

/************************* candadite shortlisted job applied ends *******************************/



// /************************* client job listing start *******************************/

// exports.client_job_listing = async function(req, res, next) {

//     var client_id = req.body.data.client_id;
//     var company_id = req.body.data.company_id;
//     var resultArray = [];

//     if(client_id && client_id !='' && company_id && company_id !=''){
        
//         // var client_job_listing = await sequelize.query("SELECT jobs.*, companies.company_name, companies.category, companies.logo, categories.title as job_category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON jobs.job_category = categories.category_id where jobs.client_id ="+client_id+" and jobs.company_id ="+company_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
//         var client_job_listing = await sequelize.query("SELECT jobs.*, companies.company_name, companies.category, companies.logo, categories.title as company_category_title, (SELECT categories.title FROM categories WHERE categories.category_id = jobs.job_category) as job_category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.client_id ="+client_id+" and jobs.company_id ="+company_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });

//         if(client_job_listing.length > 0){
//             var i = 0;
//             client_job_listing.forEach(function(element,index){ 

//                 if(element.logo !='' && element.logo!= null){
//                     var company_logo = req.app.locals.baseurl+element.logo;
//                 }else{
//                     var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
//                 }

//                 resultArray.push({
//                     "job_id": element.job_id,
//                     "job_opening_status": element.job_opening_status,
//                     "job_title": element.job_title,
//                     "job_type": element.job_type,
//                     "job_category": element.job_category,
//                     "job_category_title": element.job_category_title,
//                     "date_opened": element.date_opened,
//                     "target_date": element.target_date,
//                     "basic_info_cuntry": element.basic_info_cuntry,
//                     "basic_info_state": element.basic_info_state,
//                     "basic_info_city": element.basic_info_city,
//                     //"company_name": element.company_name,
//                     //"category": element.category,
//                     //"company_logo": company_logo,
//                     //"company_category_title": element.company_category_title
//                 });    
//                 i++;             
//             });
//             res.status(200).send({ status:200, success: true, company_details:resultArray });
//         }else{
//             res.status(200).json({ status:200, success: false, message: "job not found"});
//         }

//     }else{
//         res.status(200).json({ status:200, success: false, message: "Client id and company id is required!"});
//     } 
         
// }

// /************************* client job listing ends *******************************/

/************************* client job listing start *******************************/

exports.client_job_listing = async function(req, res, next) {
    console.log(req.body.data);

    var offset = req.body.data.start ? req.body.data.start : 0;
    var limit = req.body.data.limit ? req.body.data.limit : 10;

    var client_id = req.body.data.client_id;
    var company_id = req.body.data.company_id;
    var resultArray = [];

    if(client_id && client_id !='' && company_id && company_id !=''){
        
        // var client_job_listing = await sequelize.query("SELECT jobs.*, companies.company_name, companies.category, companies.logo, categories.title as job_category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON jobs.job_category = categories.category_id where jobs.client_id ="+client_id+" and jobs.company_id ="+company_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        //var client_job_listing = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where jobs.client_id ="+client_id+" and jobs.company_id ="+company_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        var client_job_count = await sequelize.query("SELECT jobs.*, categories.title as job_category_title, job_opening_status.title as job_opening_status_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id LEFT JOIN job_opening_status ON jobs.job_opening_status = job_opening_status.job_opening_status_id where jobs.client_id ="+client_id+" and jobs.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });
        var client_job_listing = await sequelize.query("SELECT jobs.*, categories.title as job_category_title, job_opening_status.title as job_opening_status_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id LEFT JOIN job_opening_status ON jobs.job_opening_status = job_opening_status.job_opening_status_id where jobs.client_id ="+client_id+" and jobs.company_id ="+company_id+" order by jobs.job_id DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT });

        var company_details = await sequelize.query("SELECT companies.*, company_contact_info.address, company_contact_info.city, company_contact_info.state, company_contact_info.country, company_contact_info.website, categories.title as company_category_title from companies LEFT JOIN company_contact_info ON companies.company_id = company_contact_info.company_id LEFT JOIN categories ON companies.category = categories.category_id where companies.company_id ="+company_id,{ type: Sequelize.QueryTypes.SELECT });
        if(client_job_listing.length > 0){
            var i = 0;
            company_details.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "company_id": element.company_id,
                    "company_size": element.company_size,
                    "company_email": element.company_email,
                    "company_country_code": element.company_country_code,
                    "company_phone": element.company_phone,
                    "address": element.address,
                    "city": element.city,
                    "state": element.state,
                    "country": element.country,
                    "website": element.website,
                    "company_name": element.company_name,
                    "category": element.category,
                    "company_logo": company_logo,
                    "company_category_title": element.company_category_title
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, job_list:client_job_listing, company_information:resultArray, client_job_count:client_job_count.length });
        }else{
            res.status(200).json({ status:200, success: false, message: "No job posted yet."});
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Client id and company id is required!"});
    } 
         
}

/************************* client job listing ends *******************************/

/************************* client job details start *******************************/

exports.client_job_details = async function(req, res, next) {
    
    var job_id = req.body.data.job_id;
    var resultArray = [];

    if(job_id && job_id !=''){

        // var job_details = await sequelize.query("SELECT jobs.* , companies.company_name, companies.category, companies.logo, categories.title as category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
        // var job_details = await sequelize.query("SELECT jobs.* , companies.company_name, companies.company_size, companies.company_email, companies.category, companies.logo, categories.title as company_category_title, (SELECT categories.title FROM categories WHERE categories.category_id = jobs.job_category) as job_category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id where jobs.job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
        var job_details = await sequelize.query("SELECT jobs.* , job_opening_status.title as job_opening_status_title, companies.company_name, companies.company_size, companies.company_email, companies.category, companies.logo, company_contact_info.website, categories.title as company_category_title, (SELECT categories.title FROM categories WHERE categories.category_id = jobs.job_category) as job_category_title from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id LEFT JOIN categories ON companies.category = categories.category_id LEFT JOIN company_contact_info ON jobs.company_id = company_contact_info.company_id LEFT JOIN job_opening_status ON jobs.job_opening_status = job_opening_status.job_opening_status_id where jobs.job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
        if(job_details.length > 0){
            var i = 0;
            job_details.forEach(function(element,index){ 

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                resultArray.push({
                    "job_id": element.job_id,
                    "job_opening_status": element.job_opening_status,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "date_opened": element.date_opened,
                    "target_date": element.target_date,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "job_category": element.job_category,
                    "job_category_title": element.job_category_title,
                    "person_gender": element.person_gender,
                    "person_nationality": element.person_nationality,
                    "person_work_exp": element.person_work_exp,
                    "person_language": element.person_language,
                    "person_salary": element.person_salary,
                    "person_city": element.person_city,
                    "healthcare_license": element.healthcare_license,
                    "licenses_number": element.licenses_number,
                    "company_id": element.company_id,
                    "client_id": element.client_id,
                    "currency": element.currency,
                    // "client_name": element.client_name,
                    // "client_contact_name": element.client_contact_name,
                    // "client_address": element.client_address,
                    // "client_country": element.client_country,
                    // "client_state": element.client_state,
                    // "client_city": element.client_city,
                    "job_description": element.job_description,
                    "requirements": element.requirements,
                    "skill_set": element.skill_set,
                    "no_of_positions": element.no_of_positions,
                    "responsibilities": element.responsibilities,
                    "min_qualification": element.min_qualification,
                    "status": element.status,
                    "company_name": element.company_name,
                    "company_size": element.company_size,
                    "company_email": element.company_email,
                    "company_website": element.website,
                    "category": element.category,
                    "company_logo": company_logo,
                    "company_category_title": element.company_category_title,
                    "job_opening_status_title": element.job_opening_status_title
                });    
                i++;             
            });
            res.status(200).send({ status:200, success: true, company_details:resultArray[0] });
        }else{
            res.status(200).json({ status:200, success: false, message: "job not found"});
        }
    }else{
        res.status(200).json({ status:200, success: false, message: "Job id is required!"});
    }  
}

/************************* client job details ends *******************************/


/************************* candidate favourite job start *******************************/

exports.candidate_favourite_job_upload = async function(req, res, next) {
    
    var job_id = req.body.data.job_id;
    var candidate_id = req.body.data.candidate_id;

    if(job_id && job_id !='' && candidate_id && candidate_id !=''){

        var favourite_job = await sequelize.query("SELECT candidate_favourite_job.* from candidate_favourite_job where candidate_favourite_job.candidate_id ="+candidate_id+" and candidate_favourite_job.job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
        if(favourite_job.length > 0){

            models.candidate_favourite_job.destroy({ 
                where:{candidate_favourite_job_id:favourite_job[0].candidate_favourite_job_id}
            }).then(function(value) {
                res.status(200).send({ status:200, success: true, message: "Job successfully deleted from your favourite list" });
            });
            
        }else{

            models.candidate_favourite_job.create({ 

                candidate_id: candidate_id,
                job_id: job_id,

            }).then(function(favourite_job) {

            res.status(200).send({ status:200, success: true, message: "Job successfully added to your favourite list" });
            })
            
        }
        
    }else{
        res.status(200).json({ status:200, success: false, message: "All fields are required!"});
    }  
}

/************************* candidate favourite job ends *******************************/

/************************* favourite job listing for client start *******************************/

exports.client_favourite_job_list = async function(req, res, next) {

    var client_id = req.body.data.client_id;
    var resultArray = [];

    if(client_id && client_id !='' ){
        console.log('11111111111111');

        // SELECT jobs.*, companies.company_name, companies.category, companies.logo from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id, (SELECT client_favourite_job.job_id, CASE WHEN client_favourite_job_id.job_id = jobs.job_id  THEN "yes" ELSE "no" END as favourite_job FROM client_favourite_job WHERE client_favourite_job.client_id = 2) where jobs.status = 'publish' order by jobs.job_id DESC*/
//         select a.`job_title`, b.company_name, If(c.client_favourite_job_id != 'NULL', "yes","No") 'favourite' from jobs as a
// left join companies as b on b.company_id = a.company_id
// left join client_favourite_job as c on c.job_id = a.job_id and c.client_id=2
// where `job_category` = 1
        

        // var client_job_listing = await sequelize.query("SELECT jobs.*, companies.company_name, companies.category, companies.logo from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id where jobs.client_id ="+client_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        var client_job_listing = await sequelize.query("SELECT jobs.*, companies.company_name, companies.category, companies.logo, If(client_favourite_job.client_favourite_job_id != 'NULL', 'yes','No') 'favourite' from jobs LEFT JOIN companies ON jobs.company_id = companies.company_id left join client_favourite_job on client_favourite_job.job_id = jobs.job_id and client_favourite_job.client_id="+client_id+" where jobs.client_id ="+client_id+" and jobs.status = 'publish' order by jobs.job_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        var client_favourite_job = await sequelize.query("SELECT client_favourite_job.* from client_favourite_job where client_favourite_job.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

        if(client_job_listing.length > 0){
            console.log('222222222222222');
            var i = 0;
            client_job_listing.forEach(function(element,index){ 
                console.log('3333333333333333');

                if(element.logo !='' && element.logo!= null){
                    var company_logo = req.app.locals.baseurl+element.logo;
                }else{
                    var company_logo = req.app.locals.baseurl+'contents/no_image_available_redford.jpg';
                }

                // var favourite_job = '';

                // for( var j =0; j<=client_favourite_job.length; j++){
                //     console.log('job id'+element.job_id);
                //     console.log('4444444444444444444');
                //     console.log('favourit job id'+client_favourite_job[j].job_id);
                //     if(element.job_id == client_favourite_job[j].job_id){
                //         var favourite_job = 'yes';
                //     }else{
                //         var favourite_job = 'no';
                //     }
                // }

                // client_favourite_job.forEach(function(element1,index){ 
                //     if(element.job_id == element1.job_id){
                //         var favourite_job = 'yes';
                //     }else{
                //         var favourite_job = 'no';
                //     }
                // });
                console.log('55555555555555555555');

                resultArray.push({
                    "job_id": element.job_id,
                    "job_opening_status": element.job_opening_status,
                    "job_title": element.job_title,
                    "job_type": element.job_type,
                    "job_category": element.job_category,
                    "job_category_title": element.job_category_title,
                    "date_opened": element.date_opened,
                    "target_date": element.target_date,
                    "basic_info_cuntry": element.basic_info_cuntry,
                    "basic_info_state": element.basic_info_state,
                    "basic_info_city": element.basic_info_city,
                    "company_name": element.company_name,
                    "category": element.category,
                    "company_logo": company_logo,
                    "favourite_job": element.favourite
                });    
                i++;   
            // });          
            });
            res.status(200).send({ status:200, success: true, company_details:resultArray });
        }else{
            res.status(200).json({ status:200, success: false, message: "job not found"});
        }

        
        // var client_favourite_job_listing = await sequelize.query("SELECT client_favourite_job.* from client_favourite_job where client_favourite_job.client_id ="+client_id,{ type: Sequelize.QueryTypes.SELECT });

        // if(client_favourite_job_listing.length > 0){

        //     res.status(200).send({ status:200, success: true, client_favourite_job_listing: client_favourite_job_listing });
        // }else{
        //     res.status(200).json({ status:200, success: false, message: "Favourit job not found for this client!"});
        // }

    }else{
        res.status(200).json({ status:200, success: false, message: "Client id is required!"});
    }    
}


/************************* favourite job listing for client ends *******************************/


/************************* job details with out login start *******************************/

exports.job_details_with_out_login = async function(req, res, next) {

    var job_id = req.body.data.job_id;

    if(job_id && job_id !=''){

        var job_details = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });

        res.status(200).send({ status:200, success: true, job_details:job_details });

    }else{
        res.status(200).json({ status:200, success: false, message: "Job id is required!"});
    }  

}

/************************* job details with out login ends *******************************/


/************************* delete job start *******************************/

exports.job_delete = function(req, res, next) {
    console.log(req.body.data);
    var job_id = req.body.data.job_id;

    if(job_id && job_id !='' ){

        models.jobs.destroy({ 
            where:{job_id:job_id}
        }).then(function(value) {
            res.status(200).send({ success: true, message: "Job successfully deleted" });
        });

    }else{
        res.status(200).json({ success: false, message: "Job id is required!"});
    } 
}

/************************* delete job ends *******************************/



/************************* shortlisted candidate list start *******************************/
exports.shortlisted_candidate_list = async function(req, res, next) {

    var job_id = req.body.data.job_id;
    //var user_id = req.body.data.user_id;
    //var user_type = req.body.data.user_type;
    var resultArray = [];
    var job_details = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
    var shortlisted_candidate_list = await sequelize.query("SELECT candidate_looking_for.candidate_id, candidate_looking_for.category as candidate_category, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, (SELECT SUM(experience_in_year) FROM `candidate_experience_details` WHERE `candidate_id` = candidate_looking_for.candidate_id) as total_experience, categories.title as candidate_category_title, (SELECT GROUP_CONCAT(candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = candidate_looking_for.candidate_id) as candidate_skill FROM `candidate_looking_for` LEFT JOIN candidates ON candidate_looking_for.candidate_id = candidates.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id WHERE candidate_looking_for.category = "+job_details[0].job_category+" AND candidates.candidate_id NOT IN (SELECT candidate_id FROM assigned_job_to_candidates WHERE job_id = "+job_id+ ")",{ type: Sequelize.QueryTypes.SELECT });
                                      
    if(shortlisted_candidate_list.length > 0 ){
        console.log('2222222222222222222222');

        var i = 0;
        shortlisted_candidate_list.forEach(function(element,index){ 

            if(element.first_name !='' && element.first_name!= null){
                var candidate_name = element.first_name+' '+element.last_name;
            }else if(element.name !='' && element.name!= null){
                var candidate_name = element.name;
            }else{
                var candidate_name = '';
            }

            if(element.profile_picture !='' && element.profile_picture!= null){
                var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
            }else if(element.provider_image !='' && element.provider_image!= null){
                var candidate_profile_picture = element.provider_image;
            }else{
                var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
            }

            resultArray.push({
                "candidate_id":element.candidate_id,
                "candidate_category":element.candidate_category,
                "candidate_category_title":element.candidate_category_title,
                "name":candidate_name,
                "candidate_picture":candidate_profile_picture,
                "total_experience":element.total_experience,
                "candidate_skill": element.candidate_skill
            });    
            i++;             
        }); 

        res.status(200).send({ status:200, success: true, job_details:job_details, shortlisted_candidate_list:resultArray });  
    } else {
        res.status(200).send({ status:200, success: true, job_details:job_details, shortlisted_candidate_list:resultArray }); 
    }
}
/************************* shortlisted candidate list ends *******************************/



/************************* assigned candidate list start *******************************/
// exports.assigned_candidate_list = async function(req, res, next) {

//     var job_id = req.body.data.job_id;
//     var resultArray = [];
//     var job_details = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
//     // var shortlisted_candidate_list = await sequelize.query("SELECT assigned_job_to_candidates.*, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, (SELECT SUM(experience_in_year) FROM `candidate_experience_details` WHERE `candidate_id` = assigned_job_to_candidates.candidate_id) as total_experience, candidate_looking_for.category candidate_category, categories.title as candidate_category_title, (SELECT GROUP_CONCAT(DISTINCT candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = assigned_job_to_candidates.candidate_id) as candidate_skill FROM `assigned_job_to_candidates` LEFT JOIN candidates ON assigned_job_to_candidates.candidate_id = candidates.candidate_id LEFT JOIN candidate_looking_for ON assigned_job_to_candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where assigned_job_to_candidates.job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
//     var shortlisted_candidate_list = await sequelize.query("SELECT assigned_job_to_candidates.*, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, (SELECT SUM(experience_in_year) FROM `candidate_experience_details` WHERE `candidate_id` = assigned_job_to_candidates.candidate_id) as total_experience, candidate_looking_for.category candidate_category, categories.title as candidate_category_title, (SELECT GROUP_CONCAT(DISTINCT candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = assigned_job_to_candidates.candidate_id) as candidate_skill, candidate_job_opening_status.title as candidate_job_opening_status_title, candidate_job_opening_sub_status.title as candidate_job_opening_sub_status_title FROM `assigned_job_to_candidates` LEFT JOIN candidates ON assigned_job_to_candidates.candidate_id = candidates.candidate_id LEFT JOIN candidate_looking_for ON assigned_job_to_candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id LEFT JOIN candidate_job_opening_status ON assigned_job_to_candidates.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON assigned_job_to_candidates.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id where assigned_job_to_candidates.job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
   
//     if(shortlisted_candidate_list.length > 0 ){
//         console.log('2222222222222222222222');

//         var i = 0;
//         shortlisted_candidate_list.forEach(function(element,index){ 

//             if(element.first_name !='' && element.first_name!= null){
//                 var candidate_name = element.first_name+' '+element.last_name;
//             }else if(element.name !='' && element.name!= null){
//                 var candidate_name = element.name;
//             }else{
//                 var candidate_name = '';
//             }

//             if(element.profile_picture !='' && element.profile_picture!= null){
//                 var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
//             }else if(element.provider_image !='' && element.provider_image!= null){
//                 var candidate_profile_picture = element.provider_image;
//             }else{
//                 var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
//             }

//             resultArray.push({
//                 "assigned_job_to_candidates_id":element.assigned_job_to_candidates_id,
//                 "candidate_id":element.candidate_id,
//                 "job_id":element.job_id,
//                 "candidate_job_opening_status_id":element.candidate_job_opening_status_id,
//                 "candidate_job_opening_sub_status_id":element.candidate_job_opening_sub_status_id,
//                 "candidate_job_opening_status_title":element.candidate_job_opening_status_title,
//                 "candidate_job_opening_sub_status_title":element.candidate_job_opening_sub_status_title,
//                 "user_type":element.user_type,
//                 "createdBy":element.createdBy,
//                 "createdAt":element.createdAt,
//                 "candidate_category":element.candidate_category,
//                 "candidate_category_title":element.candidate_category_title,
//                 "name":candidate_name,
//                 "candidate_picture":candidate_profile_picture,
//                 "total_experience":element.total_experience,
//                 "candidate_skill": element.candidate_skill
//             });    
//             i++;             
//         });

//         res.status(200).send({ status:200, success: true, job_details:job_details, candidate_list: resultArray });
//     }else{
//         res.status(200).send({ status:200, success: true, job_details:job_details, candidate_list: resultArray });
//     }
// }


exports.assigned_candidate_list = async function(req, res, next) {

    var job_id = req.body.data.job_id;
    var resultArray = [];
    var job_details = await sequelize.query("SELECT jobs.*, categories.title as job_category_title from jobs LEFT JOIN categories ON jobs.job_category = categories.category_id where job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT });
    // var shortlisted_candidate_list = await sequelize.query("SELECT assigned_job_to_candidates.*, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, (SELECT SUM(experience_in_year) FROM `candidate_experience_details` WHERE `candidate_id` = assigned_job_to_candidates.candidate_id) as total_experience, candidate_looking_for.category candidate_category, categories.title as candidate_category_title, (SELECT GROUP_CONCAT(DISTINCT candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = assigned_job_to_candidates.candidate_id) as candidate_skill FROM `assigned_job_to_candidates` LEFT JOIN candidates ON assigned_job_to_candidates.candidate_id = candidates.candidate_id LEFT JOIN candidate_looking_for ON assigned_job_to_candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id where assigned_job_to_candidates.job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
    // var shortlisted_candidate_list = await sequelize.query("SELECT assigned_job_to_candidates.*, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, (SELECT SUM(experience_in_year) FROM `candidate_experience_details` WHERE `candidate_id` = assigned_job_to_candidates.candidate_id) as total_experience, candidate_looking_for.category candidate_category, categories.title as candidate_category_title, (SELECT GROUP_CONCAT(DISTINCT candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = assigned_job_to_candidates.candidate_id) as candidate_skill, candidate_job_opening_status.title as candidate_job_opening_status_title, candidate_job_opening_sub_status.title as candidate_job_opening_sub_status_title FROM `assigned_job_to_candidates` LEFT JOIN candidates ON assigned_job_to_candidates.candidate_id = candidates.candidate_id LEFT JOIN candidate_looking_for ON assigned_job_to_candidates.candidate_id = candidate_looking_for.candidate_id LEFT JOIN categories ON candidate_looking_for.category = categories.category_id LEFT JOIN candidate_job_opening_status ON assigned_job_to_candidates.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON assigned_job_to_candidates.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id where assigned_job_to_candidates.job_id = "+job_id,{ type: Sequelize.QueryTypes.SELECT });
    
    var shortlisted_candidate_list = await sequelize.query("SELECT assigned_job_to_candidates.*, candidates.name, candidates.first_name, candidates.last_name, candidates.provider_image, candidates.profile_picture, "+
    "(SELECT SUM(experience_in_year) FROM `candidate_experience_details` WHERE `candidate_id` = assigned_job_to_candidates.candidate_id) as total_experience, candidate_looking_for.category candidate_category, categories.title as candidate_category_title, "+ 
    "(SELECT GROUP_CONCAT(DISTINCT candidate_skill.`skill`) FROM candidate_skill WHERE candidate_skill.candidate_id = assigned_job_to_candidates.candidate_id) as candidate_skill, candidate_job_opening_status.title as candidate_job_opening_status_title, "+
    "candidate_job_opening_sub_status.title as candidate_job_opening_sub_status_title,candidate_job_opening_sub_status.status as sub_status,candidate_job_opening_status.status as opening_status FROM `assigned_job_to_candidates` "+
    "LEFT JOIN candidates ON assigned_job_to_candidates.candidate_id = candidates.candidate_id "+
    "LEFT JOIN candidate_looking_for ON assigned_job_to_candidates.candidate_id = candidate_looking_for.candidate_id "+
    "LEFT JOIN categories ON candidate_looking_for.category = categories.category_id "+ 
    "LEFT JOIN candidate_job_opening_status ON assigned_job_to_candidates.candidate_job_opening_status_id =candidate_job_opening_status.candidate_job_opening_status_id "+
    "LEFT JOIN candidate_job_opening_sub_status ON assigned_job_to_candidates.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id where assigned_job_to_candidates.job_id ="+job_id,{ type: Sequelize.QueryTypes.SELECT })
   
    // console.log("_____________----------------___________"+shortlisted_candidate_list[0].sub_status);
    // console.log("_____________----------------___________"+shortlisted_candidate_list[0].opening_status);
    if(shortlisted_candidate_list.length > 0 ){
        console.log('2222222222222222222222');

        var i = 0;
        shortlisted_candidate_list.forEach(function(element,index){ 

            if(element.first_name !='' && element.first_name!= null){
                var candidate_name = element.first_name+' '+element.last_name;
            }else if(element.name !='' && element.name!= null){
                var candidate_name = element.name;
            }else{
                var candidate_name = '';
            }

            if(element.profile_picture !='' && element.profile_picture!= null){
                var candidate_profile_picture = req.app.locals.baseurl+element.profile_picture;
            }else if(element.provider_image !='' && element.provider_image!= null){
                var candidate_profile_picture = element.provider_image;
            }else{
                var candidate_profile_picture = req.app.locals.baseurl+'contents/no_image_redford.jpg';
            }

            if(element.opening_status != 'active'){
                element.candidate_job_opening_status_id ='';               
                element.candidate_job_opening_status_title ='';                
            }

            if(element.sub_status !='active'){
                element.candidate_job_opening_sub_status_id ='';
                element.candidate_job_opening_sub_status_title ='';
            }



            resultArray.push({
                "assigned_job_to_candidates_id":element.assigned_job_to_candidates_id,
                "candidate_id":element.candidate_id,
                "job_id":element.job_id,
                "candidate_job_opening_status_id":element.candidate_job_opening_status_id,
                "candidate_job_opening_sub_status_id":element.candidate_job_opening_sub_status_id,
                "candidate_job_opening_status_title":element.candidate_job_opening_status_title,
                "candidate_job_opening_sub_status_title":element.candidate_job_opening_sub_status_title,
                "user_type":element.user_type,
                "createdBy":element.createdBy,
                "createdAt":element.createdAt,
                "candidate_category":element.candidate_category,
                "candidate_category_title":element.candidate_category_title,
                "name":candidate_name,
                "candidate_picture":candidate_profile_picture,
                "total_experience":element.total_experience,
                "candidate_skill": element.candidate_skill
            });    
            i++;             
        });

        res.status(200).send({ status:200, success: true, job_details:job_details, candidate_list: resultArray });
    }else{
        res.status(200).send({ status:200, success: true, job_details:job_details, candidate_list: resultArray });
    }
}

/************************* assigned candidate list ends *******************************/


/************************* assigned candidate delete start *******************************/

exports.assigned_candidate_delete = function(req, res, next) {
    console.log(req.body.data);
    var assigned_job_to_candidates_id = req.body.data.assigned_job_to_candidates_id;

    if(assigned_job_to_candidates_id && assigned_job_to_candidates_id !='' ){

        models.assigned_job_to_candidates.destroy({ 
            where:{assigned_job_to_candidates_id:assigned_job_to_candidates_id}
        }).then(function(value) {
            res.status(200).send({ status:200, success: true, message: "Assigned candidate successfully deleted" });
        });

    }else{
        res.status(200).json({ status:200, success: false, message: "Assigned candidate id is required!"});
    } 
}

/************************* assigned candidate delete ends *******************************/


/************************* assigned candidate upload start *******************************/

exports.assigned_candidate_upload = async function(req, res, next) {
    console.log(req.body.data);

    var job_id = req.body.data.job_id;
    var user_id = req.body.data.user_id;
    var user_type = req.body.data.user_type;
    var candidate_id = req.body.data.candidate_id;

    // var cand_id = req.body.data.candidate_id.split(",");
    console.log('1111111111111111111111');
    console.log(candidate_id);

    var candidate_default_job_opening_status = await sequelize.query("SELECT * FROM `candidate_job_opening_status` where status = 'active' ORDER BY sequence ASC limit 1",{ type: Sequelize.QueryTypes.SELECT });
    if(candidate_default_job_opening_status.length > 0 ){
        var candidate_job_opening_status_id = candidate_default_job_opening_status[0].candidate_job_opening_status_id;
        var candidate_default_job_opening_sub_status = await sequelize.query("SELECT * FROM `candidate_job_opening_sub_status` where candidate_job_opening_status_id = "+candidate_default_job_opening_status[0].candidate_job_opening_status_id+" and status = 'active' ORDER BY sequence ASC limit 1",{ type: Sequelize.QueryTypes.SELECT });
        if(candidate_default_job_opening_sub_status.length > 0 ){
            var candidate_job_opening_sub_status_id = candidate_default_job_opening_sub_status[0].candidate_job_opening_sub_status_id;
        }else{
            var candidate_job_opening_sub_status_id = ''
        }
    }else{
        var candidate_job_opening_status_id = '';
        var candidate_job_opening_sub_status_id = '';
    }
    if(candidate_id.length > 0 ){

        if(job_id && job_id !='' && user_id && user_id !='' && user_type && user_type !='' ){

            var i=0;                       
            candidate_id.forEach(function(cid){
                models.assigned_job_to_candidates.create({
                    candidate_id:candidate_id[i],
                    job_id: job_id,
                    user_type: user_type,
                    createdBy: user_id,
                    candidate_job_opening_status_id: candidate_job_opening_status_id,
                    candidate_job_opening_sub_status_id : candidate_job_opening_sub_status_id,                            
                });

                models.candidate_status_log.create({
                    candidate_id:candidate_id[i],
                    job_id: job_id,
                    user_type: user_type,
                    createdBy: user_id,
                    candidate_job_opening_status_id: candidate_job_opening_status_id,
                    candidate_job_opening_sub_status_id : candidate_job_opening_sub_status_id,                            
                });

                i++;		
            }, this);

            res.status(200).send({ status:200, success: true, message: "Candidate successfully assigned for this job" });
            
        }else{
            res.status(200).json({ status:200, success: false, message: "All fileds are required!"});
        } 
    }else{
        res.status(200).json({ status:200, success: false, message: "No candidate assigned!"});
    } 
}

/************************* assigned candidate upload ends *******************************/



/************************* job opening status dropdown in job posting form api start *******************************/

exports.job_opening_status_in_dropdown = async function(req, res, next) {

    var job_opening_status_list = await sequelize.query("SELECT job_opening_status_id, title FROM job_opening_status Where status='active' order by sequence ASC ",{ type: Sequelize.QueryTypes.SELECT });
    
    if(job_opening_status_list.length > 0){
        res.status(200).send({ status:200, success: true, job_opening_status_list: job_opening_status_list});
    }else{
        res.status(200).send({  status:200, success: false, message: "No job opening status found" });
    }
}

/************************* job opening status dropdown in job posting form api ends *******************************/




/************************* assigned candidate update start *******************************/

exports.assigned_candidate_update = async function(req, res, next) {
    console.log(req.body.data);

    var user_id = req.body.data.user_id;
    var user_type = req.body.data.user_type;
    var candidate_job_opening_status_id = req.body.data.candidate_job_opening_status_id;
    var candidate_job_opening_sub_status_id = req.body.data.candidate_job_opening_sub_status_id;
    var assigned_job_to_candidates_id = req.body.data.assigned_job_to_candidates_id;

    if(user_id && user_id != '' && user_type && user_type != '' && assigned_job_to_candidates_id && assigned_job_to_candidates_id != '' && candidate_job_opening_status_id && candidate_job_opening_status_id !='' && candidate_job_opening_sub_status_id && candidate_job_opening_sub_status_id !='' ){

        var assigned_job_to_candidates_details = await sequelize.query("SELECT * FROM assigned_job_to_candidates Where assigned_job_to_candidates_id = "+assigned_job_to_candidates_id,{ type: Sequelize.QueryTypes.SELECT });

        models.assigned_job_to_candidates.update({
            candidate_job_opening_status_id:candidate_job_opening_status_id,
            candidate_job_opening_sub_status_id: candidate_job_opening_sub_status_id,
            user_type: user_type,
            updatedBy: user_id,                            
        },{where:{assigned_job_to_candidates_id:assigned_job_to_candidates_id}}).then(function(assigned_job_to_candidates) {

            if(candidate_job_opening_status_id == assigned_job_to_candidates_details[0].candidate_job_opening_status_id && candidate_job_opening_sub_status_id == assigned_job_to_candidates_details[0].candidate_job_opening_sub_status_id){
               
            }else{
                models.candidate_status_log.create({
                    candidate_id:assigned_job_to_candidates_details[0].candidate_id,
                    job_id: assigned_job_to_candidates_details[0].job_id,
                    candidate_job_opening_status_id:candidate_job_opening_status_id,
                    candidate_job_opening_sub_status_id: candidate_job_opening_sub_status_id,
                    user_type: user_type,
                    createdBy: user_id,                            
                });
            }

            res.status(200).send({ status:200, success: true, message: "Candidate status updated successfully" });
        })
    }else{
        res.status(200).json({ status:200, success: false, message: "all fields are required!"});
    } 
}

/************************* assigned candidate update ends *******************************/



/************************* assigned candidate details start *******************************/

// exports.assigned_candidate_details = async function(req, res, next) {
//     console.log(req.body.data);

//     var assigned_job_to_candidates_id = req.body.data.assigned_job_to_candidates_id;

//     if(assigned_job_to_candidates_id && assigned_job_to_candidates_id !='' ){
        
//         var assigned_job_to_candidates_details = await sequelize.query("SELECT * FROM `assigned_job_to_candidates` where assigned_job_to_candidates_id = "+assigned_job_to_candidates_id,{ type: Sequelize.QueryTypes.SELECT });
                                        
//         if(assigned_job_to_candidates_details.length > 0) {
//             res.status(200).send({ status:200, success: true, assigned_job_to_candidates_details:assigned_job_to_candidates_details[0] });  
//         } else {
//             res.status(200).send({ status:200, success: false, message: "No assigned job to candidate found!" }); 
//         }

//     }else{
//         res.status(200).json({ status:200, success: false, message: "Assigned job to candidates id is required!"});
//     } 
// }



exports.assigned_candidate_details = async function(req, res, next) {
    console.log(req.body.data);

    var assigned_job_to_candidates_id = req.body.data.assigned_job_to_candidates_id;

    if(assigned_job_to_candidates_id && assigned_job_to_candidates_id !='' ){
        
        var assigned_job_to_candidates_details = await sequelize.query("SELECT * FROM `assigned_job_to_candidates` where assigned_job_to_candidates_id = "+assigned_job_to_candidates_id,{ type: Sequelize.QueryTypes.SELECT });
        var candidate_job_opening_status = await models.candidate_job_opening_status.findAll({where:{candidate_job_opening_status_id:assigned_job_to_candidates_details[0].candidate_job_opening_status_id},attributes:['status']});
        var candidate_job_opening_sub_status_id = await models.candidate_job_opening_sub_status.findAll({where:{candidate_job_opening_sub_status_id:assigned_job_to_candidates_details[0].candidate_job_opening_sub_status_id},attributes:['status']});
      
        console.log("candidate_job_opening_status:" + candidate_job_opening_status[0].status +", candidate_job_opening_sub_status_id:"+candidate_job_opening_sub_status_id[0].status);
        
        if(candidate_job_opening_status[0].status !='active'){
            assigned_job_to_candidates_details[0].candidate_job_opening_status_id='';
        }

        if(candidate_job_opening_sub_status_id[0].status !='active'){
            assigned_job_to_candidates_details[0].candidate_job_opening_sub_status_id='';
        }

        if(assigned_job_to_candidates_details.length > 0) {
            res.status(200).send({ status:200, success: true, assigned_job_to_candidates_details:assigned_job_to_candidates_details[0] });  
        } else {
            res.status(200).send({ status:200, success: false, message: "No assigned job to candidate found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Assigned job to candidates id is required!"});
    } 
}

/************************* assigned candidate details ends *******************************/



/************************* job status changed_history start *******************************/

exports.job_status_changed_history = async function(req, res, next) {

    var job_id = req.body.data.job_id;

    if(job_id && job_id !='' ){

        // var job_status_changed_history = await sequelize.query("SELECT job_status_log.*, jobs.job_title, job_opening_status.title as job_opening_status_title, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name)) 'user_name' FROM `job_status_log` LEFT JOIN jobs ON job_status_log.job_id = jobs.job_id LEFT JOIN job_opening_status ON job_status_log.job_status = job_opening_status.job_opening_status_id LEFT JOIN client_basic_info ON job_status_log.createdBy = client_basic_info.client_id where job_status_log.job_id = "+job_id+" ORDER BY job_status_log.job_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var job_status_changed_history = await sequelize.query("SELECT job_status_log.*, jobs.job_title, job_opening_status.title as job_opening_status_title, IF(job_status_log.user_type = 'admin', admins.name, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name))) 'user_name' FROM `job_status_log` LEFT JOIN jobs ON job_status_log.job_id = jobs.job_id LEFT JOIN job_opening_status ON job_status_log.job_status = job_opening_status.job_opening_status_id LEFT JOIN client_basic_info ON job_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON job_status_log.createdBy = admins.admin_id where job_status_log.job_id = "+job_id+" ORDER BY job_status_log.job_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        var job_status_changed_history = await sequelize.query("SELECT job_status_log.*, jobs.job_title, job_opening_status.title as job_opening_status_title, IF(job_status_log.user_type = 'admin', 'Admin', IF(job_status_log.user_type = 'client', 'Employer', IF(job_status_log.user_type = 'candidate', 'Candidate', ' '))) 'created_by_type', IF(job_status_log.user_type = 'admin', admins.name, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name))) 'user_name' FROM `job_status_log` LEFT JOIN jobs ON job_status_log.job_id = jobs.job_id LEFT JOIN job_opening_status ON job_status_log.job_status = job_opening_status.job_opening_status_id LEFT JOIN client_basic_info ON job_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON job_status_log.createdBy = admins.admin_id where job_status_log.job_id = "+job_id+" ORDER BY job_status_log.job_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                                        
        if(job_status_changed_history.length > 0) {
            res.status(200).send({ status:200, success: true, job_status_changed_history:job_status_changed_history });  
        } else {
            res.status(200).send({ status:200, success: false, message: "No job status changed history found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Job id is required!"});
    } 
}

/************************* job status changed history ends *******************************/



/************************* candidate status changed_history start *******************************/

exports.candidate_status_changed_history = async function(req, res, next) {

    var candidate_id = req.body.data.candidate_id;

    if(candidate_id && candidate_id !='' ){
       
        // var candidate_status_changed_history = await sequelize.query("SELECT job_status_log.*, jobs.job_title, job_opening_status.title as job_opening_status_title, IF(job_status_log.user_type = 'admin', admins.name, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name))) 'user_name' FROM `job_status_log` LEFT JOIN jobs ON job_status_log.job_id = jobs.job_id LEFT JOIN job_opening_status ON job_status_log.job_status = job_opening_status.job_opening_status_id LEFT JOIN client_basic_info ON job_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON job_status_log.createdBy = admins.admin_id where job_status_log.job_id = "+candidate_id+" ORDER BY job_status_log.job_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var candidate_status_changed_history = await sequelize.query("SELECT assigned_job_to_candidates.*, jobs.job_title, candidate_job_opening_status.title as status_title, candidate_job_opening_sub_status.title as sub_status_title FROM `assigned_job_to_candidates` LEFT JOIN jobs ON assigned_job_to_candidates.job_id = jobs.job_id LEFT JOIN candidate_job_opening_status ON assigned_job_to_candidates.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON assigned_job_to_candidates.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id WHERE assigned_job_to_candidates.candidate_id = 15 ORDER BY assigned_job_to_candidates.assigned_job_to_candidates_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_status_changed_history = await sequelize.query("SELECT candidate_status_log.*, jobs.job_title, candidate_job_opening_status.title as status_title, candidate_job_opening_sub_status.title as sub_status_title, IF(candidate_status_log.user_type = 'admin', 'Admin', IF(candidate_status_log.user_type = 'client', 'Employer', IF(candidate_status_log.user_type = 'candidate', 'Candidate', ' '))) 'created_by_type', IF(candidate_status_log.user_type = 'admin', admins.name, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name))) 'user_name' FROM `candidate_status_log` LEFT JOIN jobs ON candidate_status_log.job_id = jobs.job_id LEFT JOIN candidate_job_opening_status ON candidate_status_log.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON candidate_status_log.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id LEFT JOIN client_basic_info ON candidate_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON candidate_status_log.createdBy = admins.admin_id WHERE candidate_status_log.candidate_id = "+candidate_id+" ORDER BY candidate_status_log.candidate_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                               
        if(candidate_status_changed_history.length > 0) {
            res.status(200).send({ status:200, success: true, candidate_status_changed_history:candidate_status_changed_history });  
        } else {
            res.status(200).send({ status:200, success: false, message: "No candidate status changed history found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "candidate id is required!"});
    } 
}

/************************* candidate status changed history ends *******************************/


/************************* candidate status changed_history start *******************************/

exports.candidate_job_application_status_history = async function(req, res, next) {

    var candidate_id = req.body.data.candidate_id;
    var job_id = req.body.data.job_id;

    if(candidate_id && candidate_id !='' && job_id && job_id !=''){
       
        // var candidate_status_changed_history = await sequelize.query("SELECT job_status_log.*, jobs.job_title, job_opening_status.title as job_opening_status_title, IF(job_status_log.user_type = 'admin', admins.name, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name))) 'user_name' FROM `job_status_log` LEFT JOIN jobs ON job_status_log.job_id = jobs.job_id LEFT JOIN job_opening_status ON job_status_log.job_status = job_opening_status.job_opening_status_id LEFT JOIN client_basic_info ON job_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON job_status_log.createdBy = admins.admin_id where job_status_log.job_id = "+candidate_id+" ORDER BY job_status_log.job_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var candidate_status_changed_history = await sequelize.query("SELECT assigned_job_to_candidates.*, jobs.job_title, candidate_job_opening_status.title as status_title, candidate_job_opening_sub_status.title as sub_status_title FROM `assigned_job_to_candidates` LEFT JOIN jobs ON assigned_job_to_candidates.job_id = jobs.job_id LEFT JOIN candidate_job_opening_status ON assigned_job_to_candidates.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON assigned_job_to_candidates.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id WHERE assigned_job_to_candidates.candidate_id = 15 ORDER BY assigned_job_to_candidates.assigned_job_to_candidates_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var candidate_status_changed_history = await sequelize.query("SELECT candidate_status_log.*, jobs.job_title, candidate_job_opening_status.title as status_title, candidate_job_opening_sub_status.title as sub_status_title, IF(candidate_status_log.user_type = 'admin', admins.name, IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name))) 'user_name' FROM `candidate_status_log` LEFT JOIN jobs ON candidate_status_log.job_id = jobs.job_id LEFT JOIN candidate_job_opening_status ON candidate_status_log.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON candidate_status_log.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id LEFT JOIN client_basic_info ON candidate_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON candidate_status_log.createdBy = admins.admin_id WHERE candidate_status_log.candidate_id = "+candidate_id+" and candidate_status_log.job_id = "+job_id+" ORDER BY candidate_status_log.candidate_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        // var candidate_status_changed_history = await sequelize.query("SELECT candidate_status_log.*, jobs.job_title, candidate_job_opening_status.title as status_title, candidate_job_opening_sub_status.title as sub_status_title, IF(candidate_status_log.user_type = 'admin', admins.name, IF(candidate_status_log.user_type = 'candidate', IF(candidates.name != '', candidates.name, concat(candidates.first_name,' ', candidates.last_name)), IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name)))) 'user_name' FROM `candidate_status_log` LEFT JOIN jobs ON candidate_status_log.job_id = jobs.job_id LEFT JOIN candidate_job_opening_status ON candidate_status_log.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON candidate_status_log.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id LEFT JOIN client_basic_info ON candidate_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON candidate_status_log.createdBy = admins.admin_id LEFT JOIN candidates ON candidate_status_log.createdBy = candidates.candidate_id WHERE candidate_status_log.candidate_id = "+candidate_id+" and candidate_status_log.job_id = "+job_id+" ORDER BY candidate_status_log.candidate_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
        var candidate_status_changed_history = await sequelize.query("SELECT candidate_status_log.*, jobs.job_title, candidate_job_opening_status.title as status_title, candidate_job_opening_sub_status.title as sub_status_title, IF(candidate_status_log.user_type = 'admin', 'Admin', IF(candidate_status_log.user_type = 'client', 'Employer', IF(candidate_status_log.user_type = 'candidate', 'Candidate', ' '))) 'created_by_type', IF(candidate_status_log.user_type = 'admin', admins.name, IF(candidate_status_log.user_type = 'candidate', IF(candidates.name != '', candidates.name, concat(candidates.first_name,' ', candidates.last_name)), IF(client_basic_info.name != '', client_basic_info.name, concat(client_basic_info.first_name,' ', client_basic_info.last_name)))) 'user_name' FROM `candidate_status_log` LEFT JOIN jobs ON candidate_status_log.job_id = jobs.job_id LEFT JOIN candidate_job_opening_status ON candidate_status_log.candidate_job_opening_status_id = candidate_job_opening_status.candidate_job_opening_status_id LEFT JOIN candidate_job_opening_sub_status ON candidate_status_log.candidate_job_opening_sub_status_id = candidate_job_opening_sub_status.candidate_job_opening_sub_status_id LEFT JOIN client_basic_info ON candidate_status_log.createdBy = client_basic_info.client_id LEFT JOIN admins ON candidate_status_log.createdBy = admins.admin_id LEFT JOIN candidates ON candidate_status_log.createdBy = candidates.candidate_id WHERE candidate_status_log.candidate_id = "+candidate_id+" and candidate_status_log.job_id = "+job_id+" ORDER BY candidate_status_log.candidate_status_log_id DESC",{ type: Sequelize.QueryTypes.SELECT });
                               
        if(candidate_status_changed_history.length > 0) {
            res.status(200).send({ status:200, success: true, candidate_status_changed_history:candidate_status_changed_history });  
        } else {
            res.status(200).send({ status:200, success: false, message: "No candidate job status changed history found!" }); 
        }

    }else{
        res.status(200).json({ status:200, success: false, message: "Candidate id and job id is required!"});
    } 
}

/************************* candidate status changed history ends *******************************/


