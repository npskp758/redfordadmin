/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('site_settings', {
    site_setting_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    site_name: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    mobile_no: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    app_version: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    site_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    site_logo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'site_settings'
  });
};
