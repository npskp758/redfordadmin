module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_address_info', {
    cand_address_info_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: false,
    },
    // candidate_name:{
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // father_name:{
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    address:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    city: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    state: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    country: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    zip_code:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    // phn_no_country_code:{
    //     type: DataTypes.STRING(50),
    //     allowNull: true
    // },
    // phone_no:{
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // alt_no_country_code:{
    //     type: DataTypes.STRING(50),
    //     allowNull: true
    // },
    // alternate_no:{
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // email_id:{
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // alternate_email_id:{
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    website:{
        type: DataTypes.STRING(30),
        allowNull: true
    }, 
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_address_info'
    });
  };
  