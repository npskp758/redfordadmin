module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_job_opening_sub_status', {
    candidate_job_opening_sub_status_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_job_opening_status_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    sequence:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
        defaultValue: 0
    },
    status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_job_opening_sub_status'
    });
  };
  