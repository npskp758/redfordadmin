module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_contact_details', {
    cand_contact_details_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false
    },
    // country_code: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    mob_country_code: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    mobile: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    alt_mob_country_code:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    alter_mobile:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    email:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    alter_email:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    phone:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    skype_id:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    twitter:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
    linkedin:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    intagram:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    facebook:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    whatsapp:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
     
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_contact_details'
    });
  };
  