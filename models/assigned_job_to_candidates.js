module.exports = function(sequelize, DataTypes) {
    return sequelize.define('assigned_job_to_candidates', {
    assigned_job_to_candidates_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    job_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    candidate_job_opening_status_id: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    candidate_job_opening_sub_status_id: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    user_type: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'assigned_job_to_candidates'
    });
  };
  