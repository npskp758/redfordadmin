module.exports = function(sequelize, DataTypes) {
    return sequelize.define('company_type', {
    company_type_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
    type:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
    status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
      },
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
    updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
      },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
      }    
    }, {
      tableName: 'company_type'
    });
};
  