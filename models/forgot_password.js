module.exports = function(sequelize, DataTypes) {
    return sequelize.define('forgot_password', {
        forgot_password_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        email:{
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        type:{
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        otp: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }    
    }, {
        tableName: 'forgot_password'
    });
};
  