module.exports = function(sequelize, DataTypes) {
    return sequelize.define('subscribe_list', {
    subscribe_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    email:{
        type: DataTypes.STRING(255),
        allowNull: false,
    },
    status: {
        type: DataTypes.ENUM('Subscribed','Unsubscribed','Unconfirmed','Inactive','Bounced'),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
      updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
        tableName: 'subscribe_list'
    });
};
  