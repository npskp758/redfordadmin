module.exports = function(sequelize, DataTypes) {
    return sequelize.define('email_template', {
    email_template_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    email_template_type: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    subject: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    content: {
        type: DataTypes.TEXT(),
        allowNull: true
    },
    status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
        tableName: 'email_template'
    });
};
  