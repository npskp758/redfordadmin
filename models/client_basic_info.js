module.exports = function(sequelize, DataTypes) {
    return sequelize.define('client_basic_info', {
      client_basic_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      client_id:{
        type: DataTypes.INTEGER(11),
        allowNull: false,
      },
      name:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      first_name:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      middle_name:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      last_name:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      gender:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      nationality:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      alternate_email:{
          type: DataTypes.STRING(255),
          allowNull: true
      },
      mob_country_code:{
        type: DataTypes.STRING(50),
        allowNull: true
      },
      mobile:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      alt_mob_country_code:{
        type: DataTypes.STRING(50),
        allowNull: true
      },
      alternate_mobile:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      fax:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      address: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      city: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      state: {
          type: DataTypes.STRING(255),
          allowNull: true
      },
      country: {
          type: DataTypes.STRING(255),
          allowNull: true
      },
      company_name:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      company_size:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
      company_type:{
        type: DataTypes.STRING(255),
        allowNull: true
      },
       createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
      }    
    }, {
      tableName: 'client_basic_info'
    });
};
  