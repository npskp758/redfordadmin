module.exports = function(sequelize, DataTypes) {
    return sequelize.define('jobs', {
    job_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    job_opening_status: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    job_title: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    job_type:{
        type: DataTypes.ENUM('Temporary','Part time','Full time'),
        allowNull: true
    },
    date_opened: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    target_date: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    basic_info_cuntry: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    basic_info_state: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    basic_info_city: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    job_category: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    person_gender: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    person_nationality: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    person_work_exp: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    person_language: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    person_salary: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    person_city: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    healthcare_license: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    licenses_number: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    company_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    client_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    currency: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    // client_name: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // client_contact_name: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // client_address: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // client_country: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // client_state: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    // client_city: {
    //     type: DataTypes.STRING(255),
    //     allowNull: true
    // },
    job_description:{
        type: DataTypes.TEXT(),
        allowNull: true
    },
    requirements:{
        type: DataTypes.TEXT(),
        allowNull: true
    },
    skill_set:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    no_of_positions:{
        type: DataTypes.STRING(255),
        allowNull: true 
    },
    responsibilities:{
        type: DataTypes.TEXT(),
        allowNull: true
    },
    min_qualification:{
        type: DataTypes.TEXT(),
        allowNull: true
    },
    status: {
        type: DataTypes.ENUM('publish','draft'),
        allowNull: true
    },
    is_verified: {
        type: DataTypes.ENUM('Yes','No'),
        allowNull: true,
        defaultValue: 'No'
    },
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
        tableName: 'jobs'
    });
};
  