module.exports = function(sequelize, DataTypes) {
  return sequelize.define('companies', {
    company_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    client_id:{
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    company_name:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    company_size:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    company_email:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    company_country_code:{
      type: DataTypes.STRING(50),
      allowNull: true
    },
    company_phone:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    category:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    logo:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    company_description:{
      type: DataTypes.TEXT(),
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }    
  }, {
    tableName: 'companies'
  });
};
  