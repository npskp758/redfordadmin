module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company_contact_info', {
    company_contact_info_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    company_id:{
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    address:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    city:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    state:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    country:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    website:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    contact_person_name:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    contact_person_email:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    contact_person_country_code:{
      type: DataTypes.STRING(50),
      allowNull: true
    },
    contact_person_mobile:{
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }    
  }, {
    tableName: 'company_contact_info'
  });
};
  