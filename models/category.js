module.exports = function(sequelize, DataTypes) {
    return sequelize.define('category', {
    category_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true
    },
    logo_image: {
        type: DataTypes.STRING(255),
        allowNull: true,
    },
    bg_image: {
        type: DataTypes.STRING(255),
        allowNull: true,
    },
    status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
      updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
        tableName: 'categories'
    });
};
  