module.exports = function(sequelize, DataTypes) {
    return sequelize.define('contact_us', {
    contact_us_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    company:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    email:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    mobile_country_code:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    mobile:{
        type: DataTypes.STRING(100),
        allowNull: true
    },
    message: {
        type: DataTypes.TEXT(),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
        tableName: 'contact_us'
    });
};
  