module.exports = function(sequelize, DataTypes) {
    return sequelize.define('job_opening_status', {
    job_opening_status_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    sequence:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
        defaultValue: 0
    },
    status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'job_opening_status'
    });
  };
  