module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_language', {
    candidate_language_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: false,
    },
    language:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    fluency:{
        type: DataTypes.STRING(255),
        allowNull: true
    }, 
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_language'
    });
  };
  