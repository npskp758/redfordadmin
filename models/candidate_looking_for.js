module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_looking_for', {
    candidate_looking_for_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: false,
    },
    desired_job_title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    job_type:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    salary:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    currency:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    category:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    location:{
        type: DataTypes.STRING(255),
        allowNull: true
    }, 
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_looking_for'
    });
  };
  