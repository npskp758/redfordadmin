module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_experience_details', {
    cand_experience_details_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: false,
    },
    company:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    job_title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    description:{
        type: DataTypes.TEXT(),
        allowNull: true
    },
    from_month:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    from_year:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    to_month:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    to_year:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    experience_in_year:{
        type: DataTypes.STRING(30),
        allowNull: true
    },
    experience_in_month:{
        type: DataTypes.STRING(30),
        allowNull: true
    },
    is_current_employer :{
        type: DataTypes.STRING(50),
        allowNull: true
    },    
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_experience_details'
    });
  };
  