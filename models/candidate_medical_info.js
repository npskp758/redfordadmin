module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_medical_info', {
    candidate_medical_info_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: false,
    },
    doh:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    doh_license_no:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    doh_license_title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    dha:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    dha_license_no:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    dha_license_title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    moh:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    moh_license_no:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    moh_license_title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    other_country_name:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    other_country_license_no:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    other_country_license_title:{
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_medical_info'
    });
  };
  