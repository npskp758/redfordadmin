module.exports = function(sequelize, DataTypes) {
    return sequelize.define('candidate_favourite_job', {
    candidate_favourite_job_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    candidate_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    job_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
      tableName: 'candidate_favourite_job'
    });
  };
  