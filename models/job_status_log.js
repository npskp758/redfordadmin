module.exports = function(sequelize, DataTypes) {
    return sequelize.define('job_status_log', {
    job_status_log_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    job_id:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    job_status:{
        type: DataTypes.STRING(50),
        allowNull: true
    },
    user_type: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    }   
    }, {
      tableName: 'job_status_log'
    });
  };
  