module.exports = function(sequelize, DataTypes) {
    return sequelize.define('job_seekers', {
    job_seeker_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
      username: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
      password: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    last_login_at: {
        type: DataTypes.DATE,
        allowNull: true
    },
      status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
    },
      createdBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
      updatedBy: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }    
    }, {
        tableName: 'job_seekers'
    });
};
  